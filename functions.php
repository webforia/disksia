<?php
define('RT_THEME_NAME', 'Diksia');
define('RT_THEME_SLUG', 'diksia');
define('RT_THEME_DOMAIN', 'diksia');
define('RT_THEME_VERSION', '1.3.0');
define('RT_THEME_URL', 'https://webforia.id/diksia');
define('RT_THEME_DOC', 'https://panduan.webforia.id/docs-category/diksia');
// include all core file
require get_template_directory() . '/core/core.php';

// Disables the block editor from managing widgets in the Gutenberg plugin.
add_filter('gutenberg_use_widgets_block_editor', '__return_false');
// Disables the block editor from managing widgets.
add_filter('use_widgets_block_editor', '__return_false');