<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">

		<?php wp_head(); ?>
	</head>

	<body <?php body_class('retheme-root retheme-default-scheme no-js'); ?>>
		<?php wp_body_open(); ?>

        <main class="page-main">
		
		<?php rt_get_template_part("header/header") ?>
