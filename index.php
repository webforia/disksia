<?php get_header(); ?>

    <?php echo rt_get_page_title() ?>

    <section class="page-wrapper">
        <div class="page-container">
           <div class="page-columns">
                <div class="page-content">

                    <?php if (have_posts()):?>

                        <div id="post_archive" <?php echo rt_get_posts_class('rt-posts')?>>
                            <?php  while (have_posts()): the_post(); ?>
                                <?php rt_get_template_part('post/post', '', rt_get_option('post_style', 'card')); ?>
                            <?php endwhile ?>
                        </div>

                        <?php echo rt_get_pagination(array(
                            'target' => 'post_archive',
                            'archive' =>  true,
                            'posts_per_page' => get_option('posts_per_page'),
                            'template_part' => 'template-parts/post/post-'.rt_get_option('post_style', 'card'),
                            'pagination_style' =>  rt_get_option('post_pagination', 'number'),
                        )); ?>
                    
                    <?php else: ?>
                        <?php rt_post_not_found(); ?>
                    <?php endif ?>

                </div>
                <?php rt_get_sidebar() ?>
           </div>
        </div>
    </section>

<?php get_footer(); ?>