<?php get_header();?>
    
    <?php
    if(!is_product()){
        echo rt_get_page_title();
    }
    ?>

    <section class="page-wrapper">
        <div class="page-container">
            <div class="page-columns">
                <div class="page-content">
                <?php

                if (have_posts()):

                    while (woocommerce_content()): the_post();

                        rt_get_template_part('page/page');

                    endwhile;

                endif;

                ?>
                </div>

                <?php rt_get_sidebar('shop') ?>

            </div>
        </div>
    </section>
<?php get_footer();?>