<?php $index = $args['index']; ?>
<section id="homepage-post-1" class="homepage-section">
    <div class="page-container">
    
    <?php 
    echo rt_get_header_block([
        'title' => rt_get_option("homebuilder_post_{$index}_title", 'Lastest Post'),
        'desc' => rt_get_option("homebuilder_post_{$index}_desc"),
    ]);


    rt_template_loop([
        'id' => 'home_blog_1',
        'class' => (rt_get_option("homebuilder_post_{$index}_carousel", false))?'rt-swiper--card': '',
        'post_type' => 'post',
        'template_part' => 'template-parts/post/post-'.rt_get_option("homebuilder_post_{$index}_style", 'grid'),
        'posts_per_page' => rt_get_option("homebuilder_post_{$index}_posts_per_page", 6),
        'query_by' => rt_get_option("homebuilder_post_{$index}_query_by"),
        'category' => rt_get_option("homebuilder_post_{$index}_category"),
        'post_id' => rt_get_option("homebuilder_post_{$index}_id"),
        'orderby' => rt_get_option("homebuilder_post_{$index}_orderby"),
        'layout' => rt_get_option("homebuilder_post_{$index}_layout", 'grid'),
        'setting_column' => rt_get_option("homebuilder_post_{$index}_column", 3),
        'setting_column_tablet' => rt_get_option("homebuilder_post_{$index}_column_tablet", 2),
        'setting_column_mobile' => rt_get_option("homebuilder_post_{$index}_column_mobile", 1),
        'pagination_style' => rt_get_option("homebuilder_post_{$index}_pagination"),
        'carousel' => rt_get_option("homebuilder_post_{$index}_carousel", false),
        'slider_item' => rt_get_option("homebuilder_post_{$index}_slider_show", 3),
        'slider_item_tablet' => rt_get_option("homebuilder_post_{$index}_slider_show_tablet", 2),
        'slider_item_mobile' => rt_get_option("homebuilder_post_{$index}_slider_show_mobile", 1),
        'slider_gap' => rt_get_option("blog_archive_column_gap", 30),
        'slider_gap_tablet' => rt_get_option("blog_archive_column_gap", 20),
        'slider_gap_mobile' => rt_get_option("blog_archive_column_gap", 15),
        'slider_loop' => rt_get_option("homebuilder_post_{$index}_slider_loop"),
        'slider_auto_play' => rt_get_option("homebuilder_post_{$index}_slider_autoplay"),
        "slider_link" => rt_get_option("homebuilder_post_{$index}_slider_link", false),
        "link_text" => rt_get_option("homebuilder_post_{$index}_pagination_link_text", __('Load More', RT_THEME_DOMAIN)),
        "link_url" => rt_get_option("homebuilder_post_{$index}_pagination_link_url"),
    ]);
    ?>

    </div>
</section>