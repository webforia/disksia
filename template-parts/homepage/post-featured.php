<?php
switch (rt_get_option('homepage_post_featured_style', 'grid-1')) {
    case 'grid-1':
        rt_get_template_part('homepage/post-featured-grid');
        break;
    case 'slider-1':
        rt_get_template_part('homepage/post-featured-slider');
        break;
    default:
        break;
}
