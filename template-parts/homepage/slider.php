<?php 
$slider_args = [
    'id' => 'home_slider',
    'items-lg' => 1,
    'items-md' => 1,
    'items-sm' => 1,
    'nav' => true,
    'spaceBetween' => 0,
    'pagination' => [
        'el' => '.swiper-pagination',
        'clickable' => true,
    ],
    'loop' => rt_get_option('homebuilder_slider_loop', false),
    'autoplay' => rt_get_option('homebuilder_slider_autoplay', false),
]; 
?>

<section id="homepage-slider" class="homepage-section">
    <div class="page-container">
        <?php echo rt_get_before_slider($slider_args); ?>
            <?php foreach (rt_get_option('homebuilder_slider') as $key => $slider): ?>
                <div class="swiper-slide">
                    
                    <?php if (!empty($slider['link_url'])): ?><a href="<?php echo esc_url($slider['link_url']) ?>"><?php endif?>
                    
                    <div class="rt-img rt-img--bordered rt-img--full">
                     <?php echo wp_get_attachment_image($slider['image'], rt_get_option('homebuilder_slider_image_size', 'full')); ?>
                    </div>

                    <?php if(!empty($slider['link_url'])): ?></a><?php endif ?>

                </div>
            <?php endforeach;?>
        <?php echo rt_get_after_slider(); ?>
    </div>
</section>