<section id="homepage-featured-slider" class="homepage-section">
    <div class="page-container">

        <?php  
        $the_query = new WP_Query([
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => 5,
            'tag'=> 'featured',
        ]);
        ?>

        <?php if ($the_query->have_posts()): ?>

            <?php echo rt_get_before_slider([
                'id' => "rt-swiper-slider-post",
                'items-lg' => 1,
                'items-md' => 1,
                'items-sm' => 1,
            ]) ?>
        
            <?php while ($the_query->have_posts()): $the_query->the_post(); ?>

                <?php echo rt_get_before_slide(); ?>

                    <article id="post-featured-<?php echo get_the_ID() ?>" <?php post_class(['rt-post rt-post--overlay rt-post-featured-slider'])?>>
                        
                        <?php rt_get_template_part('post/category'); ?>  

                        <?php if ( has_post_thumbnail() ) : ?>
                            <div class="rt-post__thumbnail rt-img rt-img--full">
                                <a href="<?php the_permalink() ?>"><?php the_post_thumbnail('large');  ?></a>
                            </div>
                        <?php endif ?>

                        <div class="rt-post__body">
                            <h2 class="rt-post__title">
                                <a href="<?php the_permalink() ?>"><?php echo rt_get_the_title() ?></a>
                            </h2>
                            <?php rt_get_template_part('post/meta'); ?>
                        </div>
                        <span class="rt-post__overlay"></span>
                    </article>

                <?php echo rt_get_after_slide(); ?>

            <?php endwhile; wp_reset_postdata(); ?>

            <?php echo rt_get_after_slider(); ?>

        <?php endif ?>

    </div>
</section>