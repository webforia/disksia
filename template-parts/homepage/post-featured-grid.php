<section id="homepage-featured-grid" class="homepage-section">
    <div class="page-container">

        <?php  
        $the_query = new WP_Query([
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => 5,
            'tag'=> 'featured',
        ]);
        ?>

        <div class="rt-post-featured-grid">
        <?php if ($the_query->have_posts()): ?>
  
            <?php while ($the_query->have_posts()): $the_query->the_post(); ?>

                <article id="post-featured-<?php echo get_the_ID() ?>" <?php post_class(['rt-post rt-post--overlay'])?>>
                    
                    <?php rt_get_template_part('post/category'); ?>  

                    <?php if ( has_post_thumbnail() ) : ?>
                        <div class="rt-post__thumbnail rt-img rt-img--full">
                            <a href="<?php the_permalink() ?>"><?php the_post_thumbnail('medium_large');  ?></a>
                        </div>
                    <?php endif ?>

                    <div class="rt-post__body">
                        <h2 class="rt-post__title">
                            <a href="<?php the_permalink() ?>"><?php echo rt_get_the_title() ?></a>
                        </h2>
                        <?php rt_get_template_part('post/meta'); ?>
                    </div>
                    
                    <a href="<?php the_permalink() ?>" class="rt-post__overlay"></a>
                </article>

            <?php endwhile; wp_reset_postdata(); ?>

        <?php endif ?>
        </div>

    </div>
</section>