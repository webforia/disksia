<section id="homepage-archive" class="homepage-section <?php echo 'homepage-section--' . rt_get_option('post_layout', 'sidebar-right') ?>">
    <div class="page-container">
            <?php
            echo rt_get_header_block([
                'title' => rt_get_option("homepage_post_archive_title", __('Latest Posts', RT_THEME_DOMAIN)),
                'desc' => rt_get_option("homepage_post_archive_desc"),
            ]);

            ?>
            <div class="page-columns">
                <div class="page-content">
                    <?php
                    $archive_slug = basename(get_permalink(get_option('page_for_posts')));
                    $posts_per_page = get_option('posts_per_page');

                    $the_query = new WP_Query([
                        'post_type' => 'post',
                        'posts_per_page' => $posts_per_page,
                        'post_status' => 'publish',
                    ]);
                    if ($the_query->have_posts()):
                        echo '<div id="post_archive" ' . rt_get_posts_class('rt-posts') . '>';

                        while ($the_query->have_posts()): $the_query->the_post();

                            rt_get_template_part('post/post-' . rt_get_option('post_style', 'grid'));

                        endwhile;

                        echo '</div>';

                        echo rt_get_pagination([
                            'target' => 'post_archive',
                            'template_part' => 'template-parts/post/post-' . rt_get_option('post_style', 'grid'),
                            'pagination_style' => rt_get_option('post_pagination', 'number'),
                            'posts_per_page' => get_option('posts_per_page'),
                            'total' => $the_query->max_num_pages,
                            'post_total' => $the_query->found_posts,
                            'format' => "/{$archive_slug}/page/%#%",
                            'current' => max(1, get_query_var('paged')),
                        ]);

                        wp_reset_postdata();
                    else:
                        rt_post_not_found();
                    endif;

                    ?>
                </div>
                <?php
                if (!in_array(rt_get_option('post_layout', 'default'), array('compact', 'full-width', 'normal'))) {
                    get_sidebar();
                }
                ?>
           </div>
    </div>
</section>