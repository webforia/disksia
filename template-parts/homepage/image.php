<section id="homepage-image" class="homepage-section">   
    <div class="page-container">

        <?php if(rt_get_option('homebuilder_image_link_url', '#')): ?>
            <a href="<?php echo esc_url(rt_get_option('homebuilder_image_link_url', '#')) ?>">
        <?php endif ?>  

        <div class="rt-img rt-img--bordered rt-img--full" >
            <?php echo wp_get_attachment_image(rt_get_option('homebuilder_image'), 'full'); ?>
        </div>

        <?php if(rt_get_option('homebuilder_image_link_url', '#')): ?>
            </a>
        <?php endif ?>

    </div>
</section>