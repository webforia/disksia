<?php
$homepage = rt_get_option('homebuilder_layout', ['post-archive']);

if (!empty($homepage)) {
    foreach ($homepage as $key => $layout) {

        $wraper_id = str_replace('-', '_', $layout);

        if (in_array($layout, ['post-1', 'post-2', 'post-3'])) {
            $arg['index'] = str_replace('post-', '', $layout);
            rt_get_template_part("homepage/post", $arg);
        } else {
            rt_get_template_part("homepage/{$layout}");
        }

    }
}
