<?php
// category
$terms = get_terms(array(
    'taxonomy' => 'category',
    'hide_empty' => rt_get_option('homebuilder_post_category_hide_empty', true),
));
?>

<section class="categories homepage-section">
    <div class="page-container">
    
            <?php 
            echo rt_get_header_block([
                'title' => rt_get_option("homebuilder_category_title", 'Category Title'),
                'desc' => rt_get_option("homebuilder_category_desc", 'Category Desc'),
            ]);
            ?>

            <?php if (!empty($terms) && !is_wp_error($terms)): ?>

                <?php echo rt_get_before_slider([
                        'id' => "rt-swiper-homepage-categories",
                        'items-lg' => 5,
                        'items-md' => 2,
                        'items-sm' => 1,
                    ]); 
                ?>
                    <?php foreach ($terms as $key => $term): ?>
                        <?php if ($term->slug != 'uncategorized'): ?>

                            <?php $thumbnail_id = rt_get_term_meta($term->term_id, 'wf_thumbnail', true);?>

                            <?php echo rt_get_before_slide() ?>

                            <div class="rt-category-img">
                                <a href="<?php echo get_term_link($term->slug, 'category') ?>">

                                    <?php if(wp_get_attachment_image($thumbnail_id, 'medium')):?>
                                    <div class="rt-category-img__thumbnail">
                                        <?php echo wp_get_attachment_image($thumbnail_id, 'medium') ?>
                                    </div>
                                    <?php endif ?>
                                    
                                    <div class="rt-category-img__body">
                                        <h4 class="rt-category-img__title"><?php echo esc_html($term->name); ?></h4>
                                        <span class="rt-category-img__count"><?php echo wp_sprintf(__('%s Post', RT_THEME_DOMAIN), $term->count) ?></span>
                                    </div>
                                </a> 
                                <span class="rt-category-img__overlay"></span>
                            </div>

                            <?php echo rt_get_after_slide() ?>

                        <?php endif ?>
                    <?php endforeach ?>
                
                <?php echo rt_get_after_slider() ?>

            <?php endif ?>

    </div>
</section>