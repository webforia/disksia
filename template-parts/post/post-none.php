<p>
<?php 
	if(is_home() && current_user_can('publish_posts')){
		echo sprintf(__('Ready to publish your first post? %1$sGet started here%2$s.', RT_THEME_DOMAIN), '<a href="'. esc_url(admin_url('post-new.php')) .'" target="_blank">', '</a>');
	}elseif(is_search()){
		_e("Sorry, but nothing matched your search terms. Please try again with different keywords.", RT_THEME_DOMAIN);
		get_template_part('searchform');
	}elseif(is_category()){
		_e("There aren't any posts currently published in this category.", RT_THEME_DOMAIN);
	}elseif(is_tax()) {
		_e("There aren't any posts currently published under this taxonomy.", RT_THEME_DOMAIN);
	}elseif(is_tag()){
		_e("There aren't any posts currently published under this tag.", RT_THEME_DOMAIN); 
	}else{
		_e('It seems we can&rsquo;t find what you&rsquo;re looking for.', RT_THEME_DOMAIN);
	}
?>
</p>