<?php if ( has_post_thumbnail() && rt_get_post_option('thumbnail')) : ?>
    <div class="rt-post__thumbnail rt-img rt-img--full">
        <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
            <?php the_post_thumbnail('featured_medium');  ?>
        </a>
    </div>
<?php endif ?>