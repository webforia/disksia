<article id="post-<?php echo get_the_ID() ?>" <?php post_class(['rt-post rt-post--overlay'])?>>
    <?php rt_get_template_part('post/category'); ?>  

    <?php if ( has_post_thumbnail()) : ?>
    <div class="rt-post__thumbnail rt-img rt-img--full">
        <a href="<?php the_permalink() ?>"><?php the_post_thumbnail('medium_large');  ?></a>
    </div>
    <?php endif ?>

    <div class="rt-post__body">
        <?php
        rt_get_template_part('post/title');
        rt_get_template_part('post/meta');
        ?>
    </div>
    <a href="<?php the_permalink() ?>" class="rt-post__overlay"></a>
</article>