<article  id="post-<?php echo get_the_ID() ?>" <?php post_class(array('rt-post rt-post--list-sm'))?>>
    <?php if ( has_post_thumbnail() && rt_get_post_option('thumbnail')) : ?>
        <div class="rt-post__thumbnail rt-img rt-img--full">
            <a href="<?php the_permalink() ?>"><?php the_post_thumbnail('thumbnail');  ?></a>
        </div>
    <?php endif ?>
    <div class="rt-post__body">
        <h3 class="rt-post__title">
            <a href="<?php the_permalink() ?>"><?php echo rt_get_the_title() ?></a>
        </h3>
        <?php rt_get_template_part('post/meta'); ?>
    </div>
</article>