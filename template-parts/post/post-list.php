<article  id="post-<?php echo get_the_ID() ?>" <?php post_class(array('rt-post rt-post--list'))?>>
    <?php rt_get_template_part('post/thumbnail'); ?>
    <div class="rt-post__body">
        <?php
        rt_get_template_part('post/category');
        rt_get_template_part('post/title');
        rt_get_template_part('post/meta');
        rt_get_template_part('post/content');
        ?>
    </div>
</article>