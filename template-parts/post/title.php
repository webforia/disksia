<h3 class="rt-post__title">
  <a href="<?php the_permalink() ?>"><?php echo wp_trim_words(get_the_title(), rt_get_option('post_title_length', 9)) ?></a>
</h3>