<?php if( rt_get_post_option('content')): ?>
    <div class="rt-post__excerpt"><?php echo get_the_excerpt() ?></div>
<?php endif ?>