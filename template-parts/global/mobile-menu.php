<div id="mobile_offcanvas_menu" class="rt-slidepanel rt-slidepanel--left">

    <div class="rt-slidepanel__overlay js-slidepanel-close">
             <a class="rt-slidepanel__close">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-lg" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M13.854 2.146a.5.5 0 0 1 0 .708l-11 11a.5.5 0 0 1-.708-.708l11-11a.5.5 0 0 1 .708 0Z"></path>
                    <path fill-rule="evenodd" d="M2.146 2.146a.5.5 0 0 0 0 .708l11 11a.5.5 0 0 0 .708-.708l-11-11a.5.5 0 0 0-.708 0Z"></path>
                </svg>
            </a>
    </div>

    <div class="rt-slidepanel__inner">
        
        <div class="rt-slidepanel__header">
            <?php echo rt_get_logo('mobile') ?>
        </div>

        <div class="rt-slidepanel__body" style="padding:0">
            
            <nav id="menu-primary" class="rt-menu rt-menu--vertical js-menu">
                <?php
                if (has_nav_menu('mobile')) {
                    wp_nav_menu(array(
                        'container' => '',
                        'menu_class' => 'rt-menu__main',
                        'theme_location' => 'mobile',
                        'fallback_cb' => 'rt_menu_fallback',
                    ));
                } else {
                    wp_nav_menu(array(
                        'container' => '',
                        'menu_class' => 'rt-menu__main',
                        'theme_location' => 'primary',
                        'fallback_cb' => 'rt_menu_fallback',
                    ));
                }
                ?>
            </nav>

            <div class="rt-slidepanel__footer">
                <?php echo rt_get_social_media() ?>
            </div>

        </div>

    </div>

</div>