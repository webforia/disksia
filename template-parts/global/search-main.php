<div id="main_search" class="rt-modal" data-animatein="zoomIn" data-animateout="zoomOut">
    <span class="rt-modal__overlay js-modal-close"></span>
    <div class="rt-modal__inner">

        <div class="rt-modal__header">
            <h3 class="rt-modal__title"><?php echo __('Search Form', RT_THEME_DOMAIN) ?></h3>
            <a class="rt-icon rt-modal__close js-modal-close">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-lg" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M13.854 2.146a.5.5 0 0 1 0 .708l-11 11a.5.5 0 0 1-.708-.708l11-11a.5.5 0 0 1 .708 0Z"/>
                    <path fill-rule="evenodd" d="M2.146 2.146a.5.5 0 0 0 0 .708l11 11a.5.5 0 0 0 .708-.708l-11-11a.5.5 0 0 0-.708 0Z"/>
                </svg>
            </a>
        </div>

        <div class="rt-modal__body" style="padding:20px 15px">
        
            <?php get_template_part('searchform') ?>

        </div>

    </div>
</div>