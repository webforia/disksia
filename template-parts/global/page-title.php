<section class="page-titlebar">
    <div class="page-container">

        <?php echo rt_get_breadrumbs() ?>

        <h1 class="page-titlebar__title"><?php echo $args['title'] ?></h1>
        
        <?php if(!empty($args['desc'])): ?>
            <div class="page-titlebar__desc" style="margin-top: 15px"><?php echo $args['desc'] ?></div>
        <?php endif ?>
    </div>
</section>