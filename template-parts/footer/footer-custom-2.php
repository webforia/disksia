<footer id="page-footer" class="page-footer page-footer--custom-2">
    <div class="page-footer__widget">
        <div class="page-container">

            <div id="widget-footer-1">
                <?php echo rt_get_site_copyright() ?>
            </div>

            <div id="widget-footer-2">
                <nav id="menu-footer" class="rt-menu js-menu rt-menu--horizontal rt-menu--style-link">
                    <?php
                    if (has_nav_menu('footer')) {
                        wp_nav_menu(array(
                            'container' => '',
                            'menu_class' => 'rt-menu__main',
                            'theme_location' => 'footer',
                            'fallback_cb' => 'rt_menu_fallback',
                        ));
                    } else {
                        rt_menu_default();
                    }
                    ?>
                </nav>
            </div>

        </div>

    </div>
</footer>