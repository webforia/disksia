<footer id="page-footer" class="page-footer page-footer--style-1">
    <div class="page-footer__widget">
        <div class="page-container">

            <div class="flexs">

                <div id="widget-footer-1" class="flex-lg-3 flex-md-6 flex-sm-12">
                    <?php
                    if (is_active_sidebar('retheme_footer_1')) {
                        dynamic_sidebar('retheme_footer_1');
                    }
                    ?>
                </div>

                <div id="widget-footer-2" class="flex-lg-3 flex-md-6 flex-sm-12">
                    <?php
                    if (is_active_sidebar('retheme_footer_2')) {
                        dynamic_sidebar('retheme_footer_2');
                    }
                    ?>
                </div>

                <div id="widget-footer-3" class="flex-lg-3 flex-md-6 flex-sm-12">
                    <?php
                    if (is_active_sidebar('retheme_footer_3')) {
                        dynamic_sidebar('retheme_footer_3');
                    }
                    ?>
                </div>

                <div id="widget-footer-4" class="flex-lg-3 flex-md-6 flex-sm-12">
                    <?php
                    if (is_active_sidebar('retheme_footer_4')) {
                        dynamic_sidebar('retheme_footer_4');
                    }
                    ?>
                </div>

            </div>

        </div>

    </div>
    <?php rt_get_template_part('footer/footer-bottom');?>
</footer>