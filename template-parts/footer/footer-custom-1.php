<footer id="page-footer" class="page-footer page-footer--custom-1">
    <div class="page-footer__widget">
        <div class="page-container">
            <div id="widget-footer-1">
                <?php echo rt_get_site_copyright() ?>
            </div>

            <div id="widget-footer-2">
                <?php echo rt_get_social_media() ?>
            </div>
        </div>

    </div>
</footer>