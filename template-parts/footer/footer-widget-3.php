<footer id="page-footer" class="page-footer page-footer--style-2">
    <div class="page-footer__widget">
        <div class="page-container">
            
            <div class="flex flex-row">
        
                <div id="widget-footer-1" class="flex-lg-6 flex-md-12">
                    <?php
                    if (is_active_sidebar('retheme_footer_1')) {
                        dynamic_sidebar('retheme_footer_1');
                    }
                    ?>
                </div>
        
                <div id="widget-footer-2" class="flex-lg-6 flex-md-12">
                    <?php
                    if (is_active_sidebar('retheme_footer_2')) {
                        dynamic_sidebar('retheme_footer_2');
                    }
                    ?>
                </div>

                </div>

            </div>

        </div>

    </div>
    <?php rt_get_template_part('footer/footer-bottom');?>
</footer>