<header class="page-header page-header--style-1 js-header" data-sticky="<?php echo rt_get_option('header_sticky', true)?>">
    <?php rt_get_template_part('header/header-top') ?>
    <div class="page-header__main">
        <div class="page-container">
            <div class="page-header-columns">
                <div class="page-header-column" data-alignment="left" data-display="normal">
                    <?php echo rt_get_logo() ?>
                </div>
                <div class="page-header-column" data-alignment="right" data-display="grow">
                     <nav id="menu-primary" class="rt-menu rt-menu--horizontal js-menu" data-animatein="zoomIn" data-animateout="zoomOut">
                        <?php
                        if (has_nav_menu('primary')) {
                            wp_nav_menu(array(
                                'container' => '',
                                'menu_class' => 'rt-menu__main',
                                'theme_location' => 'primary',
                                'fallback_cb' => 'rt_menu_fallback',
                            ));
                        } else {
                            rt_menu_default();
                        }
                        ?>
                    </nav>
                    
                    <?php if(rt_get_option('dark_mode', true)): ?>
                    <span class="rt-header-icon rt-header-dark-toggle">
                        <?php rt_get_template_part('global/dark-mode-toggle') ?>
                    </span>
                    <?php endif ?>

                    <a href="#main_search" class="rt-header-icon rt-header-search js-modal">
                        <span class="rt-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                                <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                            </svg>
                        </span>
                    </a>

                </div>
            </div>
        </div>
    </div>
    <?php rt_get_template_part('header/header-sticky-right') ?>
</header>