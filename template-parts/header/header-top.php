<?php if(rt_get_option('header_top', false)): ?>
<div class="page-header__top">
    <div class="page-container">
        <div class="page-header-columns">
            <div class="page-header-column" data-alignment="left" data-display="normal">
                <?php if(rt_get_option('header_top_menu', true)): ?>
                <nav id="menu-top" class="rt-menu rt-menu--horizontal rt-menu--style-link js-menu">
                    <?php
                    if (has_nav_menu('topbar')) {
                        wp_nav_menu(array(
                            'container' => '',
                            'menu_class' => 'rt-menu__main',
                            'theme_location' => 'topbar',
                            'fallback_cb' => 'rt_menu_fallback',
                        ));
                    } else {
                        rt_menu_default();
                    }
                    ?>
                </nav>
                <?php endif ?>
            </div>

            <div class="page-header-column" data-alignment="right" data-display="normal">
                <?php if(rt_get_option('header_top_socmed', true)): ?>
                <div class="rt-header-socmed">
                    <?php echo rt_get_social_media(['size' => 'sm']) ?>
                </div>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>
<?php endif ?>