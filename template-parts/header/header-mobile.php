<header class="page-header-mobile js-header-mobile" data-sticky="<?php echo rt_get_option('header_mobile_sticky', true)?>">
    <div class="page-header-mobile__main">
        <div class="page-container">
            <div class="page-header-columns">
                <div class="page-header-column" data-alignment="left" data-display="grow">
                    <a href="#mobile_offcanvas_menu" class="rt-menu-toggle rt-menu-toggle--point js-slidepanel"><span></span></a>
                </div>
                <div class="page-header-column" data-alignment="center" data-display="normal">
                    <?php echo rt_get_logo('mobile') ?>
                </div>
                <div class="page-header-column" data-alignment="right" data-display="grow">
                    
                    <?php if(rt_get_option('dark_mode', true)): ?>
                    <span class="rt-header-icon rt-header-dark-toggle">
                        <?php rt_get_template_part('global/dark-mode-toggle') ?>
                    </span>
                    <?php endif ?>

                     <a href="#main_search" class="rt-header-icon rt-header-search js-modal">
                        <span class="rt-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                                <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                            </svg>
                        </span>
                    </a>
                </div>
            </div>
       </div>
    </div>
    
</header>

<?php if(rt_get_option('banner_header', 'image') !== 'disable' && rt_get_option('header_layout', 'header-1') == 'header-3' ): ?>
    <div class="page-banner" style="margin-top: 30px">
        <div class="page-container">
        <?php
            echo rt_get_banner_ads([
                'type' => rt_get_option('banner_header', 'disable'),
                'image' => rt_get_option('banner_header_image'),
                'url' => rt_get_option('banner_header_url', '#'),
                'code' => rt_get_option('banner_header_code'),
            ]);
        ?>
        </div>
    </div>
<?php endif ?>