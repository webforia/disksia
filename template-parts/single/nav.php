<?php
$nextPost = get_next_post();
$prevPost = get_previous_post();
?>
<?php if(rt_get_single_option('navigation')): ?>
    <?php if((!empty($nextPost) || !empty($prevPost))): ?>
        <div class="rt-single-nav">

            <?php if (isset($prevPost) && $prevPost != null): ?>
            <a href="<?php echo esc_url(get_the_permalink($prevPost->ID)) ?>" class="rt-single-nav-prev">
                
                <?php if(has_post_thumbnail($prevPost->ID)): ?>
                <div class="rt-single-nav__thumbnail rt-img"><?php echo get_the_post_thumbnail($prevPost->ID, 'thumbnail') ?></div>
                <?php endif ?>

                <div class="rt-single-nav__body">
                    <div class="rt-single-nav__label">
                        <span class="rt-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-left" viewBox="0 0 16 16">
                                <path fill-rule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"/>
                            </svg>
                        </span>
                        <?php echo __('Previous Post', RT_THEME_DOMAIN) ?>
                    </div>
                    <h4 class="rt-single-nav__title"><?php echo rt_limited_string(get_the_title($prevPost->ID), 65) ?></h4>
                </div>
            </a>
            <?php else: ?>
                <span class="rt-single-nav-prev"></span>
            <?php endif?>

            <?php if (isset($nextPost) && $nextPost != null): ?>
            <a href="<?php echo esc_url(get_the_permalink($nextPost->ID)) ?>" class="rt-single-nav-next">

                <?php if(has_post_thumbnail($nextPost->ID)): ?>
                <div class="rt-single-nav__thumbnail rt-img"><?php echo get_the_post_thumbnail($nextPost->ID, 'thumbnail') ?></div>
                <?php endif ?>

                <div class="rt-single-nav__body">

                    <div class="rt-single-nav__label">
                        <?php echo __('Newer Post', RT_THEME_DOMAIN) ?>
                        <span class="rt-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-right" viewBox="0 0 16 16">
                                <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/>
                            </svg>
                        </span>
                    </div>

                    <h4 class="rt-single-nav__title"><?php echo rt_limited_string(get_the_title($nextPost->ID), 65) ?></h4>
                </div>
            </a>
            <?php else: ?>
                <span class="rt-single-nav-next"></span>
            <?php endif?>

        </div>
    <?php endif ?>
<?php endif ?>