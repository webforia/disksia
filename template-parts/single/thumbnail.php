<?php if ( has_post_thumbnail() && rt_get_single_option('thumbnail')) : ?>
    <div class="rt-single-thumbnail rt-img rt-img--full">
        <?php the_post_thumbnail('large');  ?>
    </div>
<?php endif ?>