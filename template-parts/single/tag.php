<?php  if (!empty(get_the_tags()) && rt_get_single_option('tag')): ?>
<div class="rt-single-tag">
  <div class="rt-post-tags">
    <?php foreach (get_the_tags() as $term): ?>
      <a class="rt-post-tag <?php echo esc_attr( $term->slug )?>" href="<?php echo esc_url(get_tag_link($term->term_id))?>">
          <span class="rt-icon">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-tag" viewBox="0 0 16 16">
              <path d="M6 4.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm-1 0a.5.5 0 1 0-1 0 .5.5 0 0 0 1 0z"/>
              <path d="M2 1h4.586a1 1 0 0 1 .707.293l7 7a1 1 0 0 1 0 1.414l-4.586 4.586a1 1 0 0 1-1.414 0l-7-7A1 1 0 0 1 1 6.586V2a1 1 0 0 1 1-1zm0 5.586 7 7L13.586 9l-7-7H2v4.586z"/>
            </svg>
          </span>
          <span class="rt-post-tag__label">
            <?php echo $term->name ?>
          </span>
      </a>
    <?php endforeach; ?>
  </div>
</div>
<?php endif ?>
