<?php
$terms = get_the_terms(get_the_ID(), 'category');
$related_manual = rt_get_post_related_id();
$query_related = array();

if ($related_manual) {
    $query_related['post__in'] = $related_manual;
} else {

    if (!is_wp_error($terms) && !empty($terms)) {

        foreach ($terms as $key => $term) {
            $term_id[] = $term->term_id;
        }

        if ($term_id) {
            $query_related['tax_query'][] = [
                'taxonomy' => 'category',
                'field'    => 'term_id',
                'terms'    => $term_id,
                'operator' => 'IN',
            ];
        }
    }
}

$results = array_merge($query_related,[
    'id' => 'post-related',
    'class' => 'rt-swiper--card',
    'post__not_in' => array (get_the_ID()),
    'post_type' => 'post',
    'posts_per_page' => rt_get_option('single_related_count', 6),
    'carousel' => true,
    'template_part' => 'template-parts/post/post-grid',
    'slider_item' => rt_get_option('single_related_show', 2),
    'slider_item_tablet' => rt_get_option('single_related_show_tablet', 2),
    'slider_item_mobile' => rt_get_option('single_related_show_mobile', 1),
]);

if (rt_get_single_option('related') && count(get_posts($results)) && rt_is_premium()):?>

    <div class="rt-post-related">
        <div class="rt-header-block">
            <h2 class="rt-header-block__title"><?php echo __('Post Related', RT_THEME_DOMAIN) ?></h2>
        </div>
    
        <?php rt_template_loop($results);?>
    </div>

<?php endif?>