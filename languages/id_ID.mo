��    #      4  /   L           	          $     -     5  	   =     G  ;   L     �     �  	   �     �  
   �     �     �     �     �     �  ;   �     3     ?     K     b     y     �     �     �  W   �  <     :   Q  ?   �     �  $   �  )   �  �  "          '     ;  	   A     K     T     a  9   i     �     �     �     �     �     �  
   	     	     !	     1	  I   E	  	   �	     �	     �	     �	     �	     �	     �	     
  p   $
  A   �
  B   �
  C        ^  ,   g  ,   �                                                    #           	      "                 
                              !                                         %1$s at %2$s 404 Not Found Archives Browse: Comment Full Name Home It seems we can&rsquo;t find what you&rsquo;re looking for. Lastest Post Leave a Reply Load More Loading Newer Post No %s result Page %s Pages: Post Related Previous Post Ready to publish your first post? %1$sGet started here%2$s. Search Form Search Post Search Results for: %s Search results for: %s Send a Comment Share Share this article: Showing %s of %s2 results Sorry, but nothing matched your search terms. Please try again with different keywords. There aren't any posts currently published in this category. There aren't any posts currently published under this tag. There aren't any posts currently published under this taxonomy. Viewer Your comment is awaiting moderation. Your email address will not be published. Project-Id-Version: Diskia
PO-Revision-Date: 2022-05-12 14:13+0700
Last-Translator: 
Language-Team: Webforia Studio
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 3.0.1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e;esc_html;_n
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: core/customizer
X-Poedit-SearchPathExcluded-1: core/vendor
X-Poedit-SearchPathExcluded-2: core/admin
 %1$s pada %2$s 404 tidak ditemukan Arsip Jelajahi: Komentar Nama Lengkap Beranda Sepertinya kami tidak dapat menemukan apa yang Anda cari. Artikel Terbaru Tinggalkan Komentar Muat Lebih Banyak Memuat Artikel Terbaru Pencarian %s tidak ditemukan Halaman %s Halaman: Artikel Terkait Artikel Sebelumnnya Siap untuk mempublikasikan posting pertama Anda? %1$s Mulai di sini %2$s. Pencarian Cari Artikel Hasil Pencarian untuk: %s Hasil pencarian untuk: %s Kirim Komentar Bagikan Bagikan artikel ini: Menampilkan %s dari %s2 hasil Maaf, tapi tidak ada yang cocok dengan istilah pencarian Anda. Silakan coba lagi dengan kata kunci yang berbeda. Saat ini tidak ada postingan yang diterbitkan dalam kategori ini. Saat ini tidak ada postingan yang dipublikasikan di bawah tag ini. Tidak ada posting yang saat ini diterbitkan di bawah taksonomi ini. Tayangan Komentar dari anda sedang menunggu moderasi. Alamat email Anda tidak akan dipublikasikan. 