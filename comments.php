
<?php

if (post_password_required()) {
    return;
}
?>
<div class="rt-comment">

<div class="rt-header-block">
  <h2 class="rt-header-block__title"><?php _e('Leave a Reply', RT_THEME_DOMAIN)?></h2>
</div>

<?php if (number_format_i18n(get_comments_number()) > 0): ?>
  
  <ul class="rt-comment-list">
  <?php

  wp_list_comments(array(
      'style' => 'ul',
      'short_ping' => true,
      'avatar_size' => 70,
      'callback' => 'rt_comment_callback',
  ));
  ?>
  </ul>


<?php if(paginate_comments_links()): ?>
<div class="rt-pagination">
  <?php paginate_comments_links(array(
      'prev_text' => '<span class="rt-icon"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-left" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"/></svg></span>',
      'next_text' => '<span class="rt-icon"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-right" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/></svg></span>',
  ));?>
</div>
<?php endif ?>


<?php endif;?>


<?php if (!comments_open() && get_comments_number() && post_type_supports(get_post_type(), 'comments')): ?>
  <p class="no-comments rt-alert rt-alert--info"><?php esc_html_e('Comments are closed.', RT_THEME_DOMAIN);?></p>
<?php endif;?>



 <?php
/** COMMENT FORM */
if (comments_open()):
    comment_form(array(
        'title_reply' => '',
        'label_submit' => __('Send a Comment', RT_THEME_DOMAIN),
        'id_form' => 'commentform',
        'class_form' => 'rt-comment-form js-comment',
        'comment_notes_before' => '<p class="comment_notes">' . __('Your email address will not be published.', RT_THEME_DOMAIN) . '</p>',
        'cancel_reply_before' => '<small class="comment_cancel-reply">',
        'cancel_reply_after' => '</small>',
    ));

endif;
?>
</div>
