/*=================================================;
/* Add js CSS ready
/*================================================= */
document.body.classList.add("js");
document.body.classList.remove("no-js");

/* =================================================
 *  GOTOP
 * =================================================== */
function gotop() {
  const gotop = document.querySelector(".js-gotop");

  if (gotop == null) {
    return false;
  }

  // show gotop button
  if (window.pageYOffset > 200) {
    gotop.classList.add("is-active");
  } else {
    gotop.classList.remove("is-active");
  }

  // scroll to top
  gotop.addEventListener("click", () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  });
}
window.addEventListener("scroll", gotop);

localStorage.setItem("rtVisitor", 'visit');