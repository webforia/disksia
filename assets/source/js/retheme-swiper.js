/**
 * Retheme global slider for Swiperjs
 * @version 3.0.0
 */
function swiperjs() {
    const sliders = document.querySelectorAll(".js-swiper");
  
    sliders.forEach(function (slider, index) {
      const options = JSON.parse(slider.getAttribute("data-options"));
      const container = slider.querySelector(".swiper-container");
      const navPrev = slider.querySelector(".swiper-button-prev");
      const navNext = slider.querySelector(".swiper-button-next");
  
      const swiper = new Swiper(container, options);
        
      // remove navigation if set disable
      if(options.navigation == false){
        navPrev.style.display = 'none';
        navNext.style.display = "none";
      }

    });
  }
  
  swiperjs();
  