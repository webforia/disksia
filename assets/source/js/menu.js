/**
 * Retheme Menu
 * @version 3.0.0
 */
class menu {
    constructor() {
      this.menu = ".js-menu";
      this.item = ".rt-menu__item";
      this.submenu = ".rt-menu__submenu";
      this.arrow = ".rt-menu__arrow";
      this.animateIn = "data-animatein";
      this.animateOut = "data-animateOut";
      this.duration = "data-duration";
  
      this.listener();
    }
  
    listener() {
      const menus = document.querySelectorAll(this.menu);
  
      menus.forEach((menu) => {
        const options = {
          items: menu.querySelectorAll(this.item),
          arrows: menu.querySelectorAll(this.arrow),
          animateIn: menu.getAttribute(this.animateIn),
          animateOut: menu.getAttribute(this.animateOut),
          duration: menu.getAttribute(this.duration),
        };
        if (menu.classList.contains("rt-menu--vertical")) {
          this.vertical(options);
        } else {
          this.horizontal(options);
        }
      });
    }
  
    horizontal(option) {
      const items = option.items;
      const animateIn = option.animateIn;
      const animateOut = option.animateOut;
      const duration = option.duration;
  
      items.forEach((item) => {
        item.addEventListener("mouseenter", (event) => {
          const el = event.currentTarget;
          const submenu = el.querySelector(this.submenu);
  
          item.classList.add("is-active");
          
          if (submenu !== null) {
            animateCSS(submenu, option.animateIn, "300");
          }
        });
  
        item.addEventListener("mouseleave", (event) => {
          const el = event.currentTarget;
          const submenu = el.querySelector(this.submenu);
  
          item.classList.remove("is-active");
          
          if (submenu !== null) {
            animateCSS(submenu, option.animateOut, "300");
          }
        });
      });
    }
  
    vertical(option) {
      const items = option.items;
      const animateIn = option.animateIn;
      const animateOut = option.animateOut;
      const duration = option.duration;
      const arrows = option.arrows;
  
      // arrow click
      arrows.forEach((arrow) => {
        arrow.addEventListener("click", (event) => {
          const el = event.currentTarget;
          const item = el.parentNode;
          const submenu = item.querySelector(this.submenu);
          if (submenu == null) {
            return false;
          }
          if (item.classList.contains("is-active")) {
            el.classList.remove("is-active");
            item.classList.remove("is-active");
            slideUp(submenu);
          } else {
            el.classList.add("is-active");
            item.classList.add("is-active");
            slideDown(submenu);
          }
        });
      });
    }
    
  }
  
  new menu;
  