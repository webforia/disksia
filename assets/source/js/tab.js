/**
 * Retheme tabs
 * @version 3.0.0
 */
class tabs{
    constructor(){
        this.tab = '.js-tab';
        this.item = '.rt-tab__item';
        this.title = ".rt-tab__title";
        this.open();
    }
    hideAll(tabs){
        tabs.title.forEach((element) => {
          element.classList.remove("is-active");
        });
        tabs.item.forEach((element) => {
          element.classList.remove("is-active");
          element.style.display = "none";
        });
    }

    open(){
        const tabs = document.querySelectorAll(this.tab);

        tabs.forEach((tab) => {
          // get all element
          const items = tab.querySelectorAll(this.item);
          const titles = tab.querySelectorAll(this.title);

          titles.forEach((title) => {
            title.addEventListener("click", (event) => {
              event.preventDefault();
              const el = event.currentTarget;

              const dataItem = el.getAttribute("href");
              const item = tab.querySelector(dataItem);

              // hide all tabs
              this.hideAll({ title: titles, item: items });

              // show this tabs
              el.classList.add("is-active");
              item.classList.add("is-active");

              item.style.display = "block";

            });
          });
        });
    }
}
new tabs();