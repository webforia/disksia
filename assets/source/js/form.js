/**
 * Retheme Form overlay
 * @version 3.0.0
 */
class form {
    constructor() {
      this.form = ".js-form-overlay";
      this.input = ".js-form-overlay .rt-form__input";
  
      this.openAll();
      this.open();
      this.quantity();
    }
  
    /**
     * Open/close all form if windows load
     * open if form have value
     * close if form not have value
     */
    openAll() {
      const inputs = document.querySelectorAll(this.input);
  
      inputs.forEach((input) => {
        if (input.value.length > 0) {
          input.closest(this.form).classList.add("is-active");
        } else {
          input.closest(this.form).classList.remove("is-active");
        }
      });
    }
  
    /**
     * Open form on focus
     * run openall function
     */
    open() {
      const inputs = document.querySelectorAll(this.input);
  
      inputs.forEach((input) => {
  
        input.addEventListener("focus", (event) => {
            const item = event.currentTarget;
  
            // close all form
            this.openAll();
  
            // active this input
            item.closest(this.form).classList.add("is-active");
        });
      });
    }

    quantity() {
      document.addEventListener("click", (event) => {
        const trigger = event.target.closest(".js-quantity-change span");
    
        if (trigger && document.contains(trigger)) {
          event.preventDefault();
    
          const qty = trigger
            .closest(".js-quantity-change")
            .querySelector("input");
          const val = parseFloat(qty.value);
          const max = parseFloat(qty.getAttribute("max"));
          const min = parseFloat(qty.getAttribute("min"));
          const step = parseFloat(qty.getAttribute("step"));
    
          if (val >= max) {
            qty.style.setProperty("pointer-events", "none");
          } else {
            qty.style.setProperty("pointer-events", "auto");
          }
    
          // Change the value if plus or minus
          if (trigger.classList.contains("plus")) {
            if (max && max <= val) {
              qty.value = max;
            } else if (isNaN(val) || val == null) {
              qty.value = 1;
            } else {
              qty.value = val + step;
            }
          } else {
            if (min && min >= val) {
              qty.value = min;
            } else if (val > min) {
              qty.value = val - step;
            }
          }
        }
      });
    }

  }
  
  new form;
  