/**
 * Retheme Sticky Header
 * @version 3.0.0
 */
function headerFloatDesktop() {
  const header = document.querySelector(".js-header");

  if (header == null) {
    return false;
  }
  const sticky = header.getAttribute("data-sticky");
  const headerHeight = header.clientHeight;

  if (window.pageYOffset > headerHeight && sticky == true) {
    header.classList.add("is-sticky");
  } else {
    header.classList.remove("is-sticky");
  }
}
window.addEventListener("scroll", headerFloatDesktop);

// header sm sticky
function headerFloatMobile() {
  const header = document.querySelector(".js-header-mobile");

  if (header == null) {
    return false;
  }
  const sticky = header.getAttribute("data-sticky");
  const headerHeight = header.clientHeight;

  if (window.pageYOffset > headerHeight && sticky == true) {
    header.classList.add("is-sticky");
  } else {
    header.classList.remove("is-sticky");
  }
}
window.addEventListener("scroll", headerFloatMobile);
