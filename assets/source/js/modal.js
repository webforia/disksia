/**
 * Retheme Modal Pop up
 * @version 3.1.0
 */
class modal {
  constructor() {
    this.trigger = ".js-modal";
    this.close = ".js-modal-close";
    this.component = ".rt-modal";
    this.overlay = ".rt-modal__overlay";
    this.inner = ".rt-modal__inner";
    this.animateIn = "data-animatein";
    this.animateOut = "data-animateOut";
    this.scroll = "data-scroll";

    this.listener();
    this.delete();
  }

  open(el) {
    el.element.style.display = "block";

    animateCSS(el.overlay, "fadeIn", "500");
    animateCSS(el.inner, el.animateIn, "500");

    // add listener to disable scroll
    document.querySelector("html").style.overflowY = "hidden";
  }

  delete(el) {
    const closes = document.querySelectorAll(this.close);

    // Close all modal popup
    closes.forEach((close) => {
      close.addEventListener("click", (event) => {

        const target = close.closest(this.component);

        // Close modal
        animateCSS(target.querySelector(this.inner), target.getAttribute(this.animateOut), "500");
        animateCSS(target.querySelector(this.overlay), "fadeOut", "500").then(() => {
            target.style.display = "none";
        });

        // Remove listener to re-enable scroll
        document.querySelector("html").style.overflowY = "auto";

        // remove active from all trigger modal
        document.querySelectorAll(this.trigger).forEach((trigger) => {
          trigger.classList.remove("is-active");
        });

      });
    });
  }

  listener() {
    const modals = document.querySelectorAll(this.trigger);
    modals.forEach((modal) => {
      const dataTarget = modal.getAttribute("data-target")? modal.getAttribute("data-target"): modal.getAttribute("href");
      const target = document.querySelector(dataTarget);

      if (target == null) {
        return false;
      }

      modal.addEventListener("click", (event) => {
        event.preventDefault();
        const _this = event.currentTarget;

        if (_this.classList.contains("disabled")) {
          return false;
        }

        _this.classList.add("is-active");

        this.open({
          element: target,
          overlay: target.querySelector(this.overlay),
          inner: target.querySelector(this.inner),
          animateIn: target.getAttribute(this.animateIn),
          scroll: target.getAttribute(this.scroll),
        });
        
      });
    });
  }

  load(id) {
    const modals = document.querySelectorAll(id);

    modals.forEach((modal) => {
      this.open({
        element: modal,
        overlay: modal.querySelector(this.overlay),
        inner: modal.querySelector(this.inner),
        animateIn: modal.getAttribute(this.animateIn),
        animateOut: modal.getAttribute(this.animateOut),
      });
    });
  }
}

new modal();
