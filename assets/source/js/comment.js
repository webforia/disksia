/**
 * Retheme comment
 * @version 3.0.0
 */

function commentForm() {
  const form = document.querySelector(".js-comment");

  if (form == null) {
    return false;
  }

  const comment = form.querySelector("textarea");
  const field = form.querySelector(".comment-input");

  comment.addEventListener("keyup", (event) => {
    if (!comment.classList.contains("is-active")) {
      event.preventDefault();
      comment.classList.add("is-active");
      animateCSS(field, "fadeIn", "300");
    }
  });
}
commentForm();
