/**
 * Dark mode Switch
 * @version 1.0.1
 */
function darkModeSwitch() {
  const toggles = document.querySelectorAll(".js-dark-mode");

  // enable dark mode form local storage
  if (localStorage.getItem('retheme-dark-scheme') === 'active') {
    document.body.classList.remove('retheme-default-scheme');
    document.body.classList.add('retheme-dark-scheme');

    toggles.forEach((toggle) => {
      toggle.classList.add('is-active');
    });
  }

  // enable dark mode when toogle on click
  toggles.forEach((toggle) => {
    toggle.addEventListener("click", (event) => {
      const _this = event.currentTarget;

      if (document.body.classList.contains("retheme-dark-scheme")) {
        // Dark mode deactiv
        document.body.classList.remove("retheme-dark-scheme");
        document.body.classList.add("retheme-default-scheme");

        localStorage.setItem("retheme-dark-scheme", "deactive");

        toggles.forEach((toggle) => {
          toggle.classList.remove("is-active");
        });
        
      } else {
        // Dark mode active
        document.body.classList.add("retheme-dark-scheme");
        document.body.classList.remove("retheme-default-scheme");

        localStorage.setItem("retheme-dark-scheme", "active");

        toggles.forEach((toggle) => {
          toggle.classList.add("is-active");
        });

      }
    });
  });
}
darkModeSwitch();