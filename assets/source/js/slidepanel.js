/**
 * Retheme Slidepanel
 * @version 3.1.0
 */
class slidepanel {
  constructor() {
    this.trigger = ".js-slidepanel";
    this.component = ".rt-slidepanel";
    this.overlay = ".rt-slidepanel__overlay";
    this.inner = ".rt-slidepanel__inner";
    this.close = ".js-slidepanel-close";

    this.listener();
    this.delete();
  }

  open(el) {
    el.element.style.display = "block";
    el.element.classList.add("is-active");
    animateCSS(el.overlay, "fadeIn", "500");
    animateCSS(el.inner, el.animateIn, "500");

    // add listener to disable scroll
    document.querySelector("html").style.overflowY = "hidden";
  }

  delete(el) {
    const closes = document.querySelectorAll(this.close);

    // close panel
    closes.forEach((close) => {
      close.addEventListener("click", (event) => {
        const target = close.closest(this.component);

        // panel open from point
        if (target.classList.contains("rt-slidepanel--left")) {
          var flowOut = "slideOutLeft";
        } else if (target.classList.contains("rt-slidepanel--bottom")) {
          var flowOut = "slideOutDown";
        } else if (target.classList.contains("rt-slidepanel--top")) {
          var flowOut = "slideOutUp";
        } else {
          var flowOut = "slideOutRight";
        }

        animateCSS(target.querySelector(this.inner), flowOut, "500");
        animateCSS(target.querySelector(this.overlay), "fadeOut", "500").then(() => {
          target.style.display = "none";
          target.classList.remove("is-active");
        });

        // Remove active class for all trigger
        document.querySelectorAll(this.trigger).forEach((trigger) => {
          trigger.classList.remove("is-active");
        });

        // Remove listener to re-enable scroll
        document.querySelector("html").style.overflowY = "auto";
      });
    });
  }

  listener() {
    const slidepanels = document.querySelectorAll(this.trigger);

    slidepanels.forEach((slidepanel) => {
      const dataTarget = slidepanel.getAttribute("data-target")? slidepanel.getAttribute("data-target"): slidepanel.getAttribute("href");
      const target = document.querySelector(dataTarget);

      if (target == null) {
        return false;
      }

      // panel open from point
      if (target.classList.contains("rt-slidepanel--left")) {
        var flowIn = "slideInLeft";
      } else if (target.classList.contains("rt-slidepanel--bottom")) {
        var flowIn = "slideInUp";
      } else if (target.classList.contains("rt-slidepanel--top")) {
        var flowIn = "slideInDown";
      } else {
        var flowIn = "slideInRight";
      }

      // open
      slidepanel.addEventListener("click", (event) => {
        event.preventDefault();
        const _this = event.currentTarget;

        if (_this.classList.contains("disabled")) {
          return false;
        }

        // Run open action
        this.open({
          element: target,
          overlay: target.querySelector(this.overlay),
          inner: target.querySelector(this.inner),
          animateIn: flowIn,
        });

          // Add css active class
          _this.classList.add("is-active");

      });
    });
  }

  /**
   * open automatic if windows load
   * @param {id component panel} id
   */
  load(id) {
    const slidepanels = document.querySelectorAll(id);

    slidepanels.forEach((slidepanel) => {

      // panel open from point
      if (slidepanel.classList.contains("rt-slidepanel--left")) {
        var flowIn = "slideInLeft";
      } else if (slidepanel.classList.contains("rt-slidepanel--bottom")) {
        var flowIn = "slideInUp";
      } else if (slidepanel.classList.contains("rt-slidepanel--top")) {
        var flowIn = "slideInDown";
      } else {
        var flowIn = "slideInRight";
      }

      this.open({
          element: slidepanel,
          overlay: slidepanel.querySelector(this.overlay),
          inner: slidepanel.querySelector(this.inner),
          animateIn: flowIn,
        });

      slidepanel.classList.add("is-active");
    });
  }
}

new slidepanel();