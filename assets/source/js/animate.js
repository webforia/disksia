/**
 * Retheme Animate V 3.0.0
 * helper function base animate.css
 */

/*=================================================;
/* SLIDE UP
/*================================================= */
function slideUp(target, duration = 500) {
    target.style.transitionProperty = "height, margin, padding";
    target.style.transitionDuration = duration + "ms";
    target.style.boxSizing = "border-box";
    target.style.height = target.offsetHeight + "px";
    target.offsetHeight;
    target.style.overflow = "hidden";
    target.style.height = 0;
    target.style.paddingTop = 0;
    target.style.paddingBottom = 0;
    target.style.marginTop = 0;
    target.style.marginBottom = 0;
    window.setTimeout(() => {
      target.style.display = "none";
      target.style.removeProperty("height");
      target.style.removeProperty("padding-top");
      target.style.removeProperty("padding-bottom");
      target.style.removeProperty("margin-top");
      target.style.removeProperty("margin-bottom");
      target.style.removeProperty("overflow");
      target.style.removeProperty("transition-duration");
      target.style.removeProperty("transition-property");
    }, duration);
  }
  
  /*=================================================;
  /* SLIDE DOWN
  /*================================================= */
  function slideDown(target, duration = 500) {
    target.style.removeProperty("display");
    let display = window.getComputedStyle(target).display;
    if (display === "none") display = "block";
    target.style.display = display;
    let height = target.offsetHeight;
    target.style.overflow = "hidden";
    target.style.height = 0;
    target.style.paddingTop = 0;
    target.style.paddingBottom = 0;
    target.style.marginTop = 0;
    target.style.marginBottom = 0;
    target.offsetHeight;
    target.style.boxSizing = "border-box";
    target.style.transitionProperty = "height, margin, padding";
    target.style.transitionDuration = duration + "ms";
    target.style.height = height + "px";
    target.style.removeProperty("padding-top");
    target.style.removeProperty("padding-bottom");
    target.style.removeProperty("margin-top");
    target.style.removeProperty("margin-bottom");
    window.setTimeout(() => {
      target.style.removeProperty("height");
      target.style.removeProperty("overflow");
      target.style.removeProperty("transition-duration");
      target.style.removeProperty("transition-property");
    }, duration);
  }
  
  /*=================================================;
  /* add animate for ES6
  /*================================================= */
  function animateCSS(element, animation, duration, delay) {
    // We create a Promise and return it
  
    return new Promise((resolve, reject) => {
      const animationName = `animate__${animation}`;
  
      if (element) {
        element.classList.add(
          "animate",
          "animate__animated",
          animationName,
          "is-active"
        );
  
        if (duration) {
          element.style.setProperty("--animate-duration", `${duration}ms`);
        }
  
        if (delay) {
          element.style.setProperty("--animate-delay", delay);
        }
  
        // When the animation ends, we clean the classes and resolve the Promise
        function handleAnimationEnd() {
          if (animation.search("In") > 0) {
            element.classList.remove("animate__animated", animationName);
          } else {
            element.classList.remove(
              "animate",
              "animate__animated",
              animationName,
              "is-active"
            );
          }
          resolve("Animation ended");
        }
  
        element.addEventListener("animationend", handleAnimationEnd, {
          once: true,
        });
      }
    });
  }
  
  /*=================================================;
  /* add animate for JQuery
  /*================================================= */
  function jAnimateCSS(element, animation, duration, delay) {
    // We create a Promise and return it
    return new Promise((resolve, reject) => {
      const animationName = `animate__${animation}`;
  
      if (element.length) {
        element.addClass(`animate animate__animated  ${animationName} is-active`);
  
        if (duration) {
          element.css("--animate-duration", `${duration}ms`);
        }
  
        if (delay) {
          element.css("--animate-delay", delay);
        }
  
        // When the animation ends, we clean the classes and resolve the Promise
        function handleAnimationEnd() {
          if (animation.search("In") > 0) {
            element.removeClass(`animate__animated ${animationName}`);
          } else {
            element.removeClass(
              `animate animate__animated ${animationName} is-active`
            );
          }
  
          // remove animation
          element.off("animationend");
        }
  
        element.on("animationend", handleAnimationEnd);
      }
    });
  }
  
  /*=================================================;
  /* Toggle Animate
  /*================================================= */
  function toogleAnimate() {
    const triggers = document.querySelectorAll(".js-animate");
    const targets = document.querySelectorAll(".animate_toggle");
  
    triggers.forEach((trigger) => {
      const dataTarget = (trigger.getAttribute("data-target"))? trigger.getAttribute("data-target"): trigger.getAttribute("href");
      const targetGroups = (trigger.getAttribute("data-target"))? document.querySelectorAll(`[data-target="${dataTarget}"]`): document.querySelectorAll(`[href="${dataTarget}"]`);
      const target = document.querySelector(dataTarget);
      const closes = target.querySelectorAll(".js-animate-close");
  
      const options = {
        animateIn: target.getAttribute("data-animatein")
          ? target.getAttribute("data-animatein")
          : "fadeIn",
        animateOut: target.getAttribute("data-animateout")
          ? target.getAttribute("data-animateout")
          : "fadeOut",
        duration: target.getAttribute("data-duration")
          ? target.getAttribute("data-duration")
          : "300",
      };
  
      // open
      trigger.addEventListener("click", (event) => {
        const _this = event.currentTarget;

        event.preventDefault();
        
        if (_this.classList.contains("is-active")) {
          
          // remove all trigger active
          targetGroups.forEach(targetGroup => {
            targetGroup.classList.remove("is-active");
          });
  
          target.classList.remove("animate_toggle");
          
          if (options.animateOut == "slideUp") {
            slideUp(target);
          } else {
            animateCSS(target, options.animateOut, options.duration);
          }
        } else {
          targetGroups.forEach(targetGroup => {
            targetGroup.classList.add("is-active");
          });
  
          target.classList.add("animate_toggle");
          if (options.animateIn == "slideDown") {
            slideDown(target);
          } else {
            animateCSS(target, options.animateIn, options.duration);
          }
        }
      });
  
      // close if document clicked
      document.addEventListener("click", function (e) {
        const element = window.event.target;
        const targets = document.querySelectorAll(".animate_toggle");
        const triggers = document.querySelectorAll(".js-animate.is-active");
  
        if (
          (!element.classList.contains(".js-animate") &&
            !element.closest(".js-animate") &&
            !element.closest(".animate_toggle")) ||
          element.classList.contains("js-animate-close")
        ) {
          // remove all active trigger
          triggers.forEach((trigger) => {
            trigger.classList.remove("is-active");
          });
  
          // remove all active target
          targets.forEach((target) => {
            // option each target
            const options = {
              animateOut: target.getAttribute("data-animateout")
                ? target.getAttribute("data-animateout")
                : "fadeOut",
              duration: target.getAttribute("data-duration")
                ? target.getAttribute("data-duration")
                : "300",
            };
  
            target.classList.remove("animate_toggle");
  
            if (options.animateOut == "slideUp") {
              slideUp(target);
            } else {
              animateCSS(target, options.animateOut, options.duration);
            }
          });
        }
      });
  
      //
    });
  }
  toogleAnimate();
  
  /*=================================================;
  /* ANIMATE HOVER
  /*================================================= */
  // add class on element hover
  function animateHover() {
    const elements = document.querySelectorAll(".js-hover");
  
    elements.forEach((element) => {
      element.addEventListener("mouseenter", (event) => {
        const _this = event.currentTarget;
        _this.classList.add("is-hover");
      });
  
      element.addEventListener("mouseleave", (event) => {
        const _this = event.currentTarget;
  
        _this.classList.remove("is-hover");
      });
    });
  }
  animateHover();
  
  /*=================================================;
  /* ANIMATE CLICK
  /*================================================= */
  // add class on element hover
  function animateClick() {
    const elements = document.querySelectorAll(".js-click");
  
    elements.forEach((element) => {
      element.addEventListener("click", (event) => {
        event.preventDefault();
  
        const _this = event.currentTarget;
  
        if (_this.classList.contains("is-clicked")) {
          _this.classList.remove("is-clicked");
        } else {
          _this.classList.add("is-clicked");
        }
      });
    });
  }
  animateClick();
  
  /*=================================================;
  /* CHECKBOX TOGGLE DROPDOWN
  /*================================================= */
  function checkboxDropdown() {
    const checkboxs = document.querySelectorAll(".js-checkbox-toggle");
  
    checkboxs.forEach((checkbox) => {
      const getTarget = checkbox.getAttribute("data-target");
      const getChecked = checkbox.querySelector("input[type='checkbox']");
      const target = document.querySelector(getTarget);
  
      if (getChecked.checked == true) {
        target.style.display = "block";
      }
  
      getChecked.addEventListener("change", (event) => {
        const _this = event.currentTarget;
  
        if (getChecked.checked == true) {
          slideDown(target);
        } else {
          slideUp(target);
        }
      });
    });
  }
  checkboxDropdown();