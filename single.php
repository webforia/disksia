<?php get_header();?>

    <section class="page-wrapper">
        <div class="page-container">
            <div class="page-columns">
                <div class="page-content">

                <?php if (have_posts()): ?>
                    <?php while (have_posts()): the_post(); ?>

                    <article class="rt-single">
                        
                        <?php rt_get_template_part('single/category') ?>

                        <?php echo rt_get_breadrumbs() ?>

                        <h1 class="rt-single-title"><?php echo get_the_title() ?></h1>

                        <div class="rt-single-content-bar">
                            <?php rt_get_template_part('single/meta') ?>
                            <?php rt_get_template_part('single/share'); ?>
                        </div>

                        <?php rt_get_template_part('single/thumbnail') ?>

                        <div class="rt-single-content rt-entry-content">
                            
                            <?php the_content() ?>

                            <?php  wp_link_pages([
                                'before' => '<div class="rt-page-links">' . __( 'Pages:', RT_THEME_DOMAIN),
                                'after'  => '</div>',
                            ]); ?>
                        </div>
                        
                        <?php rt_get_template_part('single/tag') ?>

                        <?php rt_get_template_part('single/share-bar'); ?>

                    </article>
                    

                    <?php rt_get_template_part('single/related') ?>

                    <?php  if ( (comments_open() || get_comments_number()) && rt_get_single_option('comment')) : ?>
                        <?php comments_template(); ?>
                    <?php endif ?>

                
                    <?php endwhile; ?>
                <?php endif ?>

                </div>

                <?php rt_get_sidebar() ?>

            </div>
        </div>
    </section>


<?php get_footer();?>