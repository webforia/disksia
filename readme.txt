= 1.3.0 (release 22 Juni 2022) =
* New: Post style list pada tampilan mobile
* New: Menambahkan logo pada menu mobile
* New: Menambahkan social media pada menu mobile
* Improve: Ganti judul komentar dengan tag h2
* Improve: Codebase template single dan page

= 1.2.0 (release 14 Juni 2022) =
* FIX: Update scripts modal popup
* FIX: Update scripts slide mobile menu

= 1.1.1 (release 31 Mei 2022) =
* FIX: Singkronisasi trigger dark mode pada header

= 1.1.0 (release 30 Mei 2022) =
* NEW: Menambahkan fitur dark mode

= 1.0.0 (release 17 Mei 2022) =
* NEW: Release pertama