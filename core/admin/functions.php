<?php
/*=================================================;
/* ADMIN - TEMPLATE PART
/*================================================= */
function rt_admin_get_template_part($template, $args = '', $name = '')
{
    get_template_part("core/admin/views/{$template}", $name, $args);

}

/*=================================================;
/* GET PAGE ADMIN
/*================================================= */
function rt_admin_get_page()
{
    return !empty($_GET['page']) ? $_GET['page'] : '';
}

/**
 * get admin page url
 *
 * @param [type] $url
 * @return url page
 */
function rt_admin_page_url($url)
{
    return admin_url() . "admin.php?page={$url}";
}

/*=================================================;
/* SHOW NOTIF AFTER THEME ACTIVE
/*================================================= */
function rt_admin_notif_stater()
{
    add_action('admin_notices', function () {
        rt_admin_get_template_part('global/starter');
    });
}
add_action('after_switch_theme', 'rt_admin_notif_stater');

/*=================================================;
/* ADMIN NOTIF HELPER
/*================================================= */
function rt_admin_notification($message, $notif = '')
{
    if ($notif == 'error') {
        $message = "<div class='notification is-warning'>{$message} <button class='delete'></button></div>";
    } else {
        $message = "<div class='notification is-success'>{$message} <button class='delete'></button></div>";
    }

    return $message;
}
