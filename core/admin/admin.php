<?php
class Retheme_Admin_Dashboard
{

    public function __construct()
    {
        add_action('admin_init', array($this, 'include_part'));
        add_action('admin_enqueue_scripts', array($this, 'register_scripts'), 999);
        add_action('admin_notices', array($this, 'tabs'), 1);
        add_action('admin_menu', array($this, 'add_menu_panel'));
        add_action('acf/init', array($this, 'add_theme_panel'));

    }

    public function include_part()
    {
        include_once dirname(__FILE__) . '/functions.php';
    }

    public function register_scripts()
    {
        // Main Style
        wp_enqueue_style('retheme-admin', get_template_directory_uri() . '/core/admin/assets/css/retheme-admin.css', false, '2.0.0');
        wp_enqueue_script('retheme-admin', get_template_directory_uri() . '/core/admin/assets/js/retheme-admin.js', array('jquery'), '1.0.0', true);
    }

    public function tabs()
    {
        $page = !empty($_GET['page']) ? $_GET['page'] : false;

        if ($page == 'theme-license'
            || $page == 'theme-panel'
            || $page == 'theme-pro') {
            rt_admin_get_template_part('global/tab');
        }

    }

    /**
     * Add menu panel
     */
    public function add_menu_panel()
    {

        add_menu_page('Theme Panel', 'Theme Panel', 'manage_options', 'theme-panel', false, '', 999);

        add_submenu_page('theme-panel', 'Dashboard', 'Dashboard', 'manage_options', 'theme-panel', function () {
            rt_admin_get_template_part('dashboard');
        }, 1);

        if (rt_is_premium_plan()) {
            add_submenu_page('theme-panel', 'License', 'License', 'manage_options', 'theme-license', function () {
                rt_admin_get_template_part('license');
            }, 99);
        }

        if (rt_is_free()) {
            add_submenu_page('theme-panel', 'Upgrade Pro', '<span class="rta-menu-pro">Upgrade Pro <i class="fa fa-star"></i></span>', 'manage_options', 'theme-pro', function () {
                rt_admin_get_template_part('upgrade-pro');
            }, 99);
        }

    }

    /**
     * Added ACF option panel
     *
     * @return void
     */
    public function add_theme_panel()
    {
        if (function_exists('acf_add_options_page')) {

            acf_add_options_page(array(
                'page_title' => __('Site Options', RT_THEME_DOMAIN),
                'menu_title' => __('Site Options', RT_THEME_DOMAIN),
                'menu_slug' => 'theme-settings',
                'parent_slug' => 'index.php',
                'position' => 1,
                'capability' => 'edit_posts',
                'redirect' => false,
            ));
        }

    }
}

new Retheme_Admin_Dashboard();
