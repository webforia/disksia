document.addEventListener("DOMContentLoaded", () => {
  (document.querySelectorAll(".notification .delete") || []).forEach(
    ($delete) => {
      const $notification = $delete.parentNode;

      $delete.addEventListener("click", () => {
        $notification.parentNode.removeChild($notification);
      });
    }
  );

  (document.querySelectorAll(".modal .delete") || []).forEach(
    ($delete) => {
      const $modal = $delete.closest('.modal');

      $delete.addEventListener("click", () => {
        $modal.style.display = 'none';
      });
    }
  );
});
