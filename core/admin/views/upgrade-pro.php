<div class="bulma">
    <h1 class="title">Perbandingan - Lite VS Pro</h1>
    <div class="rtc-table-pricing">
        <table class="table is-bordered is-striped is-narrow is-hoverable">
            <th>
                <td>Lite</td>
                <td>Pro</td>
            </th>
            <tr>
                <td>Full Header Builder</td>
                <td>Lite (5 element)</td>
                <td>Full (11 Element)</td>
            </tr>
            <tr>
                <td>Premium Support</td>
                <td><i class="fas fa-times has-text-danger"></i></td>
                <td>Forum & Chat</td>
            </tr>

            <tr>
                <td>Sticky Header</td>
                <td><i class="fas fa-times has-text-danger"></i></td>
                <td><i class="fas fa-check has-text-success"></i></td>
            </tr>
            <tr>
                <td>Import Demo</td>
                <td><i class="fas fa-check has-text-success"></i></td>
                <td><i class="fas fa-check has-text-success"></i></td>
            </tr>
            <tr>
                <td>Elementor Widgets</td>
                <td><i class="fas fa-times has-text-danger"></i></td>
                <td><i class="fas fa-check has-text-success"></i></td>
            </tr>
             <tr>
                <td>Product Layout</td>
                <td><i class="fas fa-times has-text-danger"></i></td>
                <td><i class="fas fa-check has-text-success"></i></td>
            </tr>
            <tr>
                <td>Optimasi Halaman Checkout</td>
                <td><i class="fas fa-times has-text-danger"></i></td>
                <td><i class="fas fa-check has-text-success"></i></td>
            </tr>
             <tr>
                <td>Optimasi Mobile Layout</td>
                <td><i class="fas fa-times has-text-danger"></i></td>
                <td><i class="fas fa-check has-text-success"></i></td>
            </tr>
             <tr>
                <td>Order Notif - Menampilkan transaksi terakhir</td>
                <td><i class="fas fa-times has-text-danger"></i></td>
                <td><i class="fas fa-check has-text-success"></i></td>
            </tr>
            <tr>
                <td>Floating Add To Cart pada halaman produk</td>
                <td><i class="fas fa-times has-text-danger"></i></td>
                <td><i class="fas fa-check has-text-success"></i></td>
            </tr>
             <tr>
                <td>WhatsApp Chat</td>
                <td><i class="fas fa-times has-text-danger"></i></td>
                <td><i class="fas fa-check has-text-success"></i></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td><a href="<?php echo rt_get_theme('product-url') ?>"" target="_blank" class="button is-info" style="margin: 10px 0">Upgrade ke versi pro</a></td>
            </tr>
        </table>
    </div>
</div>
