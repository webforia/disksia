
<?php
// Product attribute
$product_name = RT_THEME_NAME;
$product_slug = RT_THEME_SLUG;
$product_url = RT_THEME_DOC;
$product_version = RT_THEME_VERSION;

// Form value
$license_key = !empty($_POST['product_license_key']) ? sanitize_text_field($_POST['product_license_key']) : '';
$license_action = !empty($_POST['product_license_key']) ? sanitize_text_field($_POST['product_license_submit']) : '';

// API Manager
$license = new Retheme\Activation(array(
    'product_key' => $license_key,
));

$manager = array('result' => '');
$expired_date = $license->get_date_expired('format');

if ($license_action) {
    if ($license_action == 'activate') {
        $manager = $license->activate();
    } else {
        $manager = $license->deactivate();
    }
}

?>
<div class="bulma theme-panel">
    <section class="page-header">
        <h1 class="title"><?php echo $product_name ?> Lisensi</h1>
    </section>

    <div class="columns">
        <div class="column is-6">

            <?php

            if ($manager['result'] == 'error') {
                echo rt_admin_notification($manager['message'], 'error');
            }
            if ($manager['result'] == 'success') {
                echo rt_admin_notification($manager['message']);
            }
            ?>

            <div class="panel is-light">

                <div class="panel-heading">
                    Lisensi

                    <?php if ($license->is_local()): ?>
                        <span class="tag is-success is-capitalized" style="float: right">Local Lisensi</span>
                    <?php elseif ($license->status() === 'active'): ?>
                        <span class="tag is-success is-capitalized" style="float: right">Active</span>
                    <?php else: ?>
                        <span class="tag is-warning is-capitalized" style="float: right"><?php echo $license->status() ?></span>
                    <?php endif?>

                </div>

                <div class="panel-block">
                    <div class="rta-form-wrapper" style="width: 100%">
                       <div class="content mb-5">
                            <?php if ($license->is_local()): ?>
                                <p>Anda sedang menggunakan theme ini di localhost server, semua fitur <strong>Premium</strong> akan aktif pada server lokal. Anda wajib memasukan lisensi ketika di hosting atau server online </p>
                            <?php elseif ($license->status() === 'active'): ?>
                                <p>Tema Anda berhasil di aktifkan, silakan cek <a href="<?php echo rt_get_theme('product-docs') ?>" target="_blank">disini</a> untuk melihat panduan tema</p>
                            <?php elseif ($license->status() === 'expired'): ?>
                                <p>Lisensi tema Anda berakhir pada tanggal <?php echo $expired_date ?>, silakan lakukan perpanjangan lisensi untuk tetap mendapatkan update dan support.</p>
                            <?php else: ?>
                                <p>Masukan kunci lisensi Anda untuk dapat menggunakan semua fitur premium, update, dan support. <a target="_blank" href="<?php echo esc_url($product_url) ?>">Dapatkan premium lisensi disini</a></p>
                            <?php endif?>

                            <?php if($license->is_premium() && $license->status() !== 'expired'): ?>
                                <p>Aktif Hingga: <?php echo $expired_date ?></p>
                            <?php endif ?>
                       </div>

                        <form action="#" method="post">
                            <div class="field">
                                <label class="label">Lisensi Key</label>

                                <?php if ($license->is_local()): ?>
                                    <input class="input" id="product_license_key" name="product_license_key" type="password" value="localhost" disabled />
                                <?php else: ?>
                                    <?php if($license->is_premium()): ?>
                                        <input class="input" type="password" value="<?php echo get_option("{$product_slug}_key"); ?>" disabled />
                                        <input class="input" id="product_license_key" name="product_license_key" type="hidden" value="<?php echo get_option("{$product_slug}_key"); ?>"/>
                                    <?php else: ?>
                                        <input class="input" id="product_license_key" name="product_license_key" type="text" value="<?php echo get_option("{$product_slug}_key"); ?>"/>
                                    <?php endif ?>
                                <?php endif?>

                            </div>
                            <div style="margin-bottom: 20px">

                                <?php if ($license->is_local()): ?>
                                    <input class="button is-info" name="product_license_submit" type="submit" value="Activate License" disabled />
                                <?php else: ?>
                                    <?php if($license->is_premium()): ?>
                                        <button class="button" name="product_license_submit" type="submit" value="reset">Reset</button>
                                    <?php else: ?>
                                        <button class="button is-info" name="product_license_submit" type="submit" value="activate">Aktifkan</button>
                                    <?php endif ?>
                                <?php endif?>

                            </div>
                        </form>

                    </div>

                </div>

            </div>
        </div>
    </div>

</div>