<div class="bulma theme-panel">
    <section class="page-header">
        <h1 class="title"><?php echo RT_THEME_NAME ?> Dashboard </h1>
        <h2 class="subtitle">Version <?php echo RT_THEME_VERSION ?></h2>
    </section>
    <section class="page-info">
        <div class="container">
            <div class="columns is-multiline">

                <div class="column is-4">
                    <div class="panel is-light">
                        <div class="panel-heading">Butuh Bantuan?</div>
                        <div class="panel-block">
                            <ul>
                                <li>Jasa pembuatan toko online</li>
                                <li>Installasi tema</li>
                                <li>Mempercepat Loading Website</li>
                                <li>Fix Bug</li>
                                <li>Support Teknis</li>
                            </ul>
                        </div>
                        <div class="panel-block">
                            <a href="<?php echo RT_THEME_URL ?>" target="_blank" class="button is-info"> Hubungi Kami </a>
                        </div>
                    </div>
                </div>

                <div class="column is-4">
                    <div class="panel is-light">
                        <div class="panel-heading">Panduan</div>
                        <div class="panel-block">
                            <p class="is-size-6">Pelajari lebih lanjut cara menggunakan Tema</p>
                        </div>
                        <div class="panel-block">
                           <a href="<?php echo RT_THEME_DOC ?>" target="_blank" class="button is-info"> Pelajari Sekarang </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
</div>