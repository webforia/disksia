<div class="bulma">

  <div class="tabs rta-theme-menu">
    <ul>
      <li class="<?php echo (rt_admin_get_page() == 'theme-panel')? 'is-active':''?>"><a href="<?php echo rt_admin_page_url('theme-panel')?>">Dashboard</a></li>

      <?php if(rt_is_premium_plan()): ?>
      <li class="<?php echo (rt_admin_get_page() == 'theme-license')? 'is-active':''?>"><a href="<?php echo rt_admin_page_url('theme-license')?>">License</a></li>
      <?php endif ?>
      
   
      <li class=""><a href="<?php echo rt_admin_page_url('merlin&step=content')?>">Import Demo</a></li>


      <?php if(rt_is_free()): ?>
      <li class="<?php echo (rt_admin_get_page() == 'theme-pro')? 'is-active':''?>"><a href="<?php echo rt_admin_page_url('theme-pro')?>"><span class="has-text-warning">Upgrade Pro <i class="fa fa-star"></i></span></a></li>
      <?php endif ?>

    </ul>
  </div>
  
</div>