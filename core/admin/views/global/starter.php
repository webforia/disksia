
<div class="notice notice-info is-dismissible">
    <div class="bulma">
        <h3 class="title is-size-5">Terimakasih telah memasang tema <?php echo rt_get_theme('product-name') ?> </h3>
        <div class="content">
            <p>Silakan kunjungi panel customizer untuk melakukan penyesuaian theme </p>
            <a class="button is-small" href="<?php echo rt_get_theme('product-docs') ?>" target="_blank">Panduan</a>
            <a class="button is-info is-small" href="<?php echo esc_url( admin_url( 'customize.php' ) ); ?>">Link to Customizer</a>
        </div>
    </div>
    <button type='button' class='notice-dismiss'></button>
</div>