<?php
$license = new \Retheme\Activation;
$expired_date = $license->get_date_expired('format');
?>
<div class="bulma">

<div class="modal" style="display:block">
  <div class="modal-background"></div>
  <div class="modal-card" style="margin-top: 130px;">
    <header class="modal-card-head">
      <p class="modal-card-title">Saudagar WP Trial Expired</p>
      <button class="delete" aria-label="close"></button>
    </header>
    <section class="modal-card-body">
      <p class="is-size-6">Masa uji coba tema telah berakhir pada tanggal <strong><?php echo $expired_date ?></strong>. Untuk dapat terus menggunakan tema Saudagar WP, Anda memerlukan lisensi Premium. <a href="https://saudagarwp.com/harga/">Dapatkan lisensi premium disini</a></p>
      <p class="is-size-8" style="margin-top:10px">*Ganti tema untuk menghilangkan notifikasi</p>
    </section>
    <footer class="modal-card-foot">
      <a class="button" href="<?php echo esc_url(admin_url('themes.php')); ?>">Ganti Tema</a>
      <a class="button is-info" href="<?php echo esc_url(admin_url('admin.php?page=theme-license')); ?>">Masukan Lisensi Saudagar WP</a>
    </footer>
  </div>
</div>

</div>