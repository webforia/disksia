<?php
/*=================================================;
/* WOCOMMERCE INIT
/*================================================= */
function rt_woocommerce_support()
{
    add_theme_support('woocommerce');
    add_theme_support( 'wc-product-gallery-lightbox' );
    add_theme_support( 'wc-product-gallery-lightbox' );
    add_theme_support( 'wc-product-gallery-slider' );
}
add_action('after_setup_theme', 'rt_woocommerce_support');

/*=================================================;
/* TEMPLATE LAYOUT
/*================================================= */
function rt_woocommerce_template_layout($layout)
{
    if (rt_is_woocommerce('pages') || rt_is_woocommerce('cart') || rt_is_woocommerce('checkout') ||  rt_is_woocommerce('account')) {
        $layout = 'full-width';
    }

    if (rt_is_woocommerce('shop')) {
        $layout = 'sidebar-right';
    }

    return $layout;
}
add_filter('template_layout', 'rt_woocommerce_template_layout');

/*=================================================;
/* REGISTER WIDGET
/*================================================= */
function rt_woocommerce_register_widget()
{

    register_sidebar([
        'name' => __('WooCommerce Sidebar', RT_THEME_DOMAIN),
        'id' => 'retheme_woocommerce_sidebar',
        'before_widget' => '<div id="%1$s" class="rt-widget rt-widget--aside %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="rt-widget__header"><h4 class="rt-widget__title">',
        'after_title' => '</h4></div>',
    ]);

}
add_action('widgets_init', 'rt_woocommerce_register_widget');

/*=================================================;
/* REMOVE WOOCOMMERCE PAGE TITLE
/*================================================= */
add_filter( 'woocommerce_show_page_title', '__return_null' );


/*=================================================;
/* FIELD - ADD CLASS
/*=================================================*/
function rt_woocommerce_form_field_args($args, $key, $value = null)
{
    $class = 'rt-form';

    if ($args['type'] !== 'checkbox' && $args['type'] !== 'radio') {
        $args['class'][] = $class;
        $args['label_class'][] = 'rt-form__label';
        $args['input_class'][] = 'rt-form__input';
    }

    return $args;

  
}
add_filter('woocommerce_form_field_args', 'rt_woocommerce_form_field_args', 10, 3);