<?php
/**
 * @author : Webforia Studio
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Connect extends Customizer_Base
{

    public function __construct()
    {
        $this->set_section();
        $this->add_socmed();
        $this->add_share();

    }

    public function set_section()
    {
        $this->add_panel('connect_panel', [
            'title' => __('Connect', 'retheme-admin'),
            'priority' => 100,
        ]);

        $this->add_section('connect_panel', [
            'social' => [__('Social Media', 'retheme-admin')],
            'share' => [__('Share', 'retheme-admin')],
        ]);
    }

    public function add_socmed()
    {
        $section = 'social_section';

        $this->add_field([
            'type' => 'link',
            'settings' => 'socmed_facebook',
            'label' => __('Facebook', 'retheme-admin'),
            'default' => '#',
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'link',
            'settings' => 'socmed_instagram',
            'label' => __('Instagram', 'retheme-admin'),
            'default' => '#',
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'link',
            'settings' => 'socmed_youtube',
            'label' => __('Youtube', 'retheme-admin'),
            'default' => '#',
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'link',
            'settings' => 'socmed_twitter',
            'label' => __('Twitter', 'retheme-admin'),
            'default' => '#',
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'link',
            'settings' => 'socmed_pinterest',
            'label' => __('Pinterest', 'retheme-admin'),
            'default' => '#',
            'section' => $section,
        ]);
        if (rt_is_premium()) {
            $this->add_field([
                'type' => 'link',
                'settings' => 'socmed_tiktok',
                'label' => __('Tiktok', 'retheme-admin'),
                'default' => '#',
                'section' => $section,
            ]);
        }
    }

    public function add_share()
    {
        $section = 'share_section';

        $this->add_field([
            'type' => 'toggle',
            'settings' => 'connect_share_facebook',
            'label' => __('Facebook', 'retheme-admin'),
            'section' => $section,
            'default' => true,
        ]);
        $this->add_field([
            'type' => 'toggle',
            'settings' => 'connect_share_twitter',
            'label' => __('Twitter', 'retheme-admin'),
            'section' => $section,
            'default' => true,
        ]);

        $this->add_field([
            'type' => 'toggle',
            'settings' => 'connect_share_pinterest',
            'label' => __('Pinterest', 'retheme-admin'),
            'section' => $section,
            'default' => true,
        ]);

        $this->add_field([
            'type' => 'toggle',
            'settings' => 'connect_share_email',
            'label' => __('Email', 'retheme-admin'),
            'section' => $section,
            'default' => true,
        ]);
        
        if (rt_is_premium()) {
            $this->add_field([
                'type' => 'toggle',
                'settings' => 'connect_share_whatsapp',
                'label' => __('WhatsApp', 'retheme-admin'),
                'section' => $section,
                'default' => true,
            ]);
        }

    }

// end class
}

new Connect;
