<?php
/**
 * @author : Webforia Studio
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Header extends Customizer_Base
{

    public function __construct()
    {
        $this->set_section();
        $this->add_header_main();
        $this->add_header_top();
        $this->add_header_mobile();

    }

    public function set_section()
    {
        $this->add_panel('header_panel', [
            'title' => __('Header', 'retheme-admin'),
            'priority' => 60,
        ]);
        if (rt_is_premium()) {
            $element = [
                'header_top' => [__('Top Header', 'retheme-admin')],
                'header_main' => [__('Main Header', 'retheme-admin')],
                'header_mobile' => [__('Mobile Header', 'retheme-admin')],
            ];
        } else {
            $element = [
                'header_main' => [__('Main Header', 'retheme-admin')],
                'header_mobile' => [__('Mobile Header', 'retheme-admin')],
            ];
        }
        $this->add_section('header_panel', $element);
    }

    public function add_header_main()
    {
        $section = 'header_main_section';

        if (rt_is_premium()) {
            $this->add_field([
                'type' => 'radio-image',
                'section' => $section,
                'settings' => 'header_layout',
                'label' => __('Layout', 'retheme-admin'),
                'default' => 'header-1',
                'choices' => [
                    'header-1' => get_template_directory_uri() . '/core/customizer/assets/img/header-1.png',
                    'header-2' => get_template_directory_uri() . '/core/customizer/assets/img/header-2.png',
                    'header-3' => get_template_directory_uri() . '/core/customizer/assets/img/header-3.png',
                    'header-4' => get_template_directory_uri() . '/core/customizer/assets/img/header-4.png',
                    'header-5' => get_template_directory_uri() . '/core/customizer/assets/img/header-5.png',
                ],
            ]);

            $this->add_divinder([
                'section' => $section,
            ]);
        }

        $this->add_field([
            'type' => 'color',
            'choices' => ['alpha' => true],
            'label' => __('Text Color', 'retheme-admin'),
            'settings' => 'header_main_color',
            'default' => rt_get_theme('global_font_color')['secondary'],
            'section' => $section,
            'transport' => 'auto',
            'output' => [
                [
                    'element' => '.retheme-default-scheme .page-header__main, .retheme-default-scheme .page-header__sticky, .retheme-default-scheme .page-header__middle',
                    'property' => '--theme-font-color-secondary',
                ],
                [
                    'element' => '.retheme-default-scheme .page-header__main, .retheme-default-scheme .page-header__sticky, .retheme-default-scheme .page-header__middle',
                    'property' => '--theme-link',
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'color',
            'choices' => ['alpha' => true],
            'label' => __('Accent Color', 'retheme-admin'),
            'settings' => 'header_main_color_active',
            'default' => rt_get_theme('global_color_link')['hover'],
            'section' => $section,
            'transport' => 'auto',
            'output' => [
                [
                    'element' => '.retheme-default-scheme .page-header__main, .retheme-default-scheme .page-header__sticky, .retheme-default-scheme .page-header__middle',
                    'property' => '--theme-link-active',
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'color',
            'choices' => ['alpha' => true],
            'label' => __('Background', 'retheme-admin'),
            'settings' => 'header_main_background',
            'default' => rt_get_theme('global_background_scheme')['primary'],
            'section' => $section,
            'transport' => 'auto',
            'output' => [
                [
                    'element' => '.retheme-default-scheme .page-header__main, .retheme-default-scheme .page-header__sticky, .retheme-default-scheme .page-header__middle',
                    'property' => '--theme-background-primary',
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'color',
            'choices' => ['alpha' => true],
            'label' => __('Border Color', 'retheme-admin'),
            'settings' => 'header_main_border_color',
            'default' => rt_get_theme('global_border_color'),
            'section' => $section,
            'transport' => 'auto',
            'output' => [
                [
                    'element' => '.retheme-default-scheme .page-header__main, .retheme-default-scheme .page-header__sticky, .retheme-default-scheme .page-header__middle',
                    'property' => '--theme-border-color',
                ],
            ],
        ]);

        $this->add_divinder([
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'typography',
            'settings' => 'header_main_typography',
            'label' => __('Typography', 'retheme-admin'),
            'section' => $section,
            'default' => [
                'font-family' => '',
                'variant' => '700',
                'font-size' => '15px',
                'text-transform' => 'none',
            ],
            'output' => [
                [
                    'element' => '.page-header__main, .page-header__sticky',
                ],

            ],
            'transport' => 'auto',
        ]);

    }

    public function add_header_top()
    {
        $section = 'header_top_section';

        $this->add_field([
            'type' => 'toggle',
            'label' => __('Header Top', 'retheme-admin'),
            'settings' => 'header_top',
            'default' => false,
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'toggle',
            'label' => __('Menu', 'retheme-admin'),
            'settings' => 'header_top_menu',
            'default' => true,
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'toggle',
            'label' => __('Social Media', 'retheme-admin'),
            'settings' => 'header_top_socmed',
            'default' => true,
            'section' => $section,
        ]);

        $this->add_divinder([
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'color',
            'choices' => ['alpha' => true],
            'label' => __('Text Color', 'retheme-admin'),
            'settings' => 'header_top_color',
            'default' => 'rgba(255, 255, 255, 0.87)',
            'section' => $section,
            'transport' => 'auto',
            'output' => [
                [
                    'element' => '.retheme-default-scheme .page-header__top',
                    'property' => '--theme-font-color-secondary',
                ],
                [
                    'element' => '.retheme-default-scheme .page-header__top',
                    'property' => '--theme-link',
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'color',
            'choices' => ['alpha' => true],
            'label' => __('Accent Color', 'retheme-admin'),
            'settings' => 'header_top_color_active',
            'default' => '#ffffff',
            'section' => $section,
            'transport' => 'auto',
            'output' => [
                [
                    'element' => '.retheme-default-scheme .page-header__top',
                    'property' => '--theme-link-active',
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'color',
            'choices' => ['alpha' => true],
            'label' => __('Background', 'retheme-admin'),
            'settings' => 'header_top_background',
            'default' => '#222222',
            'section' => $section,
            'transport' => 'auto',
            'output' => [
                [
                    'element' => '.retheme-default-scheme .page-header__top',
                    'property' => '--theme-background-secondary',
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'color',
            'choices' => ['alpha' => true],
            'label' => __('Border Color', 'retheme-admin'),
            'settings' => 'header_top_border_color',
            'default' => rt_get_theme('global_border_color'),
            'section' => $section,
            'transport' => 'auto',
            'output' => [
                [
                    'element' => '.retheme-default-scheme .page-header__top',
                    'property' => '--theme-border-color',
                ],
            ],
        ]);
    }

    public function add_header_mobile()
    {
        $section = 'header_mobile_section';

        $this->add_field([
            'type' => 'color',
            'choices' => ['alpha' => true],
            'label' => __('Text Color', 'retheme-admin'),
            'settings' => 'header_mobile_color',
            'default' => rt_get_theme('global_font_color')['secondary'],
            'section' => $section,
            'transport' => 'auto',
            'output' => [
                [
                    'element' => '.retheme-default-scheme .page-header-mobile__main',
                    'property' => '--theme-font-color-secondary',
                ],
                [
                    'element' => '.retheme-default-scheme .page-header-mobile__main',
                    'property' => '--theme-link',
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'color',
            'choices' => ['alpha' => true],
            'label' => __('Accent Color', 'retheme-admin'),
            'settings' => 'header_mobile_color_active',
            'default' => rt_get_theme('global_color_link')['hover'],
            'section' => $section,
            'transport' => 'auto',
            'output' => [
                [
                    'element' => '.page-header-mobile__main',
                    'property' => '--theme-link-active',
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'color',
            'choices' => ['alpha' => true],
            'label' => __('Background', 'retheme-admin'),
            'settings' => 'header_mobile_background',
            'default' => rt_get_theme('global_background_scheme')['primary'],
            'section' => $section,
            'transport' => 'auto',
            'output' => [
                [
                    'element' => '.retheme-default-scheme .page-header-mobile__main, 
                                    .retheme-default-scheme #mobile_offcanvas_menu .rt-slidepanel__header',
                    'property' => '--theme-background-primary',
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'color',
            'choices' => ['alpha' => true],
            'label' => __('Border Color', 'retheme-admin'),
            'settings' => 'header_mobile_border_color',
            'default' => rt_get_theme('global_border_color'),
            'section' => $section,
            'transport' => 'auto',
            'output' => [
                [
                    'element' => '.retheme-default-scheme .page-header-mobile__main, 
                                    .retheme-default-scheme #mobile_offcanvas_menu .rt-slidepanel__header',
                    'property' => '--theme-border-color',
                ],
            ],
        ]);
    }

// end class
}

new Header;
