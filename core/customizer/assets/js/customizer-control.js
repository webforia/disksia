/**
 * Handle control custumizer panel
 * 
 * @version 2.0.0
 */
jQuery(document).ready( ($) => {

  wp.customize.control.each((control) => {
    var getDevice = control.params.device;
    var getClass = control.params.class;
    var selector = $(control.selector);
    var deviceTemplate =
      '<ul class="responsive-switchers"><li class="desktop"><span class="preview-desktop" data-device="desktop"><i class="dashicons dashicons-desktop"></i></span></li><li class="tablet"><span class="preview-tablet" data-device="tablet"><i class="dashicons dashicons-tablet"></i></span></li><li class="mobile"><span class="preview-mobile" data-device="mobile"><i class="dashicons dashicons-smartphone"></i></span></li></ul>';

    // add class header control
    selector.addClass("control-" + getClass);

    // add class responsive control
    if (typeof getDevice == "string") {
      selector.addClass("control-responsive control-device-" + getDevice);
    }

    // add device icon trigger on label responsive control
    control.deferred.embedded.done(() =>{
      control.container.find("label").append(deviceTemplate);
    });
  });

  /*=================================================
  * DEVICE ACTION
  /*================================================= */
  var panel = $(".wp-full-overlay");
  var deviceButtom = panel.find(".devices-wrapper button");

  panel.on("click", ".responsive-switchers .desktop", (event) => {
    panel.removeClass("preview-tablet");
    panel.removeClass("preview-mobile");
    panel.addClass("preview-desktop");

    deviceButtom.removeClass("active");
    panel.find(".devices-wrapper .preview-desktop").addClass("active");
  });

  panel.on("click", ".responsive-switchers .tablet", (event) => {
    panel.removeClass("preview-desktop");
    panel.removeClass("preview-mobile");
    panel.addClass("preview-tablet");

    deviceButtom.removeClass("active");
    panel.find(".devices-wrapper .preview-tablet").addClass("active");
  });

  panel.on("click", ".responsive-switchers .mobile", (event) => {
    panel.removeClass("preview-desktop");
    panel.removeClass("preview-tablet");
    panel.addClass("preview-mobile");

    deviceButtom.removeClass("active");
    panel.find(".devices-wrapper .preview-mobile").addClass("active");
  });
});