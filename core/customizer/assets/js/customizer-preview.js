/**
 * Assign all js function if selective resfresh load
 *
 * @version 3.0.0
 */
jQuery(document).ready(($) => {

  /** Reload header js */
  wp.customize.selectiveRefresh.bind("partial-content-rendered", () => {
    new menu();
    swiperjs();
  });

  // end document
});
