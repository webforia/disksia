<?php
/**
 * @author : Webforia Studio
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Post extends Customizer_Base
{
    public function __construct()
    {
        $this->set_section();

        // archive section
        $this->add_post_options();
        $this->add_post_layout();

        // single section
        $this->add_single_options();
        $this->add_single_related();
        $this->add_single_layout();

    }

    public function set_section()
    {
        $this->add_panel('post_panel', [
            'title' => __('Post', 'retheme-admin'),
            'priority' => 80,
        ]);

        $this->add_section('post_panel', [
            'post_archive' => [__('Archive Post', 'retheme-admin')],
            'post_single' => [__('Single Post', 'retheme-admin')],
        ]);
    }

    public function add_post_options()
    {
        $section = 'post_archive_section';

        $this->add_header([
            'label' => __('Options', 'retheme-admin'),
            'section' => $section,
        ]);

        if (rt_is_premium()) {
            $style = [
                'grid' => get_template_directory_uri() . '/core/customizer/assets/img/post-grid.png',
                'card' => get_template_directory_uri() . '/core/customizer/assets/img/post-card.png',
                'overlay' => get_template_directory_uri() . '/core/customizer/assets/img/post-overlay.png',
                'list' => get_template_directory_uri() . '/core/customizer/assets/img/post-list.png',
            ];
        } else {
            $style = [
                'grid' => get_template_directory_uri() . '/core/customizer/assets/img/post-grid.png',
            ];
        }

        $this->add_field([
            'type' => 'radio-image',
            'settings' => 'post_style',
            'label' => __('Style', 'retheme-admin'),
            'section' => $section,
            'default' => 'grid',
            'choices' => $style ,
        ]);

        $this->add_field_responsive([
            'type' => 'slider',
            'section' => $section,
            'settings' => 'post_column',
            'label' => __('Column', 'retheme-admin'),
            'description' => __('Number of Column Per Row', 'retheme-admin'),
            'default' => 2,
            'default_tablet' => 2,
            'default_mobile' => 1,
            'choices' => [
                'min' => 1,
                'max' => 6,
            ],
            'active_callback' => [
                [
                    'setting' => 'post_style',
                    'operator' => '!==',
                    'value' => 'list',
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'number',
            'section' => $section,
            'settings' => 'post_title_length',
            'label' => __('Title Length ', 'retheme-admin'),
            'description' => __('Trims title to a certain number of words.', 'retheme-admin'),
            'default' => 9,
            'choices' => [
                'min' => 0,
                'max' => 50,
            ],
        ]);

        $this->add_field([
            'type' => 'number',
            'section' => $section,
            'settings' => 'post_excerpt_length',
            'label' => __('Excerpt Length', 'retheme-admin'),
            'description' => __('Trims text to a certain number of words.', 'retheme-admin'),
            'default' => 15,
            'choices' => [
                'min' => 0,
                'max' => 50,
            ],
        ]);

        if (rt_is_premium()) {
            $this->add_field([
                'type' => 'select',
                'settings' => 'post_pagination',
                'label' => __('Pagination', 'retheme-admin'),
                'section' => $section,
                'default' => 'number',
                'multiple' => 1,
                'choices' => [
                    'none' => __('None', 'retheme-admin'),
                    'number' => __('Number', 'retheme-admin'),
                    'loadmore' => __('Load More', 'retheme-admin'),
                ],
            ]);

            $this->add_divinder([
                'section' => $section,
            ]);

            $this->add_field([
                'type' => 'multicheck',
                'settings' => 'post_element',
                'label' => __('Post Elements', 'retheme-admin'),
                'section' => $section,
                'default' => [
                    'category', 'thumbnail', 'content',
                ],
                'multiple' => 999,
                'choices' => [
                    'category' => __('Category', 'retheme-admin'),
                    'thumbnail' => __('Featured Image', 'retheme-admin'),
                    'content' => __('Content', 'retheme-admin'),
                ],
            ]);

            $this->add_divinder([
                'section' => $section,
            ]);

            $this->add_field([
                'type' => 'multicheck',
                'settings' => 'post_meta',
                'label' => __('Post Meta', 'retheme-admin'),
                'section' => $section,
                'default' => [
                    'meta-author', 'meta-date', 'meta-comment',
                ],
                'multiple' => 999,
                'choices' => [
                    'meta-author' => __('Author', 'retheme-admin'),
                    'meta-date' => __('Date', 'retheme-admin'),
                    'meta-view' => __('View', 'retheme-admin'),
                    'meta-comment' => __('Comment', 'retheme-admin'),
                ],
            ]);
        }
    }

    public function add_post_layout()
    {
        $section = 'post_archive_section';

        $this->add_header([
            'label' => __('Post Archive Layout', 'retheme-admin'),
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'radio-image',
            'settings' => 'post_layout',
            'label' => __('Layout', 'retheme-admin'),
            'description' => sprintf(__('Visit the <a href="%s">Widgets panel</a> and add widget to display a element in a sidebar.', 'retheme-admin'), "javascript:wp.customize.panel( 'widgets' ).focus();"),
            'section' => $section,
            'default' => 'sidebar-right',
            'choices' => [
                'normal' => get_template_directory_uri() . '/core/customizer/assets/img/template-normal.png',
                'compact' => get_template_directory_uri() . '/core/customizer/assets/img/template-compact.png',
                'sidebar-left' => get_template_directory_uri() . '/core/customizer/assets/img/template-sidebar-left.png',
                'sidebar-right' => get_template_directory_uri() . '/core/customizer/assets/img/template-sidebar-right.png',
            ],
        ]);
    }

    public function add_single_options()
    {
        $section = 'post_single_section';

        if (rt_is_premium()) {
            $this->add_header([
                'label' => __('Options', 'retheme-admin'),
                'section' => $section,
            ]);

            $this->add_field([
                'type' => 'multicheck',
                'settings' => 'single_element',
                'label' => __('Single Post Elements', 'retheme-admin'),
                'section' => $section,
                'default' => [
                    'thumbnail', 'category', 'sharebox', 'tag', 'authorbox', 'navigation', 'comment', 'related',
                ],
                'multiple' => 999,
                'choices' => [
                    'thumbnail' => __('Featured Image', 'retheme-admin'),
                    'category' => __('Category', 'retheme-admin'),
                    'sharebox' => __('Share', 'retheme-admin'),
                    'tag' => __('Tags', 'retheme-admin'),
                    'authorbox' => __('Author Box', 'retheme-admin'),
                    'navigation' => __('Navigation', 'retheme-admin'),
                    'comment' => __('Comment', 'retheme-admin'),
                    'related' => __('Related Posts', 'retheme-admin'),
                ],
            ]);

            $this->add_divinder([
                'section' => $section,
            ]);

            $this->add_field([
                'type' => 'multicheck',
                'settings' => 'single_meta',
                'label' => __('Single Post Meta', 'retheme-admin'),
                'section' => $section,
                'default' => [
                    'meta-author', 'meta-date', 'meta-comment',
                ],
                'multiple' => 999,
                'choices' => [
                    'meta-author' => __('Author', 'retheme-admin'),
                    'meta-date' => __('Date', 'retheme-admin'),
                    'meta-view' => __('View', 'retheme-admin'),
                    'meta-comment' => __('Comment', 'retheme-admin'),
                ],
            ]);
        }

    }

    public function add_single_related()
    {

        if (rt_is_premium()) {
            $section = 'post_single_section';

            $this->add_header([
                'label' => __('Related', 'retheme-admin'),
                'settings' => 'single_related',
                'section' => $section,
            ]);

            $this->add_field([
                'type' => 'number',
                'settings' => 'single_related_count',
                'label' => __('Posts Related Count', 'retheme-admin'),
                'section' => $section,
                'default' => 6,
            ]);

            $this->add_field_responsive([
                'type' => 'slider',
                'settings' => 'single_related_show',
                'label' => __('Slider Show', 'retheme-admin'),
                'section' => $section,
                'default' => 2,
                'default_tablet' => 2,
                'default_mobile' => 1,
                'choices' => [
                    'min' => 1,
                    'max' => 6,
                ],
            ]);
        }

    }

    public function add_single_layout()
    {
        $section = 'post_single_section';

        $this->add_header([
            'label' => __('Post Single Layout', 'retheme-admin'),
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'radio-image',
            'settings' => 'single_layout',
            'label' => __('Layout', 'retheme-admin'),
            'description' => sprintf(__('Visit the <a href="%s">Widgets panel</a> and add widget to display a element in a sidebar.', 'retheme-admin'), "javascript:wp.customize.panel( 'widgets' ).focus();"),
            'section' => $section,
            'default' => 'sidebar-right',
            'choices' => [
                'normal' => get_template_directory_uri() . '/core/customizer/assets/img/template-normal.png',
                'compact' => get_template_directory_uri() . '/core/customizer/assets/img/template-compact.png',
                'sidebar-left' => get_template_directory_uri() . '/core/customizer/assets/img/template-sidebar-left.png',
                'sidebar-right' => get_template_directory_uri() . '/core/customizer/assets/img/template-sidebar-right.png',
            ],
        ]);
    }

    // end class
}

new Post;
