<?php
namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Customizer extends Customizer_Base
{
    public function __construct()
    {
        \Kirki::add_config('retheme_customizer', [
            'capability' => 'edit_theme_options',
            'option_type' => 'theme_mod',
        ]);

        add_action('customize_controls_enqueue_scripts', [$this, 'retheme_customizer_control'], 999);
        add_action('customize_preview_init', [$this, 'register_customizer_preview']);
        add_action('init', [$this, 'include_part']);
        add_action( 'customize_register', [$this, 'remove_sections']);

    }

    function remove_sections( $wp_customize ) {
        $wp_customize->remove_section('static_front_page');
    }

    public function include_part()
    {
        /**
         * Filter customizer section
         */
        $customizers = array(
            'homepage',
            'general',
            'brand',
            'header',
            'footer',
            'post',
            'page',
            'connect',
            'banner',
            'tweak',
        );


        foreach ($customizers as $key => $customizer) {
            include dirname(__FILE__) . "/{$customizer}.php";
        }


    }

    public function retheme_customizer_control()
    {
        // Font Awesome
        wp_enqueue_style('retheme-customizer-panel', get_template_directory_uri() . '/core/customizer/assets/css/customizer-panel.css', false, '3.0.0');
        wp_enqueue_script('retheme-customizer-control', get_template_directory_uri() . '/core/customizer/assets/js/customizer-control.js', ['jquery'], '3.0.0', true);

    }

    public function register_customizer_preview()
    {
        wp_enqueue_style('retheme-customizer-preview', get_template_directory_uri() . '/core/customizer/assets/css/customizer-preview.css', false, '3.0.0');
        wp_enqueue_script('retheme-customizer-preview', get_template_directory_uri() . '/core/customizer/assets/js/customizer-preview.js', ['jquery'], '3.0.0', true);
    }

    // end class
}

new Customizer;