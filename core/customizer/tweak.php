<?php
/**
 * @author : Webforia Studio
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Tweak extends Customizer_Base
{

    public function __construct()
    {   
        $this->set_panel();
        $this->tweak();
    }

    public function set_panel()
    {
        $this->add_section('', [
            'tweak' => [__('Tweaks ', 'retheme-admin')],
        ]);
    }

    public function tweak()
    {
        $section = 'tweak_section';

        $this->add_field([
            'type' => 'toggle',
            'settings' => 'move_jquery_to_footer',
            'label' => __('Move jQuery to Footer', 'retheme-admin'),
            'section' => $section,
            'default' => true,
        ]);

        $this->add_field([
            'type' => 'toggle',
            'settings' => 'remove_wp_emoji',
            'label' => __('Remove Emoji', 'retheme-admin'),
            'section' => $section,
            'default' => true,
        ]);

        $this->add_field([
            'type' => 'toggle',
            'settings' => 'remove_block_style',
            'label' => __('Remove Block Style', 'retheme-admin'),
            'section' => $section,
            'default' => false,
        ]);

        $this->add_divinder([
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'toggle',
            'settings' => 'breadcrumbs',
            'label' => __('Breadcrumbs', 'retheme-admin'),
            'default' => true,
            'section' => $section,
            'default' => true,
        ]);

        $this->add_field([
            'type' => 'toggle',
            'settings' => 'gotop',
            'label' => __('Go Top Navigation', 'retheme-admin'),
            'default' => true,
            'section' => $section,
            'default' => true,
        ]);

        $this->add_field([
            'type' => 'toggle',
            'settings' => 'dark_mode',
            'label' => __('Dark Mode', 'retheme-admin'),
            'default' => true,
            'section' => $section,
            'default' => true,
        ]);
    }

}

new Tweak;
