<?php
/**
 * @author : Webforia Studio
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Banner extends Customizer_Base
{

    public function __construct()
    {
        $this->set_section();
        $this->add_banner_header();
    }

    public function set_section()
    {
        if (rt_is_premium()) {
            $this->add_section('', [
                'banner_ads' => [__('Banner', 'retheme-admin'), '', '', 110],
            ]);
        }
    }

    public function add_banner_header()
    {
        $section = 'banner_ads_section';

        $this->add_field([
            'type' => 'select',
            'label' => __('Ads Banner', 'retheme-admin'),
            'settings' => 'banner_header',
            'default' => 'disable',
            'section' => $section,
            'choices' => [
                'disable' => __('Disable', 'retheme-admin'),
                'image' => __('Image', 'retheme-admin'),
                'code' => __('Shortcode', 'retheme-admin'),
            ],
        ]);

        $this->add_field([
            'type' => 'image',
            'label' => __('Image Banner', 'retheme-admin'),
            'settings' => 'banner_header_image',
            'section' => $section,
            'choices' => [
                'save_as' => 'id',
            ],
            'active_callback' => [
                [
                    'setting' => 'banner_header',
                    'operator' => '==',
                    'value' => 'image',
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'url',
            'label' => __('Url Target Ads', 'retheme-admin'),
            'settings' => 'banner_header_url',
            'section' => $section,
            'default' => '#',
            'active_callback' => [
                [
                    'setting' => 'banner_header',
                    'operator' => '==',
                    'value' => 'image',
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'textarea',
            'label' => __('Ads Shortcode', 'retheme-admin'),
            'settings' => 'banner_header_code',
            'section' => $section,
            'active_callback' => [
                [
                    'setting' => 'banner_header',
                    'operator' => '==',
                    'value' => 'code',
                ],
            ],
        ]);
    }

// end class
}

new Banner;
