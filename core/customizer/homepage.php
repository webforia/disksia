<?php
/**
 * @author : Webforia Studio
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;
use Retheme\Helper;

class Homebuilder extends Customizer_Base
{

    public function __construct()
    {
        $this->set_section();

        $this->add_layout();
        $this->add_post_featured();
        $this->add_post_archive();
        $this->add_custom_post();
        $this->add_category();
        $this->add_text_content();
        $this->add_image();

    }

    public function set_section()
    {
        if (rt_is_premium()) {
            $element = [
                'homepage_layout' => [__('Layout', 'retheme-admin')],
                'homepage_post_featured' => [__('Featured Posts', 'retheme-admin')],
                'homepage_post_1' => [__('Custom Posts 1', 'retheme-admin')],
                'homepage_post_2' => [__('Custom Posts 2', 'retheme-admin')],
                'homepage_post_3' => [__('Custom Posts 3', 'retheme-admin')],
                'homepage_post_archive' => [__('Lastest Posts', 'retheme-admin')],
                'homepage_category' => [__('Categories', 'retheme-admin')],
                'homepage_text_content' => [__('Text Content', 'retheme-admin')],
                'homepage_image' => [__('Image', 'retheme-admin')],
            ];
        } else {
            $element = [
                'homepage_layout' => [__('Layout', 'retheme-admin')],
                'homepage_post_1' => [__('Custom Posts 1', 'retheme-admin')],
                'homepage_post_archive' => [__('Lastest Posts', 'retheme-admin')],
                'homepage_image' => [__('Image', 'retheme-admin')],
            ];
        }

        $this->add_panel('homebuilder_panel', [
            'title' => __('Front Page Builder', 'retheme-admin'),
            'priority' => 0,
        ]);

        $this->add_section('homebuilder_panel', $element);

    }

    public function add_layout()
    {
        $section = 'homepage_layout_section';
        if (rt_is_premium()) {
            $element = [
                'post-featured' => __('Featured Posts', 'retheme-admin'),
                'post-1' => __('Custom Posts 1', 'retheme-admin'),
                'post-2' => __('Custom Posts 2', 'retheme-admin'),
                'post-3' => __('Custom Posts 3', 'retheme-admin'),
                'post-archive' => __('Lastest Posts', 'retheme-admin'),
                'category' => __('Categories', 'retheme-admin'),
                'text-content' => __('Text Content', 'retheme-admin'),
                'image' => __('Image', 'retheme-admin'),
            ];
        } else {
            $element = [
                'post-1' => __('Custom Posts 1', 'retheme-admin'),
                'post-archive' => __('Lastest Posts', 'retheme-admin'),
                'image' => __('Image', 'retheme-admin'),
            ];
        }
        $this->add_field([
            'type' => 'sortable',
            'settings' => 'homebuilder_layout',
            'label' => __('Sections', 'retheme-admin'),
            'section' => $section,
            'description' => __('Select (and re-order) front page sections that you want to display', 'retheme-admin'),
            'default' => [
                'post-archive',
            ],
            'choices' => $element,
        ]);

    }

    public function add_post_featured()
    {
        $section = 'homepage_post_featured_section';

        $this->add_field(
            [
                'settings' => 'homepage_post_featured_style',
                'type' => 'radio-image',
                'label' => __('Style', 'retheme-admin'),
                'description' => __('Show post by tag "Featured"', 'retheme-admin'),
                'tooltip' => 'Opsi ini akan menampilkan post yang diberi label featured',
                'section' => $section,
                'default' => 'grid-1',
                'choices' => [
                    'grid-1' => get_template_directory_uri() . '/core/customizer/assets/img/post-featured-1.png',
                    'slider-1' => get_template_directory_uri() . '/core/customizer/assets/img/post-slider-1.png',
                ],
            ]
        );
    }

    public function add_custom_post()
    {

        // before post loop
        $total = (rt_is_premium()) ? 3 : 1;

        for ($index = 1; $index <= $total; $index++) {

            $section = "homepage_post_{$index}_section";
            $setting = "homebuilder_post_{$index}";

            $this->add_header([
                'label' => __('Options', 'retheme-admin'),
                'settings' => "homebuilder_post_{$index}",
                'section' => $section,
            ]);

            if (rt_is_premium()) {
                $style = [
                    'grid' => get_template_directory_uri() . '/core/customizer/assets/img/post-grid.png',
                    'card' => get_template_directory_uri() . '/core/customizer/assets/img/post-card.png',
                    'overlay' => get_template_directory_uri() . '/core/customizer/assets/img/post-overlay.png',
                    'list' => get_template_directory_uri() . '/core/customizer/assets/img/post-list.png',
                ];
            } else {
                $style = [
                    'grid' => get_template_directory_uri() . '/core/customizer/assets/img/post-grid.png',
                ];
            }

            $this->add_field([
                'type' => 'radio-image',
                'settings' => "homebuilder_post_{$index}_style",
                'label' => __('Style', 'retheme-admin'),
                'section' => $section,
                'default' => 'grid',
                'choices' => $style,
            ]);

            $this->add_field([
                'type' => 'toggle',
                'section' => $section,
                'settings' => "homebuilder_post_{$index}_carousel",
                'label' => __('Carousel', 'retheme-admin'),
                'active_callback' => [
                    [
                        'setting' => "homebuilder_post_{$index}_style",
                        'operator' => '!==',
                        'value' => 'list',
                    ],
                ],
            ]);

            $this->add_field_responsive([
                'type' => 'slider',
                'section' => $section,
                'settings' => "homebuilder_post_{$index}_column",
                'label' => __('Column', 'retheme-admin'),
                'description' => __('Number of Column Per Row', 'retheme-admin'),
                'default' => 3,
                'default_tablet' => 2,
                'default_mobile' => 1,
                'choices' => [
                    'min' => 1,
                    'max' => 6,
                ],
                'active_callback' => [
                    [
                        'setting' => "homebuilder_post_{$index}_carousel",
                        'operator' => '!==',
                        'value' => true,
                    ],
                    [
                        'setting' => "homebuilder_post_{$index}_style",
                        'operator' => '!==',
                        'value' => 'list',
                    ],
                ],

            ]);

            if (rt_is_premium()) {
                $this->add_field([
                    'type' => 'select',
                    'section' => $section,
                    'settings' => "homebuilder_post_{$index}_pagination",
                    'label' => __('Pagination Style', 'retheme-admin'),
                    'default' => 'none',
                    'multiple' => 1,
                    'choices' => [
                        'none' => __('None', 'retheme-admin'),
                        'loadmore' => __('Load More', 'retheme-admin'),
                    ],
                    'active_callback' => [
                        [
                            'setting' => "homebuilder_post_{$index}_carousel",
                            'operator' => '!=',
                            'value' => true,
                        ],
                    ],

                ]);
            }

            $this->add_field_responsive([
                'type' => 'slider',
                'settings' => "homebuilder_post_{$index}_slider_show",
                'section' => $section,
                'label' => __('Slides to Show', 'retheme-admin'),
                'default' => 3,
                'default_tablet' => 2,
                'default_mobile' => 2,
                'choices' => [
                    'min' => 1,
                    'max' => 6,
                ],
                'active_callback' => [
                    [
                        'setting' => "homebuilder_post_{$index}_carousel",
                        'operator' => '==',
                        'value' => true,
                    ],
                ],

            ]);

            $this->add_header([
                'label' => __('Query', 'retheme-admin'),
                'settings' => "homebuilder_post_{$index}_query",
                'section' => $section,
            ]);

            $this->add_field([
                'type' => 'select',
                'section' => $section,
                'settings' => "homebuilder_post_{$index}_query_by",
                'label' => __('Query Post by', 'retheme-admin'),
                'default' => 'lastest',
                'multiple' => 1,
                'choices' => [
                    'lastest' => __('Lastest Posts', 'retheme-admin'),
                    'category' => __('Categories', 'retheme-admin'),
                    'manually' => __('Hand Picks', 'retheme-admin'),
                ],
            ]);

            $this->add_field([
                'type' => 'select',
                'section' => $section,
                'settings' => "homebuilder_post_{$index}_category",
                'label' => __('Category', 'retheme-admin'),
                'multiple' => 99,
                'choices' => Helper::get_terms('category'),
                'active_callback' => [
                    [
                        'setting' => "homebuilder_post_{$index}_query_by",
                        'operator' => '==',
                        'value' => 'category',
                    ],
                ],
            ]);

            $this->add_field([
                'type' => 'select',
                'section' => $section,
                'settings' => "homebuilder_post_{$index}_id",
                'label' => __('Select Post', 'retheme-admin'),
                'multiple' => 99,
                'choices' => Helper::get_posts('post'),
                'active_callback' => [
                    [
                        'setting' => "homebuilder_post_{$index}_query_by",
                        'operator' => '==',
                        'value' => 'manually',
                    ],
                ],
            ]);

            $this->add_field([
                'type' => 'select',
                'section' => $section,
                'settings' => "homebuilder_post_{$index}_orderby",
                'label' => __('Order By', 'retheme-admin'),
                'default' => 'date',
                'multiple' => 1,
                'choices' => [
                    'ID' => 'Post Id',
                    'author' => 'Post Author',
                    'title' => 'Title',
                    'date' => 'Date',
                    'modified' => 'Last Modified Date',
                    'wp_post_views_count' => __('Most Viewer'),
                    'comment_count' => __('Most Review'),
                ],

            ]);

            $this->add_field([
                'type' => 'number',
                'section' => $section,
                'settings' => "homebuilder_post_{$index}_posts_per_page",
                'label' => __('Posts Per Page', 'retheme-admin'),
                'default' => 6,
            ]);

            // section
            $this->add_header([
                'label' => 'Section',
                'settings' => "homebuilder_post_{$index}_section",
                'section' => $section,
            ]);

            $this->add_field([
                'type' => 'text',
                'settings' => "homebuilder_post_{$index}_title",
                'label' => __('Title', 'retheme-admin'),
                'default' => __('Post Title', 'retheme-admin'),
                'section' => $section,
            ]);

            $this->add_field([
                'type' => 'textarea',
                'settings' => "homebuilder_post_{$index}_desc",
                'label' => __('Description', 'retheme-admin'),
                'default' => __('Post Description', 'retheme-admin'),
                'section' => $section,
            ]);

        }

    }

    public function add_post_archive()
    {
        $section = 'homepage_post_archive_section';

        $this->add_field([
            'type' => 'text',
            'settings' => "homepage_post_archive_title",
            'label' => __('Title', 'retheme-admin'),
            'default' => __('Latest Posts', 'retheme-admin'),
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'textarea',
            'settings' => "homepage_post_archive_desc",
            'label' => __('Description', 'retheme-admin'),
            'section' => $section,
        ]);
    }

    public function add_category()
    {
        $section = 'homepage_category_section';

        $this->add_field([
            'type' => 'text',
            'settings' => "homebuilder_category_title",
            'label' => __('Title', 'retheme-admin'),
            'default' => __('Category Title', 'retheme-admin'),
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'textarea',
            'settings' => "homebuilder_category_desc",
            'label' => __('Description', 'retheme-admin'),
            'default' => __('Category Description', 'retheme-admin'),
            'section' => $section,
        ]);
    }

    public function add_text_content()
    {
        $section = 'homepage_text_content_section';

        $this->add_field([
            'type' => 'editor',
            'settings' => "homebuilder_text_content_text",
            'label' => __('Content', 'retheme-admin'),
            'tooltip' => __('Use the rich text editor to add in text into the section', 'retheme-admin'),
            'section' => $section,
        ]);

    }

    public function add_image()
    {
        $section = 'homepage_image_section';

        $this->add_field([
            'type' => 'image',
            'settings' => 'homebuilder_image',
            'label' => __('Image', 'retheme-admin'),
            'description' => __('Upload an image to be used for the section. For best results, the image should be at least 1200px wide and it looks best if the image is taller than the overall text you will be adding.', 'retheme-admin'),
            'section' => $section,
            'default' => '',
            'choices' => [
                'save_as' => 'id',
            ],
        ]);

        $this->add_field([
            'type' => 'link',
            'settings' => 'homebuilder_image_link_url',
            'label' => __('Link Url', 'retheme-admin'),
            'default' => '#',
            'section' => $section,
        ]);

    }

// end class
}

new Homebuilder;
