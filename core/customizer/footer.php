<?php
/**
 * @author : Webforia Studio
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Footer extends Customizer_Base
{

    public function __construct()
    {
        $this->set_section();
        $this->add_footer_widget();
    }

    public function set_section()
    {
        $this->add_section('', [
            'footer_widget' => [__('Footer', 'retheme-admin'), '', '', 70],
        ]);
    }

    public function add_footer_widget()
    {
        $section = 'footer_widget_section';

        if (rt_is_premium()) {
            $this->add_field([
                'type' => 'radio-image',
                'section' => $section,
                'settings' => 'footer_widget_layout',
                'label' => __('Layout', 'retheme-admin'),
                'description' => sprintf(__('Visit the <a href="%s">Widgets panel</a> and add widget to display a element in a footer.', 'retheme-admin'), "javascript:wp.customize.panel( 'widgets' ).focus();"),
                'default' => 'footer-widget-1',
                'choices' => [
                    'footer-widget-1' => get_template_directory_uri() . '/core/customizer/assets/img/footer-1.png',
                    'footer-widget-2' => get_template_directory_uri() . '/core/customizer/assets/img/footer-2.png',
                    'footer-widget-3' => get_template_directory_uri() . '/core/customizer/assets/img/footer-3.png',
                    'footer-widget-4' => get_template_directory_uri() . '/core/customizer/assets/img/footer-4.png',
                    'footer-widget-5' => get_template_directory_uri() . '/core/customizer/assets/img/footer-5.png',
                    'footer-widget-6' => get_template_directory_uri() . '/core/customizer/assets/img/footer-6.png',
                    'footer-widget-7' => get_template_directory_uri() . '/core/customizer/assets/img/footer-7.png',
                    'footer-custom-1' => get_template_directory_uri() . '/core/customizer/assets/img/footer-8.png',
                    'footer-custom-2' => get_template_directory_uri() . '/core/customizer/assets/img/footer-9.png',
                ],
            ]);
        }

        $this->add_field([
            'label' => __('Copyright (Support HTML tag)', 'retheme-admin'),
            'settings' => "footer_bottom_text",
            'description' => __('You can use text map {{site_name}} {{year}}', 'retheme-admin'),
            'section' => $section,
            "default" => "@Copyright {{site_name}}. All Rights Reserved",
            'type' => 'textarea',
        ]);

        $this->add_field([
            'type' => 'color',
            'label' => __('Heading Color', 'retheme-admin'),
            'choices' => ['alpha' => true],
            'settings' => 'footer_widget_heading_color',
            'default' => rt_get_theme('global_font_color')['primary'],
            'section' => $section,
            'transport' => 'auto',
            'output' => [
                [
                    'element' => '.retheme-default-scheme .page-footer',
                    'property' => '--theme-font-color-primary',
                ],
            ],

        ]);

        $this->add_field([
            'type' => 'color',
            'label' => __('Text Color', 'retheme-admin'),
            'choices' => ['alpha' => true],
            'settings' => 'footer_widget_color',
            'default' => rt_get_theme('global_font_color')['secondary'],
            'section' => $section,
            'transport' => 'auto',
            'output' => [
                [
                    'element' => '.retheme-default-scheme .page-footer',
                    'property' => '--theme-font-color-secondary',
                ],
            ],

        ]);

        $this->add_field([
            'type' => 'color',
            'choices' => ['alpha' => true],
            'settings' => 'footer_widget_color_active',
            'label' => __('Accent Color', 'retheme-admin'),
            'section' => $section,
            'default' => rt_get_theme('global_color_link')['hover'],
            'transport' => 'auto',
            'output' => [
                [
                    'element' => '.retheme-default-scheme .page-footer',
                    'property' => '--theme-link-active',
                ],
            ],

        ]);

        $this->add_field([
            'type' => 'color',
            'choices' => ['alpha' => true],
            'settings' => 'footer_widget_background',
            'label' => __('Background Color', 'retheme-admin'),
            'section' => $section,
            'default' => rt_get_theme('global_background_scheme')['secondary'],
            'transport' => 'auto',
            'output' => [
                [
                    'element' => '.retheme-default-scheme .page-footer',
                    'property' => '--theme-background-primary',
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'color',
            'choices' => ['alpha' => true],
            'settings' => 'footer_widget_border_color',
            'label' => __('Border Color', 'retheme-admin'),
            'section' => $section,
            'default' => rt_get_theme('global_border_color'),
            'transport' => 'auto',
            'output' => [
                [
                    'element' => '.retheme-default-scheme .page-footer',
                    'property' => '--theme-border-color',
                ],
            ],
        ]);

    }

// end class
}

new Footer;
