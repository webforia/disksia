<?php
/**
 * @author : Webforia Studio
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class General extends Customizer_Base
{

    public function __construct()
    {
        $this->set_section();
        $this->add_typography();
        $this->add_typography_heading();
        $this->add_color();
        $this->add_button();
    }

    public function set_section()
    {
        $this->add_panel('general_panel', [
            'title' => __('Global', 'retheme-admin'),
            'priority' => 50,
        ]);

        $this->add_section('general_panel', [
            'general_typography' => [__('Typography', 'retheme-admin')],
            'general_color' => [__('Colors & Styles', 'retheme-admin')],
            'general_button' => [__('Button', 'retheme-admin')],
        ]);
    }

    public function add_typography()
    {
        $section = 'general_typography_section';

        $this->add_field([
            'type' => 'typography',
            'settings' => 'typography_heading',
            'label' => __('Headings', 'retheme-admin'),
            'section' => $section,
            'default' => rt_get_theme('typography_heading'),
            'output' => [
                [
                    'choice' => 'font-family',
                    'element' => '.retheme-root',
                    'property' => '--theme-font-family-primary',
                ],
                [
                    'choice' => 'line-height',
                    'element' => 'h1, h2, h3, h4, h5, h6',
                    'property' => 'line-height',
                ],
                [
                    'choice' => 'font-weight',
                    'element' => '.retheme-root',
                    'property' => '--theme-font-weight',
                ],
                [
                    'choice' => 'text-transform',
                    'element' => 'h1, h2, h3, h4, h5, h6',
                    'property' => 'text-transform',
                ],
                [
                    'choice' => 'font-style',
                    'element' => 'h1, h2, h3, h4, h5, h6',
                    'property' => 'font-style',
                ],

            ],
        ]);

        $this->add_divinder([
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'typography',
            'settings' => 'typography_regular',
            'label' => __('Body', 'retheme-admin'),
            'section' => $section,
            'default' => rt_get_theme('typography_regular'),
            'device' => 'desktop',
            'output' => [
                [
                    'choice' => 'font-family',
                    'element' => '.retheme-root',
                    'property' => '--theme-font-family-secondary',
                ],
                [
                    'choice' => 'font-size',
                    'element' => '.rt-entry-content',
                    'property' => 'font-size',
                ],
                [
                    'choice' => 'line-height',
                    'element' => '.rt-entry-content',
                    'property' => 'line-height',
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'typography',
            'settings' => 'typography_regular_tablet',
            'label' => __('Body', 'retheme-admin'),
            'section' => $section,
            'default' => rt_get_theme('typography_regular'),
            'device' => 'tablet',
            'output' => [
                [
                    'choice' => 'font-family',
                    'element' => '.retheme-root',
                    'property' => '--theme-font-family-secondary',
                    'media_query' => $this->breakpoint_medium,
                ],
                [
                    'choice' => 'font-size',
                    'element' => '.rt-entry-content',
                    'property' => 'font-size',
                    'media_query' => $this->breakpoint_medium,
                ],
                [
                    'choice' => 'line-height',
                    'element' => '.rt-entry-content',
                    'property' => 'line-height',
                    'media_query' => $this->breakpoint_medium,
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'typography',
            'settings' => 'typography_regular_mobile',
            'label' => __('Body', 'retheme-admin'),
            'section' => $section,
            'default' => rt_get_theme('typography_regular'),
            'device' => 'mobile',
            'output' => [
                [
                    'choice' => 'font-family',
                    'element' => '.retheme-root',
                    'property' => '--theme-font-family-secondary',
                    'media_query' => $this->breakpoint_medium,
                ],
                [
                    'choice' => 'font-size',
                    'element' => '.rt-entry-content',
                    'property' => 'font-size',
                    'media_query' => $this->breakpoint_medium,
                ],
                [
                    'choice' => 'line-height',
                    'element' => '.rt-entry-content',
                    'property' => 'line-height',
                    'media_query' => $this->breakpoint_medium,
                ],
            ],
        ]);

    }

    public function add_typography_heading()
    {
        $section = 'general_typography_section';

        if (rt_get_option('header_typo', false) || rt_is_dev()) {

            $this->add_divinder([
                'section' => $section,
            ]);

            $this->add_field_responsive(
                [
                    'type' => 'dimension',
                    'settings' => 'typography_heading_1',
                    'label' => __('Heading 1', 'retheme-admin'),
                    'description' => __('Use CSS Unit (px, em, rem, %)', 'retheme-admin'),
                    'section' => $section,
                    'output' => [
                        [
                            'element' => 'h1',
                            'property' => 'font-size',
                        ],
                    ],
                ]
            );

            $this->add_field_responsive(
                [
                    'type' => 'dimension',
                    'settings' => 'typography_heading_2',
                    'label' => __('Heading 2', 'retheme-admin'),
                    'description' => __('Use CSS Unit (px, em, rem, %)', 'retheme-admin'),
                    'section' => $section,
                    'output' => [
                        [
                            'element' => 'h2',
                            'property' => 'font-size',
                        ],
                    ],
                ]
            );

            $this->add_field_responsive(
                [
                    'type' => 'dimension',
                    'settings' => 'typography_heading_3',
                    'label' => __('Heading 3', 'retheme-admin'),
                    'description' => __('Use CSS Unit (px, em, rem, %)', 'retheme-admin'),
                    'section' => $section,
                    'output' => [
                        [
                            'element' => 'h3',
                            'property' => 'font-size',
                        ],
                    ],
                ]
            );

            $this->add_field_responsive(
                [
                    'type' => 'dimension',
                    'settings' => 'typography_heading_4',
                    'label' => __('Heading 4', 'retheme-admin'),
                    'description' => __('Use CSS Unit (px, em, rem, %)', 'retheme-admin'),
                    'section' => $section,
                    'output' => [
                        [
                            'element' => 'h4',
                            'property' => 'font-size',
                        ],
                    ],
                ]
            );

        }
    }

    public function add_color()
    {
        $section = 'general_color_section';

        // Brand
        $this->add_field([
            'type' => 'color',
            'choices' => ['alpha' => true],
            'label' => __('Brand Color Primary', 'retheme-admin'),
            'settings' => 'global_brand_color',
            'section' => $section,
            'default' => rt_get_theme('global_brand_color')['primary'],
            'output' => [
                [
                    'element' => '.retheme-root',
                    'property' => '--theme-color-brand',
                ],
                [
                    'element' => '.retheme-root',
                    'property' => '--theme-btn-primary-background',
                ],
            ],
            'transport' => 'auto',
        ]);

        $this->add_field([
            'type' => 'color',
            'choices' => ['alpha' => true],
            'label' => __('Brand Color Secondary', 'retheme-admin'),
            'settings' => 'global_brand_color_active',
            'section' => $section,
            'default' => rt_get_theme('global_brand_color')['secondary'],
            'output' => [
                [
                    'element' => '.retheme-root',
                    'property' => '--theme-color-brand-active',
                ],
                [
                    'element' => '.retheme-root',
                    'property' => '--theme-btn-primary-background-active',
                ],
            ],
            'transport' => 'auto',
        ]);

        $this->add_divinder([
            'section' => $section,
        ]);

        // Font Color
        $this->add_field([
            'type' => 'color',
            'choices' => ['alpha' => true],
            'label' => __('Heading Color', 'retheme-admin'),
            'settings' => 'global_font_color_primary',
            'section' => $section,
            'default' => rt_get_theme('global_font_color')['primary'],
            'output' => [
                [
                    'element' => '.retheme-default-scheme',
                    'property' => '--theme-font-color-primary',
                ],
            ],
            'transport' => 'auto',
        ]);

        $this->add_field([
            'type' => 'color',
            'choices' => ['alpha' => true],
            'label' => __('Text Color', 'retheme-admin'),
            'settings' => 'global_font_color_secondary',
            'section' => $section,
            'default' => rt_get_theme('global_font_color')['secondary'],
            'output' => [
                [
                    'element' => '.retheme-default-scheme, .retheme-default-scheme .page-header .rt-menu__submenu',
                    'property' => '--theme-font-color-secondary',
                ],
            ],
            'transport' => 'auto',
        ]);

        $this->add_field([
            'type' => 'color',
            'choices' => ['alpha' => true],
            'label' => __('Meta Color', 'retheme-admin'),
            'settings' => 'global_font_color_tertiary',
            'section' => $section,
            'default' => rt_get_theme('global_font_color')['tertiary'],
            'output' => [
                [
                    'element' => '.retheme-default-scheme',
                    'property' => '--theme-font-color-tertiary',
                ],
            ],
            'transport' => 'auto',
        ]);

        $this->add_divinder([
            'section' => $section,
        ]);

        // Link
        $this->add_field([
            'type' => 'color',
            'choices' => ['alpha' => true],
            'label' => __('Link Color', 'retheme-admin'),
            'settings' => 'global_color_link',
            'section' => $section,
            'default' => rt_get_theme('global_color_link')['normal'],
            'output' => [
                [
                    'element' => '.retheme-root',
                    'property' => '--theme-link',
                ],
            ],
            'transport' => 'auto',
        ]);

        $this->add_field([
            'type' => 'color',
            'choices' => ['alpha' => true],
            'label' => __('Link Accent Color', 'retheme-admin'),
            'settings' => 'global_color_link_active',
            'section' => $section,
            'default' => rt_get_theme('global_color_link')['normal'],
            'output' => [
                [
                    'element' => '.retheme-root, .page-header .rt-menu__submenu',
                    'property' => '--theme-link-active',
                ],
            ],
            'transport' => 'auto',
        ]);

        $this->add_divinder([
            'section' => $section,
        ]);

        // Background
        $this->add_field([
            'type' => 'color',
            'choices' => ['alpha' => true],
            'label' => __('Background Primary', 'retheme-admin'),
            'settings' => 'global_background_scheme_primary',
            'section' => $section,
            'default' => rt_get_theme('global_background_scheme')['primary'],
            'output' => [
                [
                    'element' => '.retheme-default-scheme, .retheme-default-scheme .page-header .rt-menu__submenu',
                    'property' => '--theme-background-primary',
                ],
            ],
            'transport' => 'auto',
        ]);

        $this->add_field([
            'type' => 'color',
            'choices' => ['alpha' => true],
            'label' => __('Background Secondary', 'retheme-admin'),
            'settings' => 'global_background_scheme_secondary',
            'section' => $section,
            'default' => rt_get_theme('global_background_scheme')['secondary'],
            'output' => [
                [
                    'element' => '.retheme-default-scheme',
                    'property' => '--theme-background-secondary',
                ],
            ],
            'transport' => 'auto',
        ]);

        $this->add_divinder([
            'section' => $section,
        ]);

        // Border Color
        $this->add_field([
            'type' => 'color',
            'choices' => ['alpha' => true],
            'label' => __('Border Color', 'retheme-admin'),
            'settings' => 'global_border_color',
            'section' => $section,
            'default' => rt_get_theme('global_border_color'),
            'output' => [
                [
                    'element' => '.retheme-default-scheme',
                    'property' => '--theme-border-color',
                ],
            ],
            'transport' => 'auto',
        ]);

        // Highlight
        $this->add_field([
            'type' => 'color',
            'choices' => ['alpha' => true],
            'label' => __('Highlight Color', 'retheme-admin'),
            'settings' => 'global_highlight_color',
            'section' => $section,
            'default' => rt_get_theme('global_highlight_color'),
            'output' => [
                [
                    'element' => '.retheme-root',
                    'property' => '--theme-color-highlight',
                ],
            ],
            'transport' => 'auto',
        ]);

    }

    public function add_button()
    {
        $section = 'general_button_section';

        $this->add_field([
            'type' => 'color',
            'choices' => ['alpha' => true],
            'label' => __('Text Color', 'retheme-admin'),
            'settings' => 'global_button_color',
            'section' => $section,
            'default' => '#ffffff',
            'output' => [
                [
                    'element' => '.retheme-root',
                    'property' => '--theme-btn-primary-color',
                ],
            ],
            'transport' => 'auto',
        ]);

        $this->add_field([
            'type' => 'color',
            'choices' => ['alpha' => true],
            'label' => __('Text Accent Color', 'retheme-admin'),
            'settings' => 'global_button_color_active',
            'section' => $section,
            'default' => '#ffffff',
            'output' => [
                [
                    'element' => '.retheme-root',
                    'property' => '--theme-btn-primary-color-active',
                ],
            ],
            'transport' => 'auto',
        ]);
        $this->add_field([
            'type' => 'color',
            'choices' => ['alpha' => true],
            'label' => __('Background Color', 'retheme-admin'),
            'settings' => 'global_button_background',
            'section' => $section,
            'default' => rt_get_theme('global_brand_color')['primary'],
            'output' => [
                [
                    'element' => '.retheme-root',
                    'property' => '--theme-btn-primary-background',
                ],
            ],
            'transport' => 'auto',
        ]);

        $this->add_field([
            'type' => 'color',
            'choices' => ['alpha' => true],
            'label' => __('Background Accent Color', 'retheme-admin'),
            'settings' => 'global_button_background_active',
            'section' => $section,
            'default' => rt_get_theme('global_brand_color')['secondary'],
            'output' => [
                [
                    'element' => '.retheme-root',
                    'property' => '--theme-btn-primary-background-active',
                ],
            ],
            'transport' => 'auto',
        ]);
    }
// end class
}

new General;
