<?php
/**
 * @author : Webforia Studio
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Page extends Customizer_Base
{

    public function __construct()
    {
        $this->set_section();
        $this->add_page_layout();
    }

    public function set_section()
    {
        $this->add_section('', [
            'page' => [__('Page', 'retheme-admin'), '', '', 90],
        ]);
    }

    public function add_page_layout()
    {
        $section = 'page_section';

        $this->add_field([
            'type' => 'radio-image',
            'settings' => 'page_layout',
            'label' => __('Layout', 'retheme-admin'),
            'description' => sprintf(__('Visit the <a href="%s">Widgets panel</a> and add widget to display a element in a sidebar.', 'retheme-admin'), "javascript:wp.customize.panel( 'widgets' ).focus();"),
            'section' => $section,
            'default' => 'normal',
            'choices' => [
                'normal' => get_template_directory_uri() . '/core/customizer/assets/img/template-normal.png',
                'compact' => get_template_directory_uri() . '/core/customizer/assets/img/template-compact.png',
                'sidebar-left' => get_template_directory_uri() . '/core/customizer/assets/img/template-sidebar-left.png',
                'sidebar-right' => get_template_directory_uri() . '/core/customizer/assets/img/template-sidebar-right.png',
            ],
        ]);
    }

// end class
}

new Page;
