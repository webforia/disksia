<?php
/**
 * @author : Webforia Studio
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Brand extends Customizer_Base
{

    public function __construct()
    {
        $this->add_brand_logo();
        $this->mobile_color_browser();
    }

    public function mobile_color_browser()
    {
        $section = 'title_tagline';

        $this->add_header([
            'label' => __('Browser Brand', 'retheme-admin'),
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'color',
            'choices' => ['alpha' => true],
            'label' => __('Color Browser', 'retheme-admin'),
            'settings' => 'brand_color_browser',
            'description' => __('Change the Color of Address Bar in Mobile Browser', 'retheme-admin'),
            'section' => $section,
            'default' => $this->get_default_mod('global_brand_color', 'primary'),
        ]);

    }

    public function add_brand_logo()
    {

        $section = 'title_tagline';

        // Logo Desktop
        $this->add_header([
            'label' => __('Desktop Logo', 'retheme-admin'),
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'image',
            'settings' => 'brand_logo_primary',
            'label' => __('Default Logo', 'retheme-admin'),
            'description' => __('This logo will be used as your default logo on desktop device', 'retheme-admin'),
            'section' => $section,
            'choices' => [
                'save_as' => 'array',
            ],
        ]);

        $this->add_field([
            'type' => 'slider',
            'settings' => 'brand_logo_primary_size',
            'label' => __('Default Logo Width', 'retheme-admin'),
            'section' => $section,
            'choices' => [
                'min' => '10',
                'max' => '300',
                'step' => '1',
            ],
            'default' => '120',
            'output' => [
                [
                    'element' => '.page-header .rt-logo',
                    'property' => 'width',
                    'units' => 'px',
                ],

            ],
            'transport' => 'auto',
        ]);

        // Logo Sticky
        $this->add_divinder([
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'image',
            'settings' => 'brand_logo_sticky',
            'label' => __('Sticky Logo', 'retheme-admin'),
            'description' => __('This logo will be used when the header is in a sticky state.', 'retheme-admin'),
            'section' => $section,
            'choices' => [
                'save_as' => 'array',
            ],
        ]);

        $this->add_field([
            'type' => 'slider',
            'settings' => 'brand_logo_sticky_size',
            'label' => __('Sticky Logo Width', 'retheme-admin'),
            'section' => $section,
            'choices' => [
                'min' => '10',
                'max' => '300',
                'step' => '1',
            ],
            'default' => '120',
            'output' => [
                [
                    'element' => '.page-header__sticky .rt-logo',
                    'property' => 'width',
                    'units' => 'px',
                ],

            ],
            'transport' => 'auto',
        ]);

        // Logo Mobile
        $this->add_header([
            'label' => __('Mobile Logo', 'retheme-admin'),
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'image',
            'settings' => 'brand_logo_mobile',
            'label' => __('Mobile Logo', 'retheme-admin'),
            'description' => __('This logo will be used on mobile devices', 'retheme-admin'),
            'section' => $section,
            'choices' => [
                'save_as' => 'array',
            ],
        ]);

        $this->add_field([
            'type' => 'slider',
            'settings' => 'brand_logo_mobile_size',
            'label' => __('Mobile Logo Width', 'retheme-admin'),
            'section' => $section,
            'choices' => [
                'min' => '10',
                'max' => '200',
                'step' => '1',
            ],
            'default' => '120',
            'output' => [
                [
                    'element' => '.page-header-mobile .rt-logo-mobile',
                    'property' => 'width',
                    'units' => 'px',
                ],

            ],
            'transport' => 'auto',
        ]);

    }

// end class
}

new Brand;
