<?php
include_once dirname(__FILE__) . '/vendor/vendor.php';
include_once dirname(__FILE__) . '/classes/class.php';
include_once dirname(__FILE__) . '/includes/include.php';
include_once dirname(__FILE__) . '/admin/admin.php';
include_once dirname(__FILE__) . '/widget/widget.php';
// Skip on wc ajax request
if (empty($_GET['wc-ajax'])) {
    include_once dirname(__FILE__) . '/customizer/customizer.php';
}
if(class_exists('woocommerce')){
    require_once dirname(__FILE__) . '/woocommerce/woocommerce.php';
}