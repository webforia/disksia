<?php
if (!class_exists('Merlin')) {
    return;
}
/**
 * Set directory locations, text strings, and settings.
 */
class Retheme_Merlin extends Merlin
{
    /**
     * Activate the EDD license.
     *
     * This code was taken from the EDD licensing addon theme example code
     * (`activate_license` method of the `EDD_Theme_Updater_Admin` class).
     *
     * @param string $license The license key.
     *
     * @return array
     */
    protected function edd_activate_license($key)
    {
        $success = false;

        // Strings passed in from the config file.
        $strings = $this->strings;

        // Theme Name.
        $theme = ucfirst($this->theme);

        // Remove "Child" from the current theme name, if it's installed.
        $theme = str_replace(' Child', '', $theme);

        // Text strings.
        $success_message = $strings['license-json-success%s'];

        // API Manager
        $license = New \Retheme\Activation(array(
            'product_extends' => rt_get_theme('extends'),
            'product_key' => $key,
        ));

        $status = $license->activate();
        $message = sprintf(esc_html($status['message']), $theme);

        if ($status['result'] == 'success') {
            $success = true;
        } else {
            $success = false;
        }

        return compact('success', 'message');

    }

}

// set config theme wizard
new Retheme_Merlin(
    $config = array(
        'directory' => 'core/vendor/merlin', // Location / directory where Merlin WP is placed in your theme.
        'merlin_url' => 'merlin', // The wp-admin page slug where Merlin WP loads.
        'parent_slug' => 'themes.php', // The wp-admin parent page slug for the admin menu item.
        'capability' => 'manage_options', // The capability required for this menu to be displayed to the user.
        'child_action_btn_url' => 'https://codex.wordpress.org/child_themes', // URL for the 'child-action-link'.
        'dev_mode' => true, // Enable development mode for testing.
        'license_step' => true, // EDD license activation step.
        'license_required' => false, // Require the license activation step.
        'license_help_url' => '', // URL for the 'license-tooltip'.
        'edd_remote_api_url' => '', // EDD_Theme_Updater_Admin remote_api_url.
        'edd_item_name' => RT_THEME_NAME, // EDD_Theme_Updater_Admin item_name.
        'edd_theme_slug' => RT_THEME_SLUG, // EDD_Theme_Updater_Admin item_slug.
        'ready_big_button_url' => site_url(), // Link for the big button on the ready step.
    ),

    $strings = array(
        'admin-menu' => esc_html__('Theme Setup', 'retheme-admin'),
        /* translators: 1: Title Tag 2: Theme Name 3: Closing Title Tag */
        'title%s%s%s%s' => esc_html__('%1$s%2$s Themes &lsaquo; Theme Setup: %3$s%4$s', 'retheme-admin'),
        'return-to-dashboard' => esc_html__('Kembali kehalaman admin', 'retheme-admin'),
        'ignore' => esc_html__('Matikan fitur wizard', 'retheme-admin'),
        'btn-skip' => esc_html__('Lewati', 'retheme-admin'),
        'btn-next' => esc_html__('Selanjutnya', 'retheme-admin'),
        'btn-start' => esc_html__('Mulai', 'retheme-admin'),
        'btn-no' => esc_html__('Batal', 'retheme-admin'),
        'btn-plugins-install' => esc_html__('Instal', 'retheme-admin'),
        'btn-child-install' => esc_html__('Instal', 'retheme-admin'),
        'btn-content-install' => esc_html__('Instal', 'retheme-admin'),
        'btn-import' => esc_html__('Impor', 'retheme-admin'),
        'btn-license-activate' => esc_html__('Aktifkan', 'retheme-admin'),
        'btn-license-skip' => esc_html__('Nanti', 'retheme-admin'),
        /* translators: Theme Name */
        'license-header%s' => esc_html__('Aktifkan %s', 'retheme-admin'),
        /* translators: Theme Name */
        'license-header-success%s' => esc_html__('%s sudah diaktifkan', 'retheme-admin'),
        /* translators: Theme Name */
        'license%s' => esc_html__('Masukan kunci lisensi untuk mendapatkan semua fitur premium, update, dan dukungan dari Webforia Studio', 'retheme-admin'),
        'license-label' => esc_html__('Kunci lisensi', 'retheme-admin'),
        'license-success%s' => esc_html__('Tema ini sudah terdaftar, silakan lanjutkan ke langkah berikutnya', 'retheme-admin'),
        'license-json-success%s' => esc_html__('Tema Anda sudah di aktifkan! Fitur Premium, update, dan dukungan bisa digunakan', 'retheme-admin'),
        'license-tooltip' => esc_html__('Butuh bantuan?', 'retheme-admin'),
        /* translators: Theme Name */
        'welcome-header%s' => esc_html__('Selamat datang di %s', 'retheme-admin'),
        'welcome-header-success%s' => esc_html__('Hi. Selamat datang kembali', 'retheme-admin'),
        'welcome%s' => esc_html__('Fitur Wizard ini akan membantu Anda untuk melakukan pengaturan tema, memasang plugin, dan mengimpor konten. Langkah ini hanya opsional tapi sangat direkomendasikan bagi Anda yang baru di dunia WordPress.', 'retheme-admin'),
        'welcome-success%s' => esc_html__('Anda mungkin sudah menjalankan fitur Wizard ini sebelumnnya, Jika Anda ingin melanjutkannya silakan klik tombol "Mulai" di bawah ini', 'retheme-admin'),
        'child-header' => esc_html__('Install tema anak', 'retheme-admin'),
        'child-header-success' => esc_html__('You\'re good to go!', 'retheme-admin'),
        'child' => esc_html__('Aktifkan tema anak sehingga Anda dapat dengan mudah melakukan perubahan tanpa merusak tema utama.', 'retheme-admin'),
        'child-success%s' => esc_html__('Tema anak Anda berhasil di install dan diaktifkan', 'retheme-admin'),
        'child-action-link' => esc_html__('Pelajari lebih lanjut tentang tema anak', 'retheme-admin'),
        'child-json-success%s' => esc_html__('Tema anak Anda sudah diinstall dan diaktifkan', 'retheme-admin'),
        'child-json-already%s' => esc_html__('Tema anak Anda sudah berhasil dibuat dan sekarang sudah diaktifkan', 'retheme-admin'),
        'plugins-header' => esc_html__('Install Plugins', 'retheme-admin'),
        'plugins-header-success' => esc_html__('Mengagumkan', 'retheme-admin'),
        'plugins' => esc_html__('Mari kita instal beberapa plugin WordPress yang penting untuk membangun situs Anda.', 'retheme-admin'),
        'plugins-success%s' => esc_html__('Semua plugin WordPress yang diperlukan sudah terpasang dan diperbarui. Tekan "Selanjutnya" untuk melanjutkan kepengaturan.', 'retheme-admin'),
        'plugins-action-link' => esc_html__('Tingkat lanjut', 'retheme-admin'),
        'import-header' => esc_html__('Impor Konten', 'retheme-admin'),
        'import' => esc_html__('Pilih salah satu tampilan website sesuai dengan demo.', 'retheme-admin'),
        'import-action-link' => esc_html__('Tingkat lanjut', 'retheme-admin'),
        'ready-header' => esc_html__('Proses instalasi selesai. Selamat bersenang-senang!', 'retheme-admin'),
        /* translators: Theme Author */
        'ready%s' => esc_html__('Tema Anda telah diatur. Nikmati tema baru Anda yang dikembangkan oleh %s.', 'retheme-admin'),
        'ready-action-link' => esc_html__('Extra', 'retheme-admin'),
        'ready-big-button' => esc_html__('Kunjungi website', 'retheme-admin'),
        // 'ready-link-1' => sprintf('<a href="%1$s" target="_blank">%2$s</a>', admin_url( 'index.php?page=wc-setup'), esc_html__('Setup Toko', 'retheme-admin')),
        'ready-link-2' => sprintf('<a href="%1$s" target="_blank">%2$s</a>', admin_url('customize.php'), esc_html__('Mulai melakukan kostumisasi', 'retheme-admin')),
        'ready-link-3' => sprintf('<a href="%1$s" target="_blank">%2$s</a>', 'https://webforia.id/kontak/', esc_html__('Hubungi dukungan tema', 'retheme-admin')),

    )
);

// check license
function merlin_check_license()
{
    return (rt_is_premium())? true: false;
}
add_filter('merlin_is_theme_registered', 'merlin_check_license');
add_filter('wp_ajax_merlin_child_theme', '__return_false'); // disable generate child theme
