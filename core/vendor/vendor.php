<?php
require_once dirname(__FILE__) . '/breadcrumb-tail.php';
require_once dirname(__FILE__) . '/class-tgm-plugin-activation.php';
require_once dirname(__FILE__) . '/plugin-update-checker/plugin-update-checker.php';

if (!function_exists('carbon_fields_boot_plugin')) {
    require_once dirname(__FILE__) . '/carbon-fields/carbon-fields-plugin.php';
}

if (!class_exists('Mobile_Detect')) {
    require_once dirname(__FILE__) . '/Mobile_Detect.php';
}

require_once dirname(__FILE__) . '/kirki/kirki.php';

if (!class_exists('Merlin') && is_admin()) {
    require_once dirname(__FILE__) . '/merlin/vendor/autoload.php';
    require_once dirname(__FILE__) . '/merlin/class-merlin.php';
    require_once dirname(__FILE__) . '/merlin-config.php';
}
$theme_slug = RT_THEME_SLUG;

if (get_option("{$theme_slug}_product_update")) {
    $theme_update = Puc_v4_Factory::buildUpdateChecker("https://gitlab.com/webforia/{$theme_slug}/", get_template_directory(), $theme_slug);
    $theme_update->setBranch('stable_release');
}
