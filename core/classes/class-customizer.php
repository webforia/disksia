<?php
/**
 * Customizer WP methods
 *
 * @package     Retheme
 * @category    Core
 * @author      Webforia Studio
 * @version     3.2.0
 */
namespace Retheme;

use Retheme\Helper;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

// No need to proceed if this class already exists.
if (!class_exists('Retheme\Customizer_Base')) {
    class Customizer_Base
    {

        public $breakpoint_large = '@media (min-width: 992px)';
        public $breakpoint_medium = '@media (max-width: 768px)';
        public $breakpoint_small = '@media (max-width: 576px)';
        public $textdomain = 'retheme';

        /**
         * Add panel
         * required kirki
         */
        public function add_panel($name, $args)
        {
            new \Kirki\Panel($name, $args);
        }

        /**
         * Add section
         * required kirki
         */
        public function add_section($panel = '', $sections)
        {

            foreach ($sections as $section_id => $section) {
                $section_args = [
                    'title' => $section[0],
                    'description' => !empty($section[1]) ? $section[1] : '',
                    'panel' => $panel,
                ];
                if (isset($section[2])) {
                    $section_args['type'] = $section[2];
                }
                if (isset($section[3])) {
                    $section_args['priority'] = $section[3];
                }
                new \Kirki\Section(str_replace('-', '_', $section_id) . '_section', $section_args);
            }
        }

        /**
         * Add field control
         */
        public function add_field($args)
        {
            $type = $args['type'];

            switch ($type) {
                case 'background':
                    new \Kirki\Field\Background($args);
                    break;
                case 'color':
                    new \Kirki\Field\Color($args);
                    break;
                case 'color_palette':
                    new \Kirki\Field\Color_Palette($args);
                    break;
                case 'dimension':
                    new \Kirki\Field\Dimension($args);
                    break;
                case 'dimensions':
                    new \Kirki\Field\Dimensions($args);
                    break;
                case 'image':
                    new \Kirki\Field\Image($args);
                    break;
                case 'multicheck':
                    new \Kirki\Field\Multicheck($args);
                    break;
                case 'multicolor':
                    new \Kirki\Field\Multicolor($args);
                    break;
                case 'number':
                    new \Kirki\Field\number($args);
                    break;
                case 'radio-buttonset':
                    new \Kirki\Field\Radio_Buttonset($args);
                    break;
                case 'radio-image':
                    new \Kirki\Field\Radio_Image($args);
                    break;
                case 'repeater':
                    new \Kirki\Field\Repeater($args);
                    break;
                case 'select':
                    new \Kirki\Field\Select($args);
                    break;
                case 'slider':
                    new \Kirki\Field\Slider($args);
                    break;
                case 'sortable':
                    new \Kirki\Field\Sortable($args);
                    break;
                case 'toggle':
                    new \Kirki\Field\Toggle($args);
                    break;
                case 'typography':
                    new \Kirki\Field\Typography($args);
                    break;
                case 'upload':
                    new \Kirki\Field\Upload($args);
                    break;
                case 'link':
                    new \Kirki\Field\URL($args);
                    break;
                case 'url':
                    new \Kirki\Field\URL($args);
                    break;
                case 'text':
                    new \Kirki\Field\Text($args);
                    break;
                case 'textarea':
                    new \Kirki\Field\Textarea($args);
                    break;
                case 'generic':
                    new \Kirki\Field\Generic($args);
                    break;
                case 'editor':
                    new \Kirki\Field\Editor($args);
                    break;
                case 'code':
                    new \Kirki\Field\Code($args);
                    break;
                default:
                    \Kirki::add_field($args);
                    break;
            }
        }

        /**
         * Custom header control
         *
         * @param array $args
         * @return void
         */
        public function add_header($args = [])
        {
            $id = rand();

            $this->add_field(wp_parse_args([
                'type' => 'generic',
                'settings' => "control_header_{$args['section']}_{$id}",
                'class' => 'header',
                'choices' => [
                    'element' => 'span',
                ],
            ], $args));
        }

        /**
         * Costum divinder control
         *
         * @param array $args
         * @return void
         */
        public function add_divinder($args = [])
        {
            $id = rand();

            $this->add_field(wp_parse_args([
                'type' => 'generic',
                'settings' => "control_divinder_{$args['section']}_{$id}",
                'class' => 'divinder',
                'choices' => [
                    'element' => 'hr',
                ],
            ], $args));
        }

        /**
         * Responsive control with mobile choose
         * @desc - Add tablet and mobile control
         * @param $args - argument from control
         * @return HTML - control field
         */
        public function add_field_responsive($args = [])
        {
            $default = !empty($args['default']) ? $args['default'] : '';
            $default_tablet = !empty($args['default_tablet']) ? $args['default_tablet'] : '';
            $default_mobile = !empty($args['default_mobile']) ? $args['default_mobile'] : '';

            // output
            $output_tablet = [];
            $output_mobile = [];

            if (!empty($args['output'])) {

                $output_tablet = $args['output'];

                foreach ($output_tablet as $key => $output) {
                    $output_tablet[$key]['media_query'] = $this->breakpoint_medium;
                }

                $output_mobile = $args['output'];

                foreach ($output_mobile as $key => $output) {
                    $output_mobile[$key]['media_query'] = $this->breakpoint_small;
                }
            }

            /** Show responsive control */
            $this->add_field(wp_parse_args([
                'device' => 'desktop',
                'default' => $default,
            ], $args));

            $this->add_field(wp_parse_args([
                'settings' => "{$args['settings']}_tablet",
                'device' => 'tablet',
                'output' => $output_tablet,
                'default' => $default_tablet,
            ], $args));

            $this->add_field(wp_parse_args([
                'settings' => "{$args['settings']}_mobile",
                'device' => 'mobile',
                'output' => $output_mobile,
                'default' => $default_mobile,
            ], $args));

        }

        /**
         * Animation Control
         *
         * @param array $args
         * @return void
         */
        public function add_field_animation($args = [])
        {
            $element = !empty($args['element']) ? $args['element'] : '';
            /**
             * Merge default array with array from control
             * @param array $args, $default
             */
            $this->add_field(wp_parse_args($args, [
                'type' => 'select',
                'label' => __('Animation', 'admin_domain'),
                'settings' => $args['settings'],
                'default' => 'fadeIn',
                'choices' => Helper::get_animation_in(),
            ]));

            $this->add_field([
                'type' => 'number',
                'label' => __('Duration', RT_THEME_DOMAIN),
                'settings' => $args['settings'] . '_duration',
                'default' => 300,
                'choices' => [
                    'min' => 120,
                    'max' => 1000,
                ],
            ]);

        }

        /*
         * Merge selector for hover
         */
        public function selector($selector, $pseudo = '')
        {
            $data = explode(",", $selector);
            $element = array();

            foreach ($data as $key => $value) {

                $element[] = $value . ':hover';
                $element[] = $value . ':active';
                $element[] = $value . ':focus';

            }
            return implode(', ', $element);

        }

        /**
         * Get default form theme mods
         *
         * this function use for get value from global settings
         * @param [type] $name
         * @param string $choose
         * @return void
         */
        public function get_default_mod($name, $choose = '')
        {
            $theme_mod = '';

            if (!empty(get_theme_mod($name, rt_get_theme($name))[$choose])) {
                $theme_mod = get_theme_mod($name, rt_get_theme($name))[$choose];
            }
            if (empty($choose)) {
                $theme_mod = get_theme_mod($name, rt_get_theme($name));
            }

            return $theme_mod;
        }

        // end class
    }
}
