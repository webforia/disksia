<?php
/**
 * Helper function
 *
 * @package     Retheme
 * @category    Core
 * @author      Webforia Studio
 * @version     3.0.0
 */

namespace Retheme;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

// No need to proceed if this class already exists.
if (!class_exists('Retheme\Helper')) {
    class Helper
    {

        /**
         * Get animation list
         * @return [array]
         */
        public static function get_animation_in()
        {

            return array(
                'backInDown' => 'backInDown',
                'backInLeft' => 'backInLeft',
                'backInRight' => 'backInRight',
                'backInUp' => 'backInUp',
                'fadeIn' => 'fadeIn',
                'fadeInDown' => 'fadeInDown',
                'fadeInDownBig' => 'fadeInDownBig',
                'fadeInLeft' => 'fadeInLeft',
                'fadeInLeftBig' => 'fadeInLeftBig',
                'fadeInRight' => 'fadeInRight',
                'fadeInRightBig' => 'fadeInRightBig',
                'fadeInUp' => 'fadeInUp',
                'fadeInUpBig' => 'fadeInUpBig',
                'fadeInTopLeft' => 'fadeInTopLeft',
                'fadeInTopRight' => 'fadeInTopRight',
                'zoomIn' => 'zoomIn',
                'zoomInDown' => 'zoomInDown',
                'zoomInLeft' => 'zoomInLeft',
                'zoomInRight' => 'zoomInRight',
                'zoomInUp' => 'zoomInUp',
                'slideInDown' => 'slideInDown',
                'slideInLeft' => 'slideInLeft',
                'slideInRight' => 'slideInRight',
                'slideInUp' => 'slideInUp',
            );
        }

        /**
         * Get animation out list
         * @return [array]
         */
        public static function get_animation_out()
        {
            return array(
                'backOutDown' => 'backOutDown',
                'backOutLeft' => 'backOutLeft',
                'backOutRight' => 'backOutRight',
                'backOutUp' => 'backOutUp',
                'fadeOut' => 'fadeOut',
                'fadeOutDown' => 'fadeOutDown',
                'fadeOutDownBig' => 'fadeOutDownBig',
                'fadeOutLeft' => 'fadeOutLeft',
                'fadeOutLeftBig' => 'fadeOutLeftBig',
                'fadeOutRight' => 'fadeOutRight',
                'fadeOutRightBig' => 'fadeOutRightBig',
                'fadeOutUp' => 'fadeOutUp',
                'fadeOutUpBig' => 'fadeOutUpBig',
                'fadeOutTopLeft' => 'fadeOutTopLeft',
                'fadeOutTopRight' => 'fadeOutTopRight',
                'zoomOut' => 'zoomOut',
                'zoomOutDown' => 'zoomOutDown',
                'zoomOutLeft' => 'zoomOutLeft',
                'zoomOutRight' => 'zoomOutRight',
                'zoomOutUp' => 'zoomOutUp',
                'slideOutDown' => 'slideOutDown',
                'slideOutLeft' => 'slideOutLeft',
                'slideOutRight' => 'slideOutRight',
                'slideOutUp' => 'slideOutUp',
            );

        }

        /**
         * Get social media list
         * @return [array]
         */
        public static function get_social_media()
        {
            return array(
                'facebook' => 'Facebook',
                'twitter' => 'Twitter',
                'instagram' => 'Instagram',
                'youtube' => 'Youtube',
                'tiktok' => 'Tiktok',
                'vimeo' => 'Vimeo',
                'pinterest' => 'Pinterest',
                'dribbble' => 'Dribbble',
                'whatsapp' => 'Whatsapp',
                'telegram-plane ' => 'Telegram',
                'behance' => 'Behance',
                'soundcloud' => 'Soundcloud',
                'tumblr' => 'Tumblr',
                'github' => 'Github',
                'medium' => 'Medium',
                'dribbble ' => 'Dribbble',
                'slack' => 'Slack',
            );
        }

        /**
         * Get terms wp taxonomy
         * @param  [taxonomy name] $term [insert taxonomy name]
         * @return [array]
         */
        public static function get_terms($term, $id = '')
        {
            $terms = array();

            if (!empty($id)) {
                $terms = wp_get_post_terms($id, $term);
            } else {
                $terms = get_terms($term);
            }

            if (!empty($terms) && !is_wp_error($terms)) {
                foreach ($terms as $key => $term) {
                    $data[$term->term_id] = $term->name;
                }
            } else {
                $data = array('Not found terms');
            }

            return $data;
        }

        /**
         * Get all post list
         * @param  [post type] $post_type [insert post type]
         * @param  string $perpage   [count post list]
         * @return [array]
         */
        public static function get_posts($post_type, $perpage = '')
        {
            $post_type_name = str_replace('-', ' ', $post_type);

            $args = array(
                'post_type' => $post_type,
                'posts_per_page' => !empty($perpage) ? $perpage : -1,
            );

            $posts = get_posts($args);

            if ($posts) {
                foreach ($posts as $post) {
                    setup_postdata($post);

                    $data[$post->ID] = $post->post_title;
                }
                wp_reset_postdata();
            } else {
                $data[] = sprintf(__('No %s result', RT_THEME_DOMAIN), $post_type_name);
            }

            return $data;
        }

        /**
         * Get all user
         * @return [array]
         */
        public static function get_user()
        {
            $args = array(
                'orderby' => 'display_name',
            );

            // Create the WP_User_Query object
            $users = get_users();
            foreach ($users as $key => $user) {
                $data[$user->ID] = $user->display_name;
            }

            return $data;
        }

        public static function include_part($template = '')
        {
            include dirname(__FILE__) . '/' . $template . '.php';
        }

        /**
         * Set css class attribute with filter hook
         * @param string $filter  [filter hoook]
         * @param array  $classes [classes]
         */
        public static function set_class($filter = '', $classes = array())
        {
            if (!empty($filter)) {
                $class_output = apply_filters($filter, join(' ', array_unique($classes)));
            } else {
                $class_output = join(' ', array_unique($classes));
            }
            return 'class="' . $class_output . '"';
        }

        /**
         * Get an array of all available post type.
         * @return [array]
         */
        public static function get_post_types($post_type = '')
        {
            $items = array();

            // Get the post types.
            $post_types = get_post_types(
                array(
                    'public' => true,
                ),
                'objects'
            );

            // add all choose
            if ($post_type) {
                $item[''] = $post_type;
            }

            // Build the array.
            foreach ($post_types as $post_type) {
                $items[$post_type->name] = $post_type->labels->name;
            }
            return $items;
        }

        /**
         * Get an array of publicly-querable taxonomies.
         *
         * @static
         * @access public
         * @return array
         */
        public static function get_taxonomies()
        {
            $items = array();

            // Get the taxonomies.
            $taxonomies = get_taxonomies(
                array(
                    'public' => true,
                )
            );

            // Build the array.
            foreach ($taxonomies as $taxonomy) {
                $id = $taxonomy;
                $taxonomy = get_taxonomy($taxonomy);
                $items[$id] = $taxonomy->labels->name;
            }

            return $items;
        }

        /**
         * Get image list
         * @return [array]
         */

        public static function get_image_size()
        {
            $images['none'] = 'Hidden Image';
            $images['full'] = 'Original Image';

            // get default image
            foreach (get_intermediate_image_sizes() as $key => $image) {
                $images[$image] = $image;
            }

            return $images;
        }

        /**
         * reuse custom query argument
         * @param array $args ['query argument']
         * @return [array]
         */
        public static function query($args = array())
        {

            $query_by = $args;

            // Get post by category
            if (!empty($args['query_by']) && $query_by['post_type'] == 'post' && $args['query_by'] == 'category') {
                $query_by['cat'] = $args['category'];
            }

            // Get post by tag
            if (!empty($args['query_by']) && $args['query_by'] == 'tags') {
                $query_by['tag__and'] = $args['tags'];
            }

            // Get post by manually post id
            if (!empty($args['query_by']) && $args['query_by'] == 'manually') {
                $query_by['post__in'] = $args['post_id'];
            }

            // Get post by custom taxonomy
            if (!empty($args['query_by']) && $args['query_by'] == 'term') {
                $query_by['tax_query'][] = array(
                    'taxonomy' => $args['taxonomy'],
                    'field' => 'term_id',
                    'terms' => $args['term'],
                    'operator' => 'IN',
                );
            }

            // Get post by metabox name post_featured
            // This aviable from ACF or hardcode custom metabox each post
            if (!empty($args['query_by']) && $args['query_by'] == 'post_featured') {
                $query_by['meta_query'][] = array(
                    'key' => 'post_featured',
                    'value' => 1,
                );
            }


            // Order by total sale
            if (!empty($args['orderby']) && $args['orderby'] == 'total_sales') {
                $query_by['meta_key'] = 'total_sales';
                $query_by['orderby'] = 'meta_value_num';
            }

            // Order post by Most Viewer
            if (!empty($args['orderby']) && $args['orderby'] == 'wp_post_views_count') {
                $query_by['meta_key'] = 'wp_post_views_count';
                $query_by['orderby'] = 'meta_value_num';
            }

            // Order post by most comment
            if (!empty($args['orderby']) && $args['orderby'] == 'comment_count') {
                $query_by['orderby'] = 'comment_count';
            }


            // Get WooCommerce products by featured product
            if (!empty($args['query_by']) && $query_by['post_type'] == 'product' && $args['query_by'] == 'featured') {
                $query_by['tax_query'][] = array(
                    'taxonomy' => 'product_visibility',
                    'field' => 'name',
                    'terms' => 'featured',
                    'operator' => 'IN',
                );
            }

            // Get WooCommerce products by input categories
            if (!empty($args['query_by']) && $query_by['post_type'] == 'product' && $args['query_by'] == 'category') {
                $query_by['tax_query'][] = array(
                    'taxonomy' => 'product_cat',
                    'field' => 'term_id',
                    'terms' => $args['category'],
                    'operator' => 'IN',
                );
            }
            
            // Default Argument
            $query_default = array(
                'post_type' => 'post',
                'posts_per_page' => 5,
                'post_status' => 'publish',
            );

            $query_args = wp_parse_args($query_by, $query_default);
            $query_id = !empty($args['id']) ? $args['id'] : 1;

            // Merge Array
            return apply_filters("retheme_query_{$query_id}", $query_args);
        }

        /**
         * Is_ajax - Returns true when the page is loaded via ajax.
         *
         * @return bool
         */
        public function is_ajax()
        {
            return function_exists('wp_doing_ajax') ? wp_doing_ajax() : Constants::is_defined('DOING_AJAX');
        }

        // end class
    }
}
