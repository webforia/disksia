/**
 * Ajax loop load more
 *
 * load more loop post by ajax scripts
 * @version 2.0.0
 */
 const loadmores = document.querySelectorAll(".js-loop-load");

 loadmores.forEach((loadmore) => {
   loadmore.addEventListener("click", (event) => {
     event.preventDefault();
 
     const _this = event.currentTarget;
     const target = _this.getAttribute("data-target");
     const targetElement = document.getElementById(target);
     const currentPage = _this.getAttribute("data-current-page");
     const totalPage = _this.getAttribute("data-total-page");
     const postTotal = _this.getAttribute("data-post-total");
     const settings = JSON.parse(_this.getAttribute("data-setting"));
     const items = targetElement.querySelectorAll(".post-item");
 
     //  Disable click if loading
     if (_this.classList.contains("is-loading")) {
       return false;
     }
 
     // Get number next page
     const pageAction = parseInt(currentPage) + 1;
 
     if (_this.classList.contains("prev") == false) {
       const pageAction = parseInt(currentPage) - 1;
     }
 
     jQuery.ajax({
       url: ajax_loop_var.ajaxurl,
       type: "post",
       dataType: "json",
       data: {
         action: "ajax_loop_result",
         page: pageAction,
         query: settings,
         check_nonce: ajax_loop_var.check_nonce,
       },
       beforeSend: function (response) {
         _this.classList.add("is-loading");
         _this.innerHTML = ajax_loop_var.text_domain.loading;
         targetElement.classList.add("rt-loading");
 
         // Mark current item show
         items.forEach((item) => {
           item.classList.add("is-show");
         });
         
       },
       success: function (response) {
         // Get next or prev page number from current page
         const nextPage = parseInt(currentPage) + 1;
         const prevPage = parseInt(currentPage) - 1;
 
         // Check masonry layout
         if (targetElement.classList.contains("js-masonry")) {
           // add new content
           targetElement.innerHTML += response;
 
           // Relayout masonry
           var masonry = new Masonry(targetElement, {
             columnWidth: ".post-item",
             itemSelector: ".post-item",
             percentPosition: true,
             horizontalOrder: true,
           });
 
           // layout Masonry after each image loads
           imagesLoaded(targetElement).on("progress", function () {
             masonry.layout();
           });
         } else {
           targetElement.innerHTML += response;
         }
 
         // Set new current page
         if (_this.classList.contains("prev")) {
           _this.setAttribute("data-current-page", prevPage);
         } else {
           _this.setAttribute("data-current-page", nextPage);
         }
 
         // Add animation to new element
         const NewElements = targetElement.querySelectorAll(
           ".flex-item:not(.is-show)"
         );
 
         NewElements.forEach((NewElement) => {
           animateCSS(NewElement, "zoomIn", "300");
           NewElement.classList.add("is-show");
         });
 
         // Remove loading animation
         setTimeout(function () {
           targetElement.classList.remove("rt-loading");
           _this.classList.remove("is-loading");
           _this.innerHTML = ajax_loop_var.text_domain.loadmore;
         }, 500);
 
         // Remove button loadmore on lastest page
         if (nextPage >= totalPage) {
           _this.parentNode.remove();
         }
 
         // run countdown class
         if (targetElement.querySelector(".wsb-countdown") != null) {
           new countdown();
         }
 
         // change result on WooCommerce
         const resultCount = targetElement.childElementCount;
 
         let str = ajax_loop_var.text_domain.result;
         let str1 = str.replace("%s", resultCount);
         let resultText = str1.replace("%s2", postTotal);
 
         const resultCountWrapper = document.querySelector(
           ".woocommerce-result-count"
         );
 
         if (resultCountWrapper) {
           resultCountWrapper.innerHTML = resultText;
         }
       },
     });
   });
 });
 