<?php
/**
 * Ajax loop
 *
 * Handle ajax loop on archive default archieve and CPT
 *
 * @package     Retheme
 * @category    Core
 * @author      Webforia Studio
 * @version     2.4.0
 */

namespace Retheme;

use Retheme\Helper;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('Retheme\Class_Ajax')) {
    class Class_Ajax
    {
        public function __construct()
        {
            add_action('wp_ajax_ajax_loop_result', [$this, 'loop_result']);
            add_action('wp_ajax_nopriv_ajax_loop_result', [$this, 'loop_result']);
            add_action('wp_enqueue_scripts', [$this, 'load_scripts']);

        }

        /**
         * return query
         *
         * @return [post]
         */
        public function loop_result()
        {

            if (check_ajax_referer('retheme-loop-nonce', 'check_nonce')) {
                // get setting form json
                $settings = $_POST['query'];

                // Merge Array with current page and status publish
                $args = wp_parse_args(Helper::query($settings), array(
                    'paged' => $_POST['page'],
                    'post_status' => 'publish',
                ));

                $the_query = new \WP_Query($args);

                ob_start();

                if ($the_query->have_posts()) {
                    while ($the_query->have_posts()): $the_query->the_post();

                        include locate_template($settings['template_part'] . '.php');

                    endwhile;

                    wp_reset_postdata();

                }

                $ajax_results = ob_get_clean();

                wp_send_json($ajax_results);
            }

        }

        /**
         * load scripts
         *
         * @return [inject ajax-loop.js, js variable]
         */
        public function load_scripts()
        {
            wp_enqueue_script('ajax-loop', get_template_directory_uri() . '/core/classes/assets/js/ajax-loop.min.js', array('jquery'), '1.0.0', true);

            wp_localize_script('ajax-loop', 'ajax_loop_var', array(
                'ajaxurl' => admin_url('admin-ajax.php'),
                'check_nonce' => wp_create_nonce('retheme-loop-nonce'),
                'text_domain' => [
                    'result' => __('Showing %s of %s2 results', RT_THEME_DOMAIN),
                    'loading' => __('Loading', RT_THEME_DOMAIN),
                    'loadmore' => __('Load More', RT_THEME_DOMAIN),
                ],
            ));

        }

        /* end class */
    }
    new Class_Ajax;
}
