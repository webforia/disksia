<?php
/**
 * Elementor Helper methods
 *
 * @package     Retheme
 * @category    Core
 * @author      Webforia Studio
 * @version     2.2.0
 */

namespace Retheme;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Background;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Widget_Base;
use Retheme\Helper;
use Retheme\HTML;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

add_action('elementor/widgets/widgets_registered', function () {

    // No need to proceed if this class already exists.
    if (!class_exists('Retheme\Elementor_Base')) {
        class Elementor_Base extends Widget_Base
        {

            public $textdomain = 'retheme';

            public function get_name()
            {
                return 'retheme-base';
            }

            public function setting_carousel($args = '')
            {

                $settings = wp_parse_args($args, [
                    'carousel' => 'yes',
                    'slide' => 'yes',
                    'gap' => 'yes',
                    'navigation' => 'yes',
                ]);

                $this->start_controls_section(
                    'setting_carousel',
                    [
                        'label' => __('Carousel', RT_THEME_DOMAIN),
                    ]
                );

                if ($settings['carousel'] === 'yes') {
                    $this->add_control(
                        'carousel',
                        [
                            'label' => __('Carousel', RT_THEME_DOMAIN),
                            'type' => Controls_Manager::SWITCHER,
                            'default' => 'no',
                            'label_off' => __('Off', RT_THEME_DOMAIN),
                            'label_on' => __('On', RT_THEME_DOMAIN),
                            'return_value' => 'yes',

                        ]
                    );
                }
                if ($settings['slide'] === 'yes') {
                    $slides_to_show = range(1, 10);
                    $slides_to_show = array_combine($slides_to_show, $slides_to_show);
                    $this->add_responsive_control(
                        'slider_item',
                        [
                            'label' => __('Slides to Show', RT_THEME_DOMAIN),
                            'type' => Controls_Manager::SELECT,
                            'options' => $slides_to_show,
                            'devices' => ['desktop', 'tablet', 'mobile'],
                            'desktop_default' => 4,
                            'tablet_default' => 3,
                            'mobile_default' => 1,

                        ]
                    );

                }
                if ($settings['gap'] === 'yes') {

                    $this->add_control(
                        'slider_gap',
                        [
                            'label' => __('Slide Spacing', RT_THEME_DOMAIN),
                            'description' => __('spacing between Carousel item', RT_THEME_DOMAIN),
                            'type' => Controls_Manager::SLIDER,
                            'size_units' => ['px'],
                            'range' => [
                                'px' => [
                                    'min' => 0,
                                    'max' => 30,
                                    'step' => 1,
                                ],
                            ],
                            'default' => [
                                'unit' => 'px',
                                'size' => 20,
                            ],
                        ]
                    );
                }
                if ($settings['navigation'] === 'yes') {
                    $this->add_control(
                        'slider_nav',
                        [
                            'label' => __('Navigation', RT_THEME_DOMAIN),
                            'type' => Controls_Manager::SELECT,
                            'default' => 'beside',
                            'options' => array(
                                'none' => 'None',
                                'beside' => 'On Side',
                            ),
                        ]
                    );

                }

                $this->add_control(
                    'slider_pagination',
                    [
                        'label' => __('Pagination', RT_THEME_DOMAIN),
                        'type' => Controls_Manager::SWITCHER,
                        'default' => 'no',
                        'label_off' => __('Off', RT_THEME_DOMAIN),
                        'label_on' => __('On', RT_THEME_DOMAIN),
                        'return_value' => 'yes',

                    ]
                );

                $this->add_control(
                    'slider_loop',
                    [
                        'label' => __('Infinite Loop', RT_THEME_DOMAIN),
                        'type' => Controls_Manager::SWITCHER,
                        'default' => 'no',
                        'label_off' => __('Off', RT_THEME_DOMAIN),
                        'label_on' => __('On', RT_THEME_DOMAIN),
                        'return_value' => 'yes',

                    ]
                );

                $this->add_control(
                    'slider_auto_play',
                    [
                        'label' => __('Auto Play', RT_THEME_DOMAIN),
                        'type' => Controls_Manager::SWITCHER,
                        'default' => 'no',
                        'label_on' => __('On', RT_THEME_DOMAIN),
                        'label_off' => __('Off', RT_THEME_DOMAIN),
                        'return_value' => 'yes',
                    ]
                );

                $this->end_controls_section();

                $this->start_controls_section(
                    'style_slider_navigation',
                    [
                        'label' => __('Carousel', RT_THEME_DOMAIN),
                        'tab' => Controls_Manager::TAB_STYLE,
                    ]
                );

                $this->add_control(
                    'pagination_color',
                    [
                        'label' => __('Pagination Color', RT_THEME_DOMAIN),
                        'type' => Controls_Manager::COLOR,
                        'selectors' => [
                            '{{WRAPPER}} .swiper-pagination-bullet' => 'background-color: {{VALUE}};',
                        ],
                    ]
                );

                $this->add_control(
                    'pagination_color_hover',
                    [
                        'label' => __('Pagination Color :Active', RT_THEME_DOMAIN),
                        'type' => Controls_Manager::COLOR,
                        'selectors' => [
                            '{{WRAPPER}} .swiper-pagination-bullet.swiper-pagination-bullet-active' => 'background-color: {{VALUE}};',
                        ],
                    ]
                );
                $this->end_controls_section();

            }


            /**
             * Pagination control
             *
             * @return void
             */
            public function setting_pagination()
            {

                $this->start_controls_section(
                    'pagination',
                    [
                        'label' => __('Pagination', RT_THEME_DOMAIN),
                    ]
                );

                $this->add_control(
                    'pagination_style',
                    [
                        'label' => __('Pagination Style', RT_THEME_DOMAIN),
                        'type' => Controls_Manager::SELECT,
                        'default' => 'no_pagination',
                        'options' => array(
                            'no_pagination' => 'No Pagination',
                            'loadmore' => 'Load More',
                        ),
                    ]
                );

                $this->end_controls_section();

            }

            public function setting_header_block()
            {
                $this->start_controls_section(
                    'header',
                    [
                        'label' => __('Header', RT_THEME_DOMAIN),
                    ]
                );
                $this->add_control(
                    'header_title',
                    [
                        'label' => __('Heading', RT_THEME_DOMAIN),
                        'type' => Controls_Manager::TEXT,
                        'default' => __('Heading title', RT_THEME_DOMAIN),
                    ]
                );
                $this->add_control(
                    'header_style',
                    [
                        'label' => __('Style', RT_THEME_DOMAIN),
                        'type' => Controls_Manager::SELECT,
                        'default' => 'style-1',
                        'options' => [
                            'style-1' => __('Style 1', RT_THEME_DOMAIN),
                            'style-2' => __('Style 2', RT_THEME_DOMAIN),
                            'style-3' => __('Style 3', RT_THEME_DOMAIN),
                            'style-4' => __('Style 4', RT_THEME_DOMAIN),
                        ],
                    ]
                );
                $this->end_controls_section();

                /* add style header block */
                $this->start_controls_section(
                    'style_header',
                    [
                        'label' => __('Header', RT_THEME_DOMAIN),
                        'tab' => Controls_Manager::TAB_STYLE,
                    ]
                );

                $this->add_control(
                    'style_header_color',
                    [
                        'label' => __('Color', RT_THEME_DOMAIN),
                        'type' => Controls_Manager::COLOR,
                        'selectors' => [
                            '{{WRAPPER}} .rt-header-block .rt-header-block__title' => 'color: {{VALUE}};',
                        ],
                    ]
                );

                $this->add_control(
                    'style_header_color_line_primary',
                    [
                        'label' => __('Color Line Primary', RT_THEME_DOMAIN),
                        'type' => Controls_Manager::COLOR,
                        'selectors' => [
                            '{{WRAPPER}} .rt-header-block--style-1 .rt-header-block__title' => 'border-color: {{VALUE}};',
                            '{{WRAPPER}} .rt-header-block--style-2 .rt-header-block__title' => 'border-color: {{VALUE}};',
                            '{{WRAPPER}} .rt-header-block--style-3 .rt-header-block__title' => 'background-color: {{VALUE}};',
                            '{{WRAPPER}} .rt-header-block--style-4 .rt-header-block__title' => 'border-color: {{VALUE}};',
                        ],
                    ]
                );

                $this->add_control(
                    'style_header_color_line_second',
                    [
                        'label' => __('Color Line Second', RT_THEME_DOMAIN),
                        'type' => Controls_Manager::COLOR,
                        'selectors' => [
                            '{{WRAPPER}} .rt-header-block--style-1 ' => 'border-color: {{VALUE}};',
                            '{{WRAPPER}} .rt-header-block--style-3 ' => 'border-color: {{VALUE}};',
                            '{{WRAPPER}} .rt-header-block--style-4::before' => 'background-color: {{VALUE}};',
                        ],
                    ]
                );

                $this->add_responsive_control(
                    'header_block_margin',
                    [
                        'label' => __('Margin Bottom', RT_THEME_DOMAIN),
                        'type' => Controls_Manager::SLIDER,
                        'size_units' => ['px', '%'],
                        'selectors' => [
                            '{{WRAPPER}} .rt-header-block' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                        ],
                    ]
                );

                $this->end_controls_section();
            }

            /**
             * HELPER elementor loop query
             */
            public static function elementor_loop($settings = array())
            {
                // get query argument
                $the_query = new \WP_Query(Helper::query($settings));

                // class wrapper
                $classes[] = !empty($settings['class_wrapper']) ? $settings['class_wrapper'] : '';

                // start loop
                if ($the_query->have_posts()):

                    // layout column
                    if ($settings['carousel'] != 'yes') {

                        // class wrapper
                        $column_md = ($settings['setting_column'])? $settings['setting_column']: 3;
                        $column_sm = ($settings['setting_column_tablet'])? $settings['setting_column_tablet']: 2;
                        $column_xs = ($settings['setting_column_mobile'])? $settings['setting_column_mobile']: 1;

                        $classes[] = 'grids';
                        $classes[] = 'grids-md-'.$column_md;
                        $classes[] = 'grids-sm-'.$column_sm;
                        $classes[] = 'grids-xs-'.$column_xs;
                        $classes[] = ($settings['layout_masonry'] == 'yes') ? 'grid-masonry js-masonry' : '';

                        echo HTML::open(array(
                            'id' => "block_{$settings['id']}",
                            'class' => $classes,
                        ));

                        while ($the_query->have_posts()): $the_query->the_post();
                            include locate_template($settings['template_part'] . '.php');
                        endwhile;

                        echo HTML::close();

                        // add pagination
                        echo HTML::pagination(wp_parse_args($settings, array(
                            'target' => "block_{$settings['id']}",
                            'pagination_style' => 'number',
                            'post_type' => 'post',
                            'posts_per_page' => 9,
                            'total' => $the_query->max_num_pages,
                            'post_total' => $the_query->found_posts,
                            'format' => '?paged=%#%',
                            'current' => max(1, get_query_var('paged')),
                            'add_args' => [
                                'post_type' => $settings['post_type'],
                            ],
                        )));

                    }

                    // layout carousel
                    if ($settings['carousel'] == 'yes') {

                        // Header nav
                        $slider_nav = array();
                        if ($settings['slider_nav'] == 'header') {
                            $slider_nav = array(
                                'navigation' => [
                                    'nextEl' => "#header-{$settings['id']} .js-slider-next",
                                    'prevEl' => "#header-{$settings['id']} .js-slider-prev",
                                ],
                            );
                        }

                        // remove internal nav
                        if ($settings['slider_nav'] != 'beside') {
                            $classes[] = 'rt-swiper--nonav';
                        }


                        // Pagination
                        $slider_pagination = array();
                        if ($settings['slider_pagination'] == 'yes') {
                            $slider_pagination = array(
                                'pagination' => [
                                    'el' => '.swiper-pagination',
                                    'clickable' => true,
                                ],
                            );
                            $classes[] = 'rt-swiper--pagination-outer';
                        }

                        // auto play

                        $slider_option = array_merge($slider_nav, $slider_pagination);
                       
                        echo HTML::before_slider(wp_parse_args($slider_option, array(
                            'id' => "rt-swiper-{$settings['id']}",
                            'class' => implode(' ', $classes),
                            'items-lg' => ($settings['slider_item'])? $settings['slider_item']: 4,
                            'items-md' => ($settings['slider_item_tablet'])? $settings['slider_item_tablet']: 3,
                            'items-sm' => ($settings['slider_item_mobile']? $settings['slider_item_mobile']: 1),
                            'loop' => ($settings['slider_loop'] == 'yes') ? true : false,
                            'spaceBetween' => !empty($settings['slider_gap']['size']) ? $settings['slider_gap']['size'] : 20,
                            'autoplay' => ($settings['slider_auto_play'] == 'yes') ? true : false,
                            'sameheight' => true,
                        )));

                        while ($the_query->have_posts()): $the_query->the_post();
                            echo HTML::open(['class' => 'swiper-slide']);
                            include locate_template($settings['template_part'] . '.php');
                            echo HTML::close();
                        endwhile;

                        echo HTML::after_slider();
                    }

                    wp_reset_postdata();
                else:
                    do_action('rt_post_none');
                endif;

            }

            /* end class */
        }
    }

});
