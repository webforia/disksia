<?php
/**
 * HTML methods
 *
 * @package     Retheme
 * @category    Core
 * @author      Webforia Studio
 * @version     2.1.0
 */

namespace Retheme;

use Retheme\Helper;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

// No need to proceed if this class already exists.
if (!class_exists('Retheme\HTML')) {
    class HTML
    {

        /**
         * open tag html helper
         * @param  string $tag  [tag html]
         * @param  array  $args [setting]
         * @return [type]       [html]
         */
        public static function open($args = '', $tag = '')
        {
            $tag = !empty($tag) ? $tag : 'div';

            if (is_array($args)) {
                foreach ($args as $key => $attribute) {
                    if ($key === 'class') {
                        if (is_array($attribute)) {
                            $data[] = $key . '=' . '"' . join(' ', $attribute) . '"';
                        } else {
                            $data[] = $key . '=' . '"' . $attribute . '"';
                        }
                    } elseif ($key === 'id') {
                        $data[] = $key . '=' . '"' . $attribute . '"';
                    } else {
                        $data[] = 'data-' . $key . '=' . '"' . $attribute . '"';
                    }
                }
                $attribute = join(' ', $data);
            } else {
                $attribute = "class='{$args}'";
            }

            return "<{$tag} {$attribute}>";

        }

        /**
         * Close tag helper
         * @param  string $tag  [tag html]
         * @param  string $args [setting]
         * @return [type]       [html]
         */
        public static function close($tag = '', $args = '')
        {
            $tag = !empty($tag) ? $tag : 'div';

            return "</{$tag}>";
        }

        /**
         * open tag slider open
         * @param  array  $args [setting]
         * @return [type]       [html slider]
         */
        public static function before_slider($args = array())
        {
            // breakpoint
            $items_lg = !empty($args['items-lg']) ? $args['items-lg'] : 1;
            $items_md = !empty($args['items-md']) ? $args['items-md'] : 1;
            $items_sm = !empty($args['items-sm']) ? $args['items-sm'] : 1;
            $gap_lg = !empty($args['gap-lg']) ? $args['gap-lg'] : 30;
            $gap_md = !empty($args['gap-md']) ? $args['gap-md'] : 20;
            $gap_sm = !empty($args['gap-sm']) ? $args['gap-sm'] : 15;
            $class_container = !empty($args['class']) ? $args['class'] : '';

            // merge option from slider
            $options = wp_parse_args($args, array(
                'slidesPerView' => 'auto',
                'breakpoints' => array(
                    '960' => array(
                        'slidesPerView' => $items_lg,
                        'spaceBetween' => $gap_lg,
                    ),
                    '720' => array(
                        'slidesPerView' => $items_md,
                        'spaceBetween' => $gap_md,
                    ),
                    '320' => array(
                        'slidesPerView' => $items_sm,
                        'spaceBetween' => $gap_sm,
                    ),
                ),
                'navigation' => [
                    'nextEl' => '.swiper-button-next',
                    'prevEl' => '.swiper-button-prev',
                ],
                'watchOverflow' => true,
                'setWrapperSize' => true,
                'watchSlidesProgress' => true,
                'watchSlidesVisibility' => true,
            ));

            // same height item
            $sameheight = !empty($args['sameheight']) ? 'rt-swiper--stretch' : '';

            // open slider container
            $html = self::open([
                'id' => !empty($args['id']) ? $args['id'] : 'rt-swiper-' . rand(),
                'class' => ['rt-swiper js-swiper', $sameheight, $class_container],
                'options' => htmlspecialchars(json_encode($options)),
            ]);

            $html .= self::open(['class' => ['swiper-container']]);

            // open slider wrapper
            $html .= self::open(['class' => ['swiper-wrapper']]);

            return $html;

        }

        /**
         * Close tag slider
         * @return [type] [html]
         */
        public static function after_slider()
        {
            $html = '</div>';
            $html .= '<div class="swiper-pagination"></div>';
            $html .= '<div class="swiper-button-next"></div>';
            $html .= '<div class="swiper-button-prev"></div>';
            $html .= '</div>';
            $html .= '</div>';
            return $html;

        }

        /**
         * Show pagination
         * @param array $args [setting query]
         * @return ['html']
         */
        public static function pagination($args = array())
        {
            $format = !empty($args['format']) ? $args['format'] : '';
            $output = '';

            if ($args['total'] > 1 ) {
                if ($args['pagination_style'] == 'number') {

                    ob_start();

                    echo '<div class="rt-pagination">';

                    echo paginate_links(wp_parse_args($args, array(
                        'format' => $format,
                        'prev_text' => '<span class="rt-icon-chevron-left"></span>',
                        'next_text' => '<span class="rt-icon-chevron-right"></span>',
                    )));

                    echo '</div>';

                    $output = ob_get_clean();

                }

                if ($args['pagination_style'] == 'loadmore') {

                    $options = htmlspecialchars(json_encode($args));

                    echo '<div class="rt-pagination rt-pagination--loadmore">';
                    echo '<a
                        data-target="' . $args['target'] . '"
                        data-current-page="1"
                        data-post-perpage="' . $args['posts_per_page'] . '"
                        data-total-page="' . $args['total'] . '"
                        data-post-total="' . $args['post_total'] . '"
                        data-setting= "' . $options . '"
                        class="rt-pagination__button js-loop-load rt-btn rt-btn--border">' . __('Load More', RT_THEME_DOMAIN) . '</a>';
                    echo '</div>';

                }

                if ($args['pagination_style'] == 'link') {

                    $link_url = esc_url_raw($args['link_url']);
                    $link_text = $args['link_text'];

                    echo '<div class="rt-pagination rt-pagination--link">';
                    echo "<a class='rt-btn rt-btn--border' href='{$link_url}'>{$link_text}</a>";
                    echo '</div>';

                }

                return $output;
            }

        }

        /**
         * Header block
         * @param  $args attribute header
         * @return HTML
         */
        public static function header_block($args = array())
        {
            $id = !empty($args['id']) ? "id='{$args['id']}'" : '';
            $class = !empty($args['class']) ? "class='rt-header-block {$args['class']}'" : "class='rt-header-block'";
            $target = !empty($args['target']) ? $args['target'] : '';

            if (!empty($args['title'])) {
                $output = "<div {$id} {$class}>";

                $output .= "<div class='rt-header-block__inner'>";
                $output .= "<h2 class='rt-header-block__title'>{$args['title']}</h2>";

                if (!empty($args['desc'])):
                    $output .= "<div class='rt-header-block__desc'>{$args['desc']}</div>";
                endif;

                $output .= "</div>";

                if (!empty($args['nav'])):
                    $output .= "<div class='rt-header-block__nav'>";
                    $output .= "<a class='rt-header-block__prev js-swiper-prev'><i class='rt-icon-chevron-left'></i></a>";
                    $output .= "<a class='rt-header-block__next js-swiper-next'><i class='rt-icon-chevron-right'></i></a>";
                    $output .= "</div>";
                endif;

                if (!empty($args['link_text'])):
                    $link_text = $args['link_text'];
                    $link_url = esc_url($args['link_url']);
                    $output .= "<div class='rt-header-block__nav'>";
                    $output .= "<a class='link-text' href='{$link_url}'>{$link_text}<i class='rt-icon-chevron-right ml-5'></i></a>";
                    $output .= "</div>";
                endif;

                $output .= '</div>';

                return $output;
            }

        }

        /**
         * show script tag
         * @param  [type] $args [value]
         * @return [type]       [tag]
         */
        public static function script($args)
        {
            $output = '<script>' . $args . '</script>';

            return $output;

        }

    }
}
