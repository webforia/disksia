<?php
/*=================================================
 *  COMMENT FORM
/*================================================= */
/**
 * change html default form html
 *
 * @param [type] $fields
 * @return void
 */
function rt_comment_form($fields)
{

    $commenter = wp_get_current_commenter();

    $req = get_option('require_name_email');
    $aria_req = ($req ? " aria-required='true'" : '');
    $html5 = current_theme_supports('html5', 'comment-form') ? 1 : 0;

    $fields = array(
        'author' => '<div class="comment-input"><div class="rt-form rt-form--overlay js-form-overlay">' . '<label class="rt-form__label" for="author">' . __('Full Name', RT_THEME_DOMAIN) . '</label> ' .
        '<input id="author" class="rt-form__input" name="author" type="text" value="' . esc_attr($commenter['comment_author']) . '" size="30"' . $aria_req . ' /></div>',

        'url' => '<div class="rt-form rt-form--overlay js-form-overlay"><label class="rt-form__label" for="url">' . __('Website (optional)') . '</label> ' .
        '<input class="rt-form__input" id="url" name="url" ' . ($html5 ? 'type="url"' : 'type="text"') . ' value="' . esc_attr($commenter['comment_author_url']) . '" size="30" /></div>',

        'email' => '<div class="rt-form rt-form--overlay js-form-overlay"><label class="rt-form__label" for="email">' . __('Email') . '</label> ' .
        '<input id="email" class="rt-form__input" name="email" type="text" value="' . esc_attr($commenter['comment_author_email']) . '" size="30"' . $aria_req . ' /></div></div>',
    );

    return $fields;
}

add_filter('comment_form_default_fields', 'rt_comment_form');

/*=================================================;
/* COMMENT AREA
/*================================================= */
/**
 * Change html text area form
 *
 * @param [type] $args
 * @return void
 */
function rt_comment_textarea($args)
{
    $args['comment_field'] = '<div class="rt-form rt-form--textarea rt-form--overlay js-form-overlay">
            <label class="rt-form__label" for="comment">' . __('Comment', RT_THEME_DOMAIN) . '</label>
            <textarea class="rt-form__input" id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea>
        </div>';
    $args['class_submit'] = 'rt-btn rt-btn--primary';

    return $args;
}
add_filter('comment_form_defaults', 'rt_comment_textarea');

/*=================================================
 *  ADD CLASS LINK COMMENT
/*================================================= */
/**
 * Replace link comment classes
 */
function rt_reply_link_class($class)
{
    $class = str_replace("class='comment-reply-link", "class='comment-reply-link rt-comment__reply", $class);

    return $class;
}
add_filter('comment_reply_link', 'rt_reply_link_class');


/*=================================================;
/* COMMENT LIST
/*================================================= */
function rt_comment_callback($comment, $args, $depth)
{
    if ('div' === $args['style']) {
        $tag = 'div';
        $add_below = 'comment';
    } else {
        $tag = 'li';
        $add_below = 'div-comment';
    }?>
    <<?php echo esc_html($tag) ?> <?php comment_class(empty($args['has_children']) ? '' : 'parent')?> id="comment-<?php comment_ID()?>">

    <?php if ('div' != $args['style']): ?>
        <div id="div-comment-<?php comment_ID()?>">
    <?php endif;?>

   <div class="rt-comment-list__item">
      <div class="rt-img rt-comment-list__avatar">
          <?php echo get_avatar($comment, $args['avatar_size']); ?>
      </div>
        <div class="rt-comment-list__body">

            <?php if ($comment->comment_approved == '0'): ?>
              <p class="rt-comment-list__approved"><?php _e('Your comment is awaiting moderation.', RT_THEME_DOMAIN);?></p>
            <?php endif;?>

            <h5 class="rt-comment-list__title">
              <a href="<?php echo get_comment_author_url(); ?>"><?php echo get_comment_author() ?></a>
              <?php if (get_the_author() == get_comment_author()): ?>
                <span class="rt-comment-list__author"><?php _e('author', RT_THEME_DOMAIN)?></span>
              <?php endif;?>
            </h5>

            <div class="rt-comment-list__meta">
              <span href="#" class="comment-list-meta-date">
              <span class="rt-icon">
                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor" class="bi bi-calendar3" viewBox="0 0 16 16">
                  <path d="M14 0H2a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z"/>
                  <path d="M6.5 7a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm-9 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm-9 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                </svg>
              </span>
                <?php printf(__('%1$s at %2$s', RT_THEME_DOMAIN), get_comment_date(), get_comment_time());?>
              </span>
            </div>

            <div class="rt-comment-list__content">
                <?php comment_text();?>
            </div>

            <div class="rt-comment-list__reply">
              <span class="rt-icon">
                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor" class="bi bi-reply" viewBox="0 0 16 16">
                  <path d="M6.598 5.013a.144.144 0 0 1 .202.134V6.3a.5.5 0 0 0 .5.5c.667 0 2.013.005 3.3.822.984.624 1.99 1.76 2.595 3.876-1.02-.983-2.185-1.516-3.205-1.799a8.74 8.74 0 0 0-1.921-.306 7.404 7.404 0 0 0-.798.008h-.013l-.005.001h-.001L7.3 9.9l-.05-.498a.5.5 0 0 0-.45.498v1.153c0 .108-.11.176-.202.134L2.614 8.254a.503.503 0 0 0-.042-.028.147.147 0 0 1 0-.252.499.499 0 0 0 .042-.028l3.984-2.933zM7.8 10.386c.068 0 .143.003.223.006.434.02 1.034.086 1.7.271 1.326.368 2.896 1.202 3.94 3.08a.5.5 0 0 0 .933-.305c-.464-3.71-1.886-5.662-3.46-6.66-1.245-.79-2.527-.942-3.336-.971v-.66a1.144 1.144 0 0 0-1.767-.96l-3.994 2.94a1.147 1.147 0 0 0 0 1.946l3.994 2.94a1.144 1.144 0 0 0 1.767-.96v-.667z"/>
                </svg>
              </span> 
              <?php comment_reply_link(array_merge($args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'])));?>
            </div>

        </div>
    </div>

    <?php if ('div' != $args['style']): ?>
    </div>
    <?php endif;?>
    <?php
}