<?php
/*=================================================;
/* LOGO
/*================================================= */
function rt_get_logo($type = '')
{
    $logo = rt_get_option('brand_logo_primary');
    $logo_sticky = wp_parse_args(rt_get_option('brand_logo_sticky'), $logo );
    $logo_mobile = wp_parse_args(rt_get_option('brand_logo_mobile'), $logo );
    $site_name = get_bloginfo('name');

    if ($type == 'mobile') {
        $html = '<div class="rt-logo-mobile">';
        $html .= '<a href="' . site_url() . '">';

        if ($logo_mobile) {
            $html .= "<img src='{$logo_mobile['url']}' width='{$logo_mobile['width']}' height='{$logo_mobile['height']}' alt='{$site_name}'>";
        } else {
            $html .= $site_name;
        }

        $html .= '</a>';
        $html .= '</div>';
    } else {
        $html = '<div class="rt-logo">';
        $html .= '<a href="' . site_url() . '">';

        if ($logo) {
            $html .= "<div class='rt-logo__primary'><img src='{$logo['url']}' width='{$logo['width']}' height='{$logo['height']}' alt='{$site_name}'></div>";
            $html .= "<div class='rt-logo__sticky'><img src='{$logo_sticky['url']}' width='{$logo_sticky['width']}' height='{$logo_sticky['height']}' alt='{$site_name}'></div>";
        } else {
            $html .= $site_name;
        }
        $html .= '</a>';
        $html .= '</div>';
    }

    return $html;
}

/*=================================================;
/* HEADER BLOCK
/*================================================= */
function rt_get_header_block($args)
{
    return Retheme\HTML::header_block($args);
}

/*=================================================;
/* BEFORE SLIDE
/*================================================= */
function rt_get_before_slider($args)
{
    // merge option from slider
    $options = wp_parse_args($args, [
        'slidesPerView' => 'auto',
        'breakpoints' => [
            '960' => [
                'slidesPerView' => !empty($args['items-lg']) ? $args['items-lg'] : 1,
                'spaceBetween' => !empty($args['gap-lg']) ? $args['gap-lg'] : 30,
            ],
            '720' => [
                'slidesPerView' => !empty($args['items-md']) ? $args['items-md'] : 1,
                'spaceBetween' => !empty($args['gap-md']) ? $args['gap-md'] : 20,
            ],
            '320' => [
                'slidesPerView' => !empty($args['items-sm']) ? $args['items-sm'] : 1,
                'spaceBetween' => !empty($args['gap-sm']) ? $args['gap-sm'] : 15,
            ],
        ],
        'navigation' => [
            'nextEl' => '.swiper-button-next',
            'prevEl' => '.swiper-button-prev',
        ],
        'watchOverflow' => true,
        'setWrapperSize' => true,
        'watchSlidesProgress' => true,
        'watchSlidesVisibility' => true,
    ]);

    // open slider container
    $slider_id = !empty($args['id']) ? $args['id'] : 'rt-swiper-' . rand();
    $slider_class = [
        'main' => 'rt-swiper js-swiper',
        'container' => !empty($args['class']) ? $args['class'] : '',
        'height' => !empty($args['sameheight']) ? 'rt-swiper--stretch' : '',
    ];

    $slider_options = htmlspecialchars(json_encode($options));

    $html = '<div id="' . $slider_id . '" class="' . implode(' ', $slider_class) . '" data-options="' . $slider_options . '">';
    $html .= '<div class="swiper-container">';
    $html .= '<div class="swiper-wrapper">';

    return $html;
}

/*=================================================;
/* AFTER SLIDE
/*================================================= */
function rt_get_after_slider()
{
    $html = '</div>';
    $html .= '<div class="swiper-pagination"></div>';
    $html .= '<div class="swiper-button-next"></div>';
    $html .= '<div class="swiper-button-prev"></div>';
    $html .= '</div>';
    $html .= '</div>';
    return $html;
}

/*=================================================;
/* BEFORE SLIDE ITEM
/*================================================= */
function rt_get_before_slide()
{
    return '<div class="swiper-slide">';
}

/*=================================================;
/* AFTER SLIDE
/*================================================= */
function rt_get_after_slide()
{
    return '</div>';
}

/*=================================================;
/* PAGE TITLE
/*================================================= */
function rt_get_page_title()
{
    $title = __('Archives', RT_THEME_DOMAIN);
    $desc = '';

    if (is_archive()) {
        $title = get_the_archive_title();
    }

    if (is_singular()) {
        $title = get_the_title();
    }

    if (is_post_type_archive()) {
        $title = post_type_archive_title('', false);
    }

    if (is_author()) {
        $title = get_the_author();
    }

    if (is_year()) {
        $title = get_the_date(_x('Y', 'yearly archives date format'));
    }

    if (is_month()) {
        $title = get_the_date(_x('F Y', 'monthly archives date format'));
    }

    if (is_day()) {
        $title = get_the_date(_x('F j, Y', 'daily archives date format'));
    }

    if (is_home()) {
        $title = __('Lastest Post', RT_THEME_DOMAIN);
    }

    if (is_404()) {
        $title = __('404', RT_THEME_DOMAIN);
    }

    if (is_search()) {
        $title = sprintf(__('Search Results for: %s', RT_THEME_DOMAIN), get_search_query());
    }

    rt_get_template_part('global/page-title', ['title' => $title, 'desc' => $desc]);
}

/*=================================================;
/* Breadcrumbs
/*================================================= */
function rt_get_breadrumbs()
{
    if (rt_get_option('breadcrumbs', true)) {
        ob_start();

        $html = "<div class='rt-breadcrumbs'>";

        if (function_exists('bcn_display')) {
            $html .= bcn_display();
        } elseif (function_exists('yoast_breadcrumb') && !empty(get_option('wpseo_titles')['breadcrumbs-enable'])) {
            $html .= yoast_breadcrumb('', '', false);
        } elseif (function_exists('rank_math_the_breadcrumbs') && rank_math_get_breadcrumbs()) {
            $html .= rank_math_get_breadcrumbs();
        } elseif (function_exists('breadcrumb_trail')) {
            $html .= breadcrumb_trail([
                'show_title' => false,
                'show_on_front' => false,
                'show_browse' => false,
                'labels' => [
                    'browse' => __('Browse:', RT_THEME_DOMAIN),
                    'aria_label' => esc_attr_x('Breadcrumbs', 'breadcrumbs aria label', RT_THEME_DOMAIN),
                    'home' => __('Home', RT_THEME_DOMAIN),
                    'error_404' => __('404 Not Found', RT_THEME_DOMAIN),
                    'archives' => __('Archives', RT_THEME_DOMAIN),
                    'search' => __('Search results for: %s', RT_THEME_DOMAIN),
                    'paged' => __('Page %s', RT_THEME_DOMAIN),
                    'paged_comments' => __('Comment Page %s', RT_THEME_DOMAIN),
                    'archive_minute' => __('Minute %s', RT_THEME_DOMAIN),
                    'archive_week' => __('Week %s', RT_THEME_DOMAIN),
                ],
                'echo' => false,
            ]);
        }

        $html .= "</div>";

        $html .= ob_get_clean();

        return $html;
    }
}

/*=================================================;
/* POST NOT FOUND
/*================================================= */
function rt_post_not_found()
{
    rt_get_template_part('post/post-none');
}

/*=================================================;
/* Pagination
/*================================================= */
function rt_get_pagination($args = array())
{

    // Check this page have archive
    if (!empty($args['archive'])) {
        global $wp_query;

        $query_by['total'] = $wp_query->max_num_pages;
        $query_by['post_total'] = $wp_query->found_posts;

        // Check taxonomy have archive
        $query = get_queried_object();

        if (!empty($query->slug)) {
            $query_by['tax_query'][] = [
                'taxonomy' => $query->taxonomy,
                'field' => 'slug',
                'terms' => $query->slug,
                'operator' => 'IN',
            ];
        }

        $args = array_merge($query_by, $args);
    }

    // Build query
    $args = apply_filters('rt_loop_query', wp_parse_args($args, [
        'target' => 'post-archive',
        'pagination_style' => 'number',
        'post_type' => 'post',
        'posts_per_page' => 9,
        'total' => 9,
        'post_total' => 2,
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'template_part' => 'template-parts/post/post',
    ]));

    // Generate HTML
    $html = '';

    if ($args['total'] > 1) {

        if ($args['pagination_style'] == 'loadmore') {
            $options = htmlspecialchars(json_encode($args));

            $html = '<div class="rt-pagination rt-pagination--loadmore">';
            $html .= '<a
            data-target="' . $args['target'] . '"
            data-current-page="1"
            data-post-perpage="' . $args['posts_per_page'] . '"
            data-total-page="' . $args['total'] . '"
            data-post-total="' . $args['post_total'] . '"
            data-setting= "' . $options . '"
            class="rt-pagination__button js-loop-load rt-btn rt-btn--border">' . __('Load More', RT_THEME_DOMAIN) . '</a>';
            $html .= '</div>';
        }

        if ($args['pagination_style'] == 'number') {
            $html = '<div class="rt-pagination">';
            $html .= paginate_links(wp_parse_args($args, [
                'prev_text' => '<span class="rt-icon"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor" class="bi bi-arrow-left" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"/></svg></span>',
                'next_text' => '<span class="rt-icon"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor" class="bi bi-arrow-right" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/></svg></span>',
            ]));
            $html .= '</div>';
        }
    }

    return $html;

}

/*=================================================
 *  WOOCOMMERCE CONDITION TAGS
/*=================================================
 * Check woocommerce plugin active
 * Check woocommerce page
 *
 * @version 1.0.0
 */

function rt_is_woocommerce($page = '')
{
    if (class_exists('WooCommerce')) {
        // Rertuns true on WooCommerce Plugins active
        if (empty($page)) {
            return true;
        }

        // all template WooCommerce but cart and checkout not include
        if ($page == 'pages' && is_woocommerce()) {
            return true;
        }

        // Returns true when on the product archive page (shop).
        if ($page == 'shop' && is_shop() || $page == 'shop' && is_tax(get_object_taxonomies('product'))) {
            return true;
        }

        // Return true on single product page
        if ($page == 'product' && is_product()) {
            return true;
        }

        // Returns true on the customer’s account pages.
        if ($page == 'account' && is_account_page()) {
            return true;
        }

        // Returns true on the cart page.
        if ($page == 'cart' && is_cart()) {
            return true;
        }

        // Returns true on the checkout page.
        if ($page == 'checkout' && is_checkout()) {
            return true;
        }

        // Returns true when viewing a WooCommerce endpoint
        if ($page == 'endpoint_url' && is_wc_endpoint_url()) {
            return true;
        }

        if ($page == 'category' && is_product_category()) {
            return true;
        }

        if ($page == 'tag' && is_product_tag()) {
            return true;
        }
    } else {
        return false;
    }
}

/*=================================================;
/* TEMPLATE LOOP
/*================================================= */
/**
 * Show loop contents for grid or carousel layout
 * Added pagination number or load more to CPT
 * Show dynamic content from wp query to carousel or grid layout
 * Show static contnet to carousel or grid layout
 *
 */

function rt_template_loop($args = array())
{

    // custom query
    $the_query = new \WP_Query(\Retheme\Helper::query($args));

    // set default setting
    // replace default setting with new setting from loop query
    $settings = wp_parse_args($args, [
        'id' => 'loop_archive',
        'post_type' => 'post',
        'post_status' => 'publish',
        'content' => '', // use for static content or array
        'posts_per_page' => 9,
        'class' => '',
        'template_part' => 'template-part/content',
        'setting_column' => 4,
        'setting_column_tablet' => 2,
        'setting_column_mobile' => 2,
        'pagination_style' => 'number',
        'carousel' => false,
        'slider_item' => 4,
        'slider_item_tablet' => 2,
        'slider_item_mobile' => 1,
        'slider_gap' => 30,
        'slider_gap_tablet' => 20,
        'slider_gap_mobile' => 15,
        'slider_loop' => false,
        'slider_auto_play' => false,
        'slider_pagination' => false,
        'slider_nav' => [
            'nextEl' => '.swiper-button-next',
            'prevEl' => '.swiper-button-prev',
        ],
        'slider_same_height' => true,
        'slider_link' => false,
        'link_text' => '',
        'link_url' => '',

    ]);

    // class wrapper
    $classes[] = !empty($settings['class']) ? $settings['class'] : '';
    $classes[] = !empty($settings['masonry']) ? 'grid-masonry js-masonry' : '';

    // loop from wp query
    if ($settings['post_type'] !== 'content') {
        if ($the_query->have_posts()):

            // layout column
            if (!$settings['carousel']) {

                // column setting css class
                $classes[] = 'grids';
                $classes[] = 'grids-md-' . $settings['setting_column'];
                $classes[] = 'grids-sm-' . $settings['setting_column_tablet'];
                $classes[] = 'grids-xs-' . $settings['setting_column_mobile'];

                $classes = implode(' ', $classes);

                echo "<div id='{$settings['id']}' class='{$classes}'>";

                while ($the_query->have_posts()): $the_query->the_post();

                    get_template_part($settings['template_part']);

                endwhile;

                echo '</div>';

                // add pagination
                echo rt_get_pagination(wp_parse_args($args, [
                    'target' => $settings['id'],
                    'pagination_style' => 'number',
                    'post_type' => 'post',
                    'posts_per_page' => 9,
                    'total' => $the_query->max_num_pages,
                    'post_total' => $the_query->found_posts,
                    'format' => '?paged=%#%',
                    'current' => max(1, get_query_var('paged')),
                ]));

            }

            // layout carousel
            if ($settings['carousel']) {

                $classes[] = 'rt-swiper--card';

                echo rt_get_before_slider([
                    'id' => "rt-swiper-{$settings['id']}",
                    'class' => implode(' ', $classes),
                    'items-lg' => $settings['slider_item'],
                    'items-md' => $settings['slider_item_tablet'],
                    'items-sm' => $settings['slider_item_mobile'],
                    'gap-lg' => $settings['slider_gap'],
                    'gap-md' => $settings['slider_gap_tablet'],
                    'gap-sm' => $settings['slider_gap_mobile'],
                    'loop' => $settings['slider_loop'],
                    'autoplay' => $settings['slider_auto_play'],
                    'sameheight' => $settings['slider_same_height'],
                    'navigation' => $settings['slider_nav'],
                ]);

                while ($the_query->have_posts()): $the_query->the_post();
                    echo rt_get_before_slide();
                    get_template_part($settings['template_part']);
                    echo rt_get_after_slide();
                endwhile;

                // Added new slide link
                if ($settings['slider_link']) {
                    $link_url = esc_url_raw($settings['link_url']);
                    $link_text = $settings['link_text'];

                    echo rt_get_before_slide();
                    echo "<a href='{$link_url}' class='swiper-link'>";
                    echo '<span class="rt-icon"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor" class="bi bi-arrow-right" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/></svg></span>';
                    echo "<span>{$link_text}</span>";
                    echo "</a>";
                    echo rt_get_after_slide();
                }

                echo rt_get_after_slider();
            }

            wp_reset_postdata();
        else:
            do_action('rt_post_none');
        endif;
    }

    // loop form array or static content
    if ($settings['post_type'] === 'content') {
        if ($settings['content']) {

            // layout column
            if (!$settings['carousel']) {

                // class wrapper
                $classes[] = 'grids';
                $classes[] = 'grids-md-' . $settings['setting_column'];
                $classes[] = 'grids-sm-' . $settings['setting_column_tablet'];
                $classes[] = 'grids-xs-' . $settings['setting_column_mobile'];

                $classes = implode(' ', $classes);

                echo "<div id='{$settings['id']}' class='{$classes}'>";

                foreach ($settings['content'] as $key => $content) {
                    get_template_part($settings['template_part'], $content);
                }

                echo '</div>';
            }

            // layout carousel
            if ($settings['carousel']) {

                $classes[] = 'rt-swiper--card';

                echo rt_get_before_slider([
                    'id' => "rt-swiper-{$settings['id']}",
                    'class' => implode(' ', $classes),
                    'items-lg' => $settings['slider_item'],
                    'items-md' => $settings['slider_item_tablet'],
                    'items-sm' => $settings['slider_item_mobile'],
                    'spaceBetween' => $settings['slider_gap'],
                    'loop' => $settings['slider_loop'],
                    'autoplay' => $settings['slider_auto_play'],
                    'sameheight' => $settings['slider_same_height'],
                    'navigation' => $settings['slider_nav'],
                ]);

                foreach ($settings['content'] as $key => $content) {
                    echo rt_get_before_slide();
                    get_template_part($settings['template_part'], $content);
                    echo rt_get_after_slide();
                }
                echo rt_get_after_slider();
            }

        } else {
            do_action('rt_post_none');
        }
    }
}

/**
 * Generate social media icon link
 *
 * @return void
 */
function rt_get_social_media($args = '')
{
    $args = wp_parse_args($args, [
        'type' => 'simple',
        'size' => 'md',
    ]);

    $facebook = rt_get_option('socmed_facebook', '#');
    $instagram = rt_get_option('socmed_instagram', '#');
    $youtube = rt_get_option('socmed_youtube', '#');
    $twitter = rt_get_option('socmed_twitter', '#');
    $pinterest = rt_get_option('socmed_pinterest', '#');
    $tiktok = rt_get_option('socmed_tiktok', '#');
    $size = 16;

    if ($facebook || $instagram || $youtube || $twitter || $pinterest || $tiktok) {

        $html = '<div class="rt-socmed rt-socmed--' . $args['type'] . ' rt-socmed--' . $args['size'] . '">';

        if ($facebook) {
            $html .= '<a class="rt-socmed__item facebook" href="' . $facebook . '" target="blank">';
            $html .= '<span class="rt-icon">';
            $html .= '<svg xmlns="http://www.w3.org/2000/svg" width="' . $size . '" height="' . $size . '" fill="currentColor" class="bi bi-facebook" viewBox="0 0 16 16"><path d="M16 8.049c0-4.446-3.582-8.05-8-8.05C3.58 0-.002 3.603-.002 8.05c0 4.017 2.926 7.347 6.75 7.951v-5.625h-2.03V8.05H6.75V6.275c0-2.017 1.195-3.131 3.022-3.131.876 0 1.791.157 1.791.157v1.98h-1.009c-.993 0-1.303.621-1.303 1.258v1.51h2.218l-.354 2.326H9.25V16c3.824-.604 6.75-3.934 6.75-7.951z"/></svg>';
            $html .= '</span>';
            $html .= '</a>';
        }

        if ($instagram) {
            $html .= '<a class="rt-socmed__item instagram" href="' . $instagram . '" target="blank">';
            $html .= '<span class="rt-icon">';
            $html .= '<svg xmlns="http://www.w3.org/2000/svg" width="' . $size . '" height="' . $size . '" fill="currentColor" class="bi bi-instagram" viewBox="0 0 16 16"><path d="M8 0C5.829 0 5.556.01 4.703.048 3.85.088 3.269.222 2.76.42a3.917 3.917 0 0 0-1.417.923A3.927 3.927 0 0 0 .42 2.76C.222 3.268.087 3.85.048 4.7.01 5.555 0 5.827 0 8.001c0 2.172.01 2.444.048 3.297.04.852.174 1.433.372 1.942.205.526.478.972.923 1.417.444.445.89.719 1.416.923.51.198 1.09.333 1.942.372C5.555 15.99 5.827 16 8 16s2.444-.01 3.298-.048c.851-.04 1.434-.174 1.943-.372a3.916 3.916 0 0 0 1.416-.923c.445-.445.718-.891.923-1.417.197-.509.332-1.09.372-1.942C15.99 10.445 16 10.173 16 8s-.01-2.445-.048-3.299c-.04-.851-.175-1.433-.372-1.941a3.926 3.926 0 0 0-.923-1.417A3.911 3.911 0 0 0 13.24.42c-.51-.198-1.092-.333-1.943-.372C10.443.01 10.172 0 7.998 0h.003zm-.717 1.442h.718c2.136 0 2.389.007 3.232.046.78.035 1.204.166 1.486.275.373.145.64.319.92.599.28.28.453.546.598.92.11.281.24.705.275 1.485.039.843.047 1.096.047 3.231s-.008 2.389-.047 3.232c-.035.78-.166 1.203-.275 1.485a2.47 2.47 0 0 1-.599.919c-.28.28-.546.453-.92.598-.28.11-.704.24-1.485.276-.843.038-1.096.047-3.232.047s-2.39-.009-3.233-.047c-.78-.036-1.203-.166-1.485-.276a2.478 2.478 0 0 1-.92-.598 2.48 2.48 0 0 1-.6-.92c-.109-.281-.24-.705-.275-1.485-.038-.843-.046-1.096-.046-3.233 0-2.136.008-2.388.046-3.231.036-.78.166-1.204.276-1.486.145-.373.319-.64.599-.92.28-.28.546-.453.92-.598.282-.11.705-.24 1.485-.276.738-.034 1.024-.044 2.515-.045v.002zm4.988 1.328a.96.96 0 1 0 0 1.92.96.96 0 0 0 0-1.92zm-4.27 1.122a4.109 4.109 0 1 0 0 8.217 4.109 4.109 0 0 0 0-8.217zm0 1.441a2.667 2.667 0 1 1 0 5.334 2.667 2.667 0 0 1 0-5.334z"/></svg>';
            $html .= '</span>';
            $html .= '</a>';
        }

        if ($youtube) {
            $html .= '<a class="rt-socmed__item youtube" href="' . $youtube . '" target="blank">';
            $html .= '<span class="rt-icon">';
            $html .= '<svg xmlns="http://www.w3.org/2000/svg" width="' . $size . '" height="' . $size . '" fill="currentColor" class="bi bi-youtube" viewBox="0 0 16 16"><path d="M8.051 1.999h.089c.822.003 4.987.033 6.11.335a2.01 2.01 0 0 1 1.415 1.42c.101.38.172.883.22 1.402l.01.104.022.26.008.104c.065.914.073 1.77.074 1.957v.075c-.001.194-.01 1.108-.082 2.06l-.008.105-.009.104c-.05.572-.124 1.14-.235 1.558a2.007 2.007 0 0 1-1.415 1.42c-1.16.312-5.569.334-6.18.335h-.142c-.309 0-1.587-.006-2.927-.052l-.17-.006-.087-.004-.171-.007-.171-.007c-1.11-.049-2.167-.128-2.654-.26a2.007 2.007 0 0 1-1.415-1.419c-.111-.417-.185-.986-.235-1.558L.09 9.82l-.008-.104A31.4 31.4 0 0 1 0 7.68v-.123c.002-.215.01-.958.064-1.778l.007-.103.003-.052.008-.104.022-.26.01-.104c.048-.519.119-1.023.22-1.402a2.007 2.007 0 0 1 1.415-1.42c.487-.13 1.544-.21 2.654-.26l.17-.007.172-.006.086-.003.171-.007A99.788 99.788 0 0 1 7.858 2h.193zM6.4 5.209v4.818l4.157-2.408L6.4 5.209z"/></svg>';
            $html .= '</span>';
            $html .= '</a>';
        }

        if ($twitter) {
            $html .= '<a class="rt-socmed__item twitter" href="' . $twitter . '" target="blank">';
            $html .= '<span class="rt-icon">';
            $html .= '<svg xmlns="http://www.w3.org/2000/svg" width="' . $size . '" height="' . $size . '" fill="currentColor" class="bi bi-twitter" viewBox="0 0 16 16"><path d="M5.026 15c6.038 0 9.341-5.003 9.341-9.334 0-.14 0-.282-.006-.422A6.685 6.685 0 0 0 16 3.542a6.658 6.658 0 0 1-1.889.518 3.301 3.301 0 0 0 1.447-1.817 6.533 6.533 0 0 1-2.087.793A3.286 3.286 0 0 0 7.875 6.03a9.325 9.325 0 0 1-6.767-3.429 3.289 3.289 0 0 0 1.018 4.382A3.323 3.323 0 0 1 .64 6.575v.045a3.288 3.288 0 0 0 2.632 3.218 3.203 3.203 0 0 1-.865.115 3.23 3.23 0 0 1-.614-.057 3.283 3.283 0 0 0 3.067 2.277A6.588 6.588 0 0 1 .78 13.58a6.32 6.32 0 0 1-.78-.045A9.344 9.344 0 0 0 5.026 15z"/</svg>';
            $html .= '</span>';
            $html .= '</a>';
        }

        if ($pinterest) {
            $html .= '<a class="rt-socmed__item pinterest" href="' . $pinterest . '" target="blank">';
            $html .= '<span class="rt-icon">';
            $html .= '<svg xmlns="http://www.w3.org/2000/svg" width="' . $size . '" height="' . $size . '" fill="currentColor" class="bi bi-pinterest" viewBox="0 0 16 16"><path d="M8 0a8 8 0 0 0-2.915 15.452c-.07-.633-.134-1.606.027-2.297.146-.625.938-3.977.938-3.977s-.239-.479-.239-1.187c0-1.113.645-1.943 1.448-1.943.682 0 1.012.512 1.012 1.127 0 .686-.437 1.712-.663 2.663-.188.796.4 1.446 1.185 1.446 1.422 0 2.515-1.5 2.515-3.664 0-1.915-1.377-3.254-3.342-3.254-2.276 0-3.612 1.707-3.612 3.471 0 .688.265 1.425.595 1.826a.24.24 0 0 1 .056.23c-.061.252-.196.796-.222.907-.035.146-.116.177-.268.107-1-.465-1.624-1.926-1.624-3.1 0-2.523 1.834-4.84 5.286-4.84 2.775 0 4.932 1.977 4.932 4.62 0 2.757-1.739 4.976-4.151 4.976-.811 0-1.573-.421-1.834-.919l-.498 1.902c-.181.695-.669 1.566-.995 2.097A8 8 0 1 0 8 0z"/></svg>';
            $html .= '</span>';
            $html .= '</a>';
        }

        if ($tiktok) {
            $html .= '<a class="rt-socmed__item tiktok" href="' . $tiktok . '" target="blank">';
            $html .= '<span class="rt-icon">';
            $html .= '<svg xmlns="http://www.w3.org/2000/svg" width="' . $size . '" height="' . $size . '" fill="currentColor" class="bi bi-tiktok" viewBox="0 0 16 16"><path d="M9 0h1.98c.144.715.54 1.617 1.235 2.512C12.895 3.389 13.797 4 15 4v2c-1.753 0-3.07-.814-4-1.829V11a5 5 0 1 1-5-5v2a3 3 0 1 0 3 3V0Z"/></svg>';
            $html .= '</span>';
            $html .= '</a>';
        }

        $html .= '</div>';

        return $html;
    }
}

/*=================================================
 *  LIMIT STRING
/*=================================================
 * @version 2.0.0
 * limit text
 * return string
 */
function rt_limited_string($text, $length)
{
    $text = strip_tags(strip_shortcodes($text));
    $length = !empty($length) ? $length : 160;

    if (strlen($text) > $length) {
        $text = substr($text, 0, $length) . '...';
    }

    return $text;
}

/*=================================================
 *  LIMIT TITLE
/*=================================================
 * @since 1.3.0
 * @param  number for limit string $length
 * @return string title limit
 */
function rt_get_the_title($length = '')
{
    $length = !empty($length) ? $length : 60;
    $title = get_the_title();

    rt_limited_string($title, $length);
    return $title;
}

/*=================================================;
/* SIDEBAR
/*=================================================
 * Check is page have sidebar
 *
 * @version 1.0.0
 * @return boolean
 */
function rt_get_sidebar($name = null, $args = array())
{

    if (!in_array(rt_get_template(), array('compact', 'full-width', 'normal'))) {
        get_sidebar($name, $args);
    }
}
/*=================================================;
/* BANNER ADS
/*================================================= */
function rt_get_banner_ads($args = [])
{
    $args = wp_parse_args($args, [
        'type' => '',
        'image' => '',
        'url' => '#',
        'code' => '',
    ]);

    $html = '';

    if ($args['type'] == 'image') {
        if (!empty($args['image'])) {
            $html .= '<a href="' . $args['url'] . '">';
            $html .= '<div class="rt-img">';
            $html .= wp_get_attachment_image($args['image'], 'full');
            $html .= '</div>';
            $html .= '</a>';
        }
    } else if ($args['type'] == 'code') {
        $html .= do_shortcode($args['code']);
    }

    return $html;
}

/*=================================================;
/* FOOTER COPYRIGHT
/*================================================= */
function rt_get_site_copyright()
{
    $theme = rt_get_theme('product-name');

    if (rt_is_premium()) {
        $text_option = rt_get_option('footer_bottom_text', '@Copyright ' . get_bloginfo('name') . '. All Rights Reserved');
        $text_format = str_replace(array('{{site_name}}', '{{year}}'), array(get_bloginfo('name'), date("Y")), $text_option);

        $html = "<span class='rt-site-copyright'>{$text_format}</span>";
    } else {
        $html =  "<span class='rt-site-copyright'>@Copyright {$theme} by <a href='https://webforia.id/'>Webforia Studio</a>. All Rights Reserved</span>";
    }

    return $html;
}

/*=================================================;
/* POST CATEGORY META
/*================================================= */
function rt_get_categories_style($term_id)
{
    $styles['color'] = rt_get_term_meta($term_id, 'wf_badges_color');
    $styles['background'] = rt_get_term_meta($term_id, 'wf_badges_background');

    foreach ($styles as $key => $style) {
        if ($style) {
            $style_css[] = "{$key}: $style;";
        }
    }

    if ($style_css) {
        return implode($style_css);
    }
}
