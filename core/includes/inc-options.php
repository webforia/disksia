<?php
/*==============================================
 * GET OPTION
==============================================
 * this function get option from customizer
 * if metabox not null this function return data form metabox
 * name customizer, option and metabox must same
 * role: filter > metabox > option > customizer
 *
 * @version 1.0.0
 */
function rt_get_option($setting, $default = '')
{
    // check default options
    if (!empty(rt_get_post_meta($setting))) {

        $option = rt_get_post_meta($setting);

    } elseif (!empty(rt_get_theme_option($setting))) {

        $option = rt_get_theme_option($setting);

    } else {
        $option = get_theme_mod($setting, $default);
    }

    return apply_filters($setting, $option);

}

/*=================================================;
/* GET METABOX FROM CARBON FIELD
/*=================================================
 * @version 1.0.0
 * @return post meta
 * @requires carbonfield framework
 * @see https://docs.carbonfields.net/learn/containers/post-meta.html
 */
function rt_get_post_meta($value)
{
    return get_post_meta(get_the_ID(), "_{$value}");
}

/*=================================================;
/* GET TERM META FROM CARBON FIELD
/*=================================================
 * @version 1.0.0
 * @return term meta
 * @requires carbonfield framework
 * @see https://docs.carbonfields.net/learn/containers/term-meta.html
 */
function rt_get_term_meta($term_id, $value)
{
    return get_term_meta($term_id, "_{$value}", true);

}

/*=================================================;
/* GET OPTIONS FROM CARBON FIELD
/*================================================= */
function rt_get_theme_option($value)
{
    return get_option("_{$value}");
}

/*=================================================;
/* GET FIELD ACF
/*=================================================
 * This function replace default get field acf
 *
 * @version 1.0.0
 */
function rt_get_field($field, $post_id = false, $format_value = true)
{

    if (!class_exists('acf')) {
        return false;
    }

    if ($post_id == 'option') {
        $value = get_field($field, 'option');
    } else {
        $value = get_field($field, $post_id, $format_value);
    }

    /**
     * Some values are saved as empty string or 0 for fields (e.g true_false fields).
     * So we used is_null instead of is_empty to check if post meta is set or not.
     */
    if (is_null($value)) {
        return false;

    }

    return $value;
}

/*=================================================;
/* TEMPLATE PART
/*=================================================
 * Get template from template-parts folder
 */
function rt_get_template_part($template, $args = '', $name = '')
{
    get_template_part("template-parts/{$template}", $name, $args);

}

/*=================================================;
/* LOCAL SERVER
/*=================================================
 * Check if user is only running the product’s on localhost
 *
 * @version 1.0.0
 */
function rt_is_local()
{
    $site = $_SERVER['SERVER_NAME'];

    $local = [
        'localhost',
        '127.0.0.1',
        '10.0.0.0/8',
        '172.16.0.0/12',
        '192.168.0.0/16',
        '*.dev',
        '.*local',
        'dev.*',
        'staging.*',
    ];

    if (in_array($site, $local)) {
        return false;
    }

}

/*=================================================;
/* DEVELOPER FEATUARE
/*================================================= */
function rt_is_dev()
{
    return apply_filters('retheme_dev', false);
}

/*=================================================;
/* PREMIUM
/*=================================================
 * Check if user is only running the product’s Premium Version code
 *
 * @version 1.0.0
 * @return true on localhost or user have valid license
 */
function rt_is_premium()
{
    if (rt_is_local() || rt_is_dev()) {
        return true;
    } else {
        $premium = new Retheme\Activation();

        return $premium->is_premium();
    }

}

/*=================================================;
/* PLAN TO PREMIUM
/*=================================================
 * code running premium active or not
 *
 * @version 1.0.0
 * @return void
 */
function rt_is_premium_plan()
{
    return true;
}

/*=================================================;
/* FREE
/*================================================= */
function rt_is_free()
{
    if (rt_is_premium()) {
        return false;
    } else {
        return true;
    }
}

/*=================================================;
/* FREE PLAN
/*=================================================
 * Check if the user is on the free plan of the product and not want to active premium.
 *
 * @version 1.0.0
 */
function rt_is_free_plan()
{
    if (rt_is_premium_plan()) {
        return false;
    } else {
        return true;
    }

}

/*=================================================;
/* Featured PLAN
/*=================================================
 * code not running because, code for future plan
 *
 * @version 1.0.0
 */
function rt_is_feature()
{
    return false;
}

/*=================================================;
/* MOBILE - DETECT
/*================================================= */
// mobile and tablet
function rt_is_mobile_all()
{
    $detect = new Mobile_Detect;

    return ($detect->isMobile()) ? true : false;
}

// only mobile
function rt_is_mobile()
{
    $detect = new Mobile_Detect;

    if ($detect->isMobile() && !$detect->isTablet()) {
        return true;
    }

}
// only tablet
function rt_is_tablet()
{
    $detect = new Mobile_Detect;

    if ($detect->isTablet()) {
        return true;
    }

}

/*=================================================;
/* RESPONSIVE OPTION - MOBILE DETECT
/*================================================= */
function rt_get_option_responsive($setting, $default = '')
{

    $option = rt_get_option($setting, $default);

    if (apply_filters('adaptive_mobile_detect', true)) {
        
        if (rt_is_tablet()) {
            $option = rt_get_option("{$setting}_tablet", $default);
        }

        if (rt_is_mobile()) {
            $option = rt_get_option("{$setting}_mobile", $default);
        }
    }

    return $option;
}
