<?php
/*=================================================;
/* GET TEMPLATE LAYOUT
/*=================================================
 * Get template from theme mods
 *
 * @version 1.0.0
 * @return string
 */
function rt_get_template()
{
    $template = 'normal';

    if (is_single()) {
        $template = rt_get_option('single_layout', 'sidebar-right');
    }

    if (is_page()) {
        $template = rt_get_option('page_layout', 'normal');
    }

    if (is_archive() || is_home()) {
        $template = rt_get_option('post_layout', 'sidebar-right');
    }

    if (is_page_template('templates/front-page.php')) {
        $template = 'normal';
    }

    if (is_page_template('templates/full-width.php')) {
        $template = 'normal';
    }
    
    if (is_page_template('templates/compact.php')) {
        $template = 'compact';
    }

    if (is_search()) {
        $template = rt_get_option('post_layout', 'sidebar-right');
    }

    return apply_filters('template_layout', $template);
}

/*=================================================;
/* LAYOUT BODY CLASS
/*=================================================
 * Added class template to body html
 *
 * @version 1.0.0
 * @return css class
 */
function rt_set_template_class($classes)
{
    $classes[] = 'template--' . rt_get_template();

    if (in_array(rt_get_template() == 'compact', ['compact', 'default', 'full-width', 'normal'])) {
        $classes[] = 'is-gutenberg';
    }
    
    return $classes;
}
add_filter('body_class', 'rt_set_template_class');
