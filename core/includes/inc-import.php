<?php
/*=================================================;
/* REMOVE DEFAULT WIDGETS
/*=================================================
 * Add your widget area to unset the default widgets from.
 * If your theme's first widget area is "sidebar-1", you don't need this.
 *
 * @source https://stackoverflow.com/questions/11757461/how-to-populate-widgets-on-sidebar-on-theme-activation
 *
 * @param  array $widget_areas Arguments for the sidebars_widgets widget areas.
 * @return array of arguments to update the sidebars_widgets option.
 */
function rt_import_merlin_unset_default_widgets_args($widget_areas)
{

    $widget_areas = array(
        'retheme_sidebar' => array(),
        'retheme_woocommerce' => array(),
        'retheme_footer_1' => array(),
        'retheme_footer_2' => array(),
        'retheme_footer_3' => array(),
        'retheme_footer_4' => array(),
    );

    return $widget_areas;
}
add_filter('merlin_unset_default_widgets_args', 'rt_import_merlin_unset_default_widgets_args');

/*=================================================;
/* DEMO IMPORT
/*================================================= */
function rt_import_files($import)
{

    $key = rand();
    $import = [
        [
            'import_file_name' => 'Diksia Default',
            'categories' => ['Personal'],
            'import_file_url' => "https://webforia.id/wp-content/uploads/webforia/diksia/default/diksia-default.xml?key={$key}",
            'import_widget_file_url' => "https://webforia.id/wp-content/uploads/webforia/diksia/default/diksia-default.wie?key={$key}",
            'import_customizer_file_url' => "https://webforia.id/wp-content/uploads/webforia/diksia/default/diksia-default.dat?key={$key}",
            'import_preview_image_url' => "https://webforia.id/wp-content/uploads/webforia/diksia/default/diksia-default.jpg?key={$key}",
            'preview_url' => "https://diskia.webforia.id?key={$key}",
        ],
    ];

    return $import;

}
add_filter('merlin_import_files', 'rt_import_files');

/*=================================================;
/* SET DEFAULT AFTER IMPORT
/*================================================= */
function rt_set_after_all_import($selected_import)
{
    // Assign menu
    set_theme_mod('nav_menu_locations', [
        'primary' => get_term_by('name', 'Primary Menu', 'nav_menu')->term_id,
        'topbar' => get_term_by('name', 'Top Bar Menu', 'nav_menu')->term_id,
        'footer' => get_term_by('name', 'Footer Menu', 'nav_menu')->term_id,
    ]);

    // Set homepage and blog
    update_option('posts_per_page', 6);
    update_option('show_on_front', 'page');
    update_option('page_on_front', get_page_by_path('home')->ID);
    update_option('page_for_posts', get_page_by_path('blog')->ID);

    // Set template home to template builder
    update_post_meta(get_page_by_path('home')->ID, '_wp_page_template', 'templates/front-page.php');

}
add_action('merlin_after_all_import', 'rt_set_after_all_import');

/*=================================================;
/* SET PERMALINK
/*================================================= */
function rt_demo_set_permalink()
{
    // Set Permalink
    global $wp_rewrite;
    $wp_rewrite->set_permalink_structure('/%postname%/');
    $wp_rewrite->flush_rules();
}
add_action('rt_merlin_import_final', 'rt_demo_set_permalink');

/*=================================================;
/* REQUIRED PLUGIN
/*================================================= */
/**
 * Register the required plugins for this theme.
 */
function rt_theme_required_plugins()
{
    /*
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */

    $plugins[] = array();

    if (rt_is_premium()) {
        $plugins[] = [
            'name' => 'Webforia Post Booster',
            'slug' => 'webforia-post-booster',
            'source' => 'https://webforia.id/wp-content/uploads/webforia/webforia-post-booster.zip',
        ];
    }

    /*
     * Array of configuration settings. Amend each line as needed.
     *
     * TGMPA will start providing localized text strings soon. If you already have translations of our standard
     * strings available, please help us make TGMPA even better by giving us access to these translations or by
     * sending in a pull-request with .po file(s) with the translations.
     *
     * Only uncomment the strings in the config array if you want to customize the strings.
     */
    $config = array(
        'id' => 'rt_tgmpa', // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '', // Default absolute path to bundled plugins.
        'menu' => 'tgmpa-install-plugins', // Menu slug.
        'parent_slug' => 'theme-panel', // Parent menu slug.
        'capability' => 'edit_theme_options', // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
        'has_notices' => true, // Show admin notices or not.
        'dismissable' => true, // If false, a user cannot dismiss the nag message.
        'dismiss_msg' => true, // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false, // Automatically activate plugins after installation or not.
        'message' => '', // Message to output right before the plugins table.
    );

    tgmpa($plugins, $config);

}
add_action('tgmpa_register', 'rt_theme_required_plugins');
