<?php 
/*=================================================;
/* REGISTER WIDGET LOCATION
/*================================================= */
function rt_register_widget()
{

    register_sidebar([
        'name' => __('Sidebar', RT_THEME_DOMAIN),
        'id' => 'retheme_sidebar',
        'before_widget' => '<div id="%1$s" class="rt-widget rt-widget--aside %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="rt-widget__header"><h4 class="rt-widget__title">',
        'after_title' => '</h4></div>',
    ]);

    register_sidebar([
        'name' => __('Footer Column 1', RT_THEME_DOMAIN),
        'id' => 'retheme_footer_1',
        'before_widget' => '<div id ="%1$s" class="rt-widget rt-widget--footer %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="rt-widget__header"><h4 class="rt-widget__title">',
        'after_title' => '</h4></div>',
    ]);

    register_sidebar([
        'name' => __('Footer Column 2', RT_THEME_DOMAIN),
        'id' => 'retheme_footer_2',
        'before_widget' => '<div id ="%1$s" class="rt-widget rt-widget--footer %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="rt-widget__header"><h4 class="rt-widget__title">',
        'after_title' => '</h4></div>',
    ]);

    register_sidebar([
        'name' => __('Footer Column 3', RT_THEME_DOMAIN),
        'id' => 'retheme_footer_3',
        'before_widget' => '<div id ="%1$s" class="rt-widget rt-widget--footer %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="rt-widget__header"><h4 class="rt-widget__title">',
        'after_title' => '</h4></div>',
    ]);

    register_sidebar([
        'name' => __('Footer Column 4', RT_THEME_DOMAIN),
        'id' => 'retheme_footer_4',
        'before_widget' => '<div id ="%1$s" class="rt-widget rt-widget--footer %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="rt-widget__header"><h4 class="rt-widget__title">',
        'after_title' => '</h4></div>',
    ]);

    register_sidebar([
        'name' => __('Footer Column 5', RT_THEME_DOMAIN),
        'id' => 'retheme_footer_5',
        'before_widget' => '<div id ="%1$s" class="rt-widget rt-widget--footer %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="rt-widget__header"><h4 class="rt-widget__title">',
        'after_title' => '</h4></div>',
    ]);

    register_sidebar([
        'name' => __('Footer Column 6', RT_THEME_DOMAIN),
        'id' => 'retheme_footer_6',
        'before_widget' => '<div id ="%1$s" class="rt-widget rt-widget--footer %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="rt-widget__header"><h4 class="rt-widget__title">',
        'after_title' => '</h4></div>',
    ]);

}
add_action('widgets_init', 'rt_register_widget');

/*=================================================;
/* SET SIZE TAG
/*================================================= */
function rt_tag_cloud_widget($args)
{
    $args['largest'] = 14;
    $args['smallest'] = 14;
    $args['unit'] = 'px';

    return $args;
}
add_filter('widget_tag_cloud_args', 'rt_tag_cloud_widget');