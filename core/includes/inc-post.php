<?php
/*=================================================;
/* WRAPPER CLASS
/*=================================================
 * Added CSS class to parent item loop
 *
 * @version 1.0.0
 * @return html attribute
 */
function rt_get_posts_class($class = '')
{   
    $classes[] = $class;
    $classes[] = 'rt-post-archive';

    if (rt_get_option('post_style', 'grid') !== 'list') {
        $classes[] = "grids";
        $classes[] = "grids-md-" . rt_get_option('post_column', 2);
        $classes[] = "grids-md-" . rt_get_option('post_column', 2);
        $classes[] = "grids-sm-" . rt_get_option('post_column_tablet', 1);

        // Masonry layout enable
        if (rt_get_option('post_masonry', false)) {
            $classes[] = 'grid-masonry js-masonry';
        }

    }

    // Separates classes with a single space, collates classes for post DIV.
    return 'class="' . esc_attr(implode(' ', $classes)) . '"';
}

/*=================================================;
/* LOOPS CLASS
/*=================================================
 * Added custom class on items loop all CPT
 *
 * @version 1.0.0
 */
function rt_post_class($classes)
{
    $classes[] = 'post-item';
    return $classes;
}
add_filter('post_class', 'rt_post_class');

/*=================================================;
/* ARCHIVE - REMOVE PREFIX PAGE TITLE
/*=================================================
 * remove prefix archive on category
 */
function rt_archive_remove_prefix_title($title)
{
    $title = single_cat_title('', false);

    return $title;

}
add_filter('get_the_archive_title', 'rt_archive_remove_prefix_title');

/*=================================================;
/* LIMIT TEXT OUTPUT THE EXCERPT
/*=================================================
 * limit summary text for post
 *
 * @version 1.0.0
 * @see https://developer.wordpress.org/reference/functions/get_the_excerpt/
 * @return string
 */
function rt_limit_excerpt_length($length)
{
    return intval(rt_get_option('post_excerpt_length', 15));
}
add_filter('excerpt_length', 'rt_limit_excerpt_length', 999);

/*=================================================;
/* CHANGE SYMBOL EXCERPT MORE
/*================================================= */
function rt_custom_excerpt_more($more)
{
    return '...';
}
add_filter('excerpt_more', 'rt_custom_excerpt_more');

/*=================================================;
/* GET POST RELATED
/*=================================================
 * Get product id from metabox product
 *
 * @version 1.0.0
 * @return array() id
 */
function rt_get_post_related_id()
{
    $relateds = rt_get_post_meta('post_related');

    $post_related = array();
    if ($relateds) {
        foreach ($relateds as $key => $related) {
            $post_related[] = $related['id'];
        }
    }

    return $post_related;
}

/*=================================================;
/* GET POST VIEW
/*=================================================
 * This function need plugin Webforia Post Booster for update
 */
function rt_get_post_view()
{
    $post_view = get_post_meta(get_the_ID(), 'wp_post_views_count', true);
    return !empty($post_view) ? number_format($post_view, 0, "", ".") : 0;
}

/*=================================================;
/* POST ARCHIVE OPTION
/*================================================= */
function rt_get_post_option($value){
    $element = rt_get_option('post_element', ['category', 'thumbnail', 'content']);
    $meta = rt_get_option('post_meta', ['meta-author', 'meta-date']);

    return in_array($value, array_merge($element, $meta));
}

/*=================================================;
/* POST SINGLE OPTION
/*================================================= */
function rt_get_single_option($value)
{
    $element = rt_get_option('single_element', ['thumbnail', 'category', 'sharebox', 'tag', 'authorbox', 'navigation', 'comment', 'related']);
    $meta = rt_get_option('single_meta', ['meta-author', 'meta-date', 'meta-comment']);

    return in_array($value, array_merge($element, $meta));

}