<?php
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function rt_setup()
{
    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     * If you're building a theme based on retheme, use a find and replace
     * to change 'retheme' to the name of your theme in all the template files.
     */
    load_theme_textdomain(RT_THEME_DOMAIN, get_template_directory() . '/languages');
    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support('title-tag');

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @see https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');
    add_theme_support('wp-block-styles');
    add_theme_support('align-wide');
    add_theme_support('responsive-embeds');
    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support('post-thumbnails');
    add_theme_support('html5', [
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
        'script',
        'style',
    ]);

    /**
     * Add theme support vendor breadcrumbs
     * @see https://themehybrid.com/docs/breadcrumb-trail
     */
    add_theme_support('breadcrumb-trail');

    if (!isset($content_width)) {
        $content_width = 690;
    }

    /**
     * Add theme support selective refresh for customizer
     */
    add_theme_support('customize-selective-refresh-widgets');

    register_nav_menus([
        'primary' => 'Primary Menu',
        'topbar' => 'Top Bar Menu',
        'footer' => 'Footer Menu',
        'mobile' => 'Mobile Menu',
    ]);

    /**
     * Add custom image size
     */
    add_image_size('featured_medium', 400, 225, true);
    
    if (rt_is_premium()) {
        do_action('rt_after_setup_theme');
    }
}
add_action('after_setup_theme', 'rt_setup');

/*=================================================;
/* DEFAUT VARIABLE
/*================================================= */
function rt_get_theme($value, $arg = '')
{

    $default = apply_filters('rt_setup_theme', [
        'product-name' => RT_THEME_NAME,
        'product-slug' => RT_THEME_SLUG,
        'product-version' => RT_THEME_VERSION,
        'product-docs' => RT_THEME_DOC,
        'product-url' => RT_THEME_URL,
        'product-group' => 'https://web.facebook.com/groups/265676900996871',
        'product-contact' => 'https://webforia.id/kontak/',
        'extends' => array(),
        'typography_heading' => [
            'font-family' => '',
            'line-height' => '1.2',
            'variant' => '600',
            'text-transform' => 'none',
        ],
        'typography_regular' => [
            'font-family' => '',
            'font-size' => '16px',
            'line-height' => '1.75',
        ],
        'global_brand_color' => [
            'primary' => '#2f80ed',
            'secondary' => '#3e8df4',
        ],
        'global_font_color' => [
            'primary' => '#262626',
            'secondary' => '#474747',
            'tertiary' => '#777777',
        ],
        'global_color_link' => [
            'normal' => '#2f80ed',
            'hover' => '#3e8df4',
        ],
        'global_background_scheme' => [
            'primary' => '#ffffff',
            'secondary' => '#f0f2f5',
        ],
        'global_border_color' => '#dbdee2',
        'global_highlight_color' => '#f49a40',

    ]);

    return !empty($default[$value]) ? $default[$value] : '';

}

/*=================================================;
/* THEME - BRAND META
/*=================================================
 * Add theme color brand on search board mobile browser
 *
 * @version 1.0.0
 */
function rt_theme_color()
{

    $color = rt_get_option('brand_color_browser');

    if (!empty($color)) {
        echo '<meta name="theme-color" content="' . $color . '">';
    }

}
add_action('wp_head', 'rt_theme_color');
