<?php
include_once dirname(__FILE__) . '/inc-setup.php';
include_once dirname(__FILE__) . '/inc-options.php';
include_once dirname(__FILE__) . '/inc-template-tag.php';
include_once dirname(__FILE__) . '/inc-template-part.php';
include_once dirname(__FILE__) . '/inc-scripts.php';
include_once dirname(__FILE__) . '/inc-comment.php';
include_once dirname(__FILE__) . '/inc-menu.php';
include_once dirname(__FILE__) . '/inc-post.php';
include_once dirname(__FILE__) . '/inc-widget.php';
include_once dirname(__FILE__) . '/inc-import.php';