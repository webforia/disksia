<?php
/*=================================================;
/* REGISTER ASSETS
/*================================================= */
function rt_scripts()
{
    // Animate
    wp_enqueue_style('animate', get_template_directory_uri() . '/assets/animate/animate.min.css', false, '4.1.1');
    wp_enqueue_script('animate', get_template_directory_uri() . '/assets/animate/animate.min.js', false, '3.0.0', true);

    // Retheme styles
    wp_enqueue_script('retheme-form', get_template_directory_uri() . '/assets/js/form.min.js', ['animate'], '3.0.0', true);
    wp_enqueue_script('retheme-header', get_template_directory_uri() . '/assets/js/header.min.js', false, '3.0.0', true);
    wp_enqueue_script('retheme-menu', get_template_directory_uri() . '/assets/js/menu.min.js', ['animate'], '3.1.0', true);
    wp_enqueue_script('retheme-slidepanel', get_template_directory_uri() . '/assets/js/slidepanel.min.js', ['animate'], '3.1.0', true);
    wp_enqueue_script('retheme-modal', get_template_directory_uri() . '/assets/js/modal.min.js', ['animate'], '3.1.0', true);

    // Swiper
    wp_enqueue_style('swiper', get_template_directory_uri() . '/assets/swiper/swiper.min.css', false, '6.2.0');
    wp_enqueue_script('swiper', get_template_directory_uri() . '/assets/swiper/swiper.min.js', false, '6.2.0', true);
    wp_enqueue_script('retheme-swiper', get_template_directory_uri() . '/assets/swiper/retheme-swiper.min.js', ['swiper'], '3.0.0', true);

    // Main style
    wp_enqueue_style('retheme', get_template_directory_uri() . '/assets/css/styles.min.css', false, '3.3.1');
    wp_enqueue_script('retheme', get_template_directory_uri() . '/assets/js/main.min.js', false, '3.0.0', true);

    if (rt_get_option('dark_mode', true)) {
        wp_enqueue_script('retheme-dark-mode', get_template_directory_uri() . '/assets/js/dark-toggle.min.js', false, '1.0.0', true);
    }

    // Comment
    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
        wp_enqueue_script('retheme-comment', get_template_directory_uri() . '/assets/js/comment.min.js', ['animate'], '1.0.0', true);

    }
}
add_action('wp_enqueue_scripts', 'rt_scripts', 99999);

/*=================================================;
/* MOVE JQUERY IN FOOTER
/*================================================= */
function rt_script_move_jquery_into_footer($wp_scripts)
{

    if (is_admin()) {
        return;
    }

    if (rt_get_option('move_jquery_to_footer', true)) {
        $wp_scripts->add_data('jquery', 'group', 1);
        $wp_scripts->add_data('jquery-core', 'group', 1);
        $wp_scripts->add_data('jquery-migrate', 'group', 1);
    }

}
add_action('wp_default_scripts', 'rt_script_move_jquery_into_footer');

/*=================================================;
/* REMOVE EMOJI
/*================================================= */
function rt_script_remove_wp_emoji()
{
    if (is_admin()) {
        return;
    }
    if (rt_get_option('remove_wp_emoji', true)) {
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('admin_print_scripts', 'print_emoji_detection_script');
        remove_action('wp_print_styles', 'print_emoji_styles');
        remove_action('admin_print_styles', 'print_emoji_styles');
    }
}

add_action('after_setup_theme', 'rt_script_remove_wp_emoji');

/*=================================================;
/* DISABLE GUTENBERG BLOCK STYLE ON FRONTEND
/*================================================= */
function rt_script_remove_block_css()
{
    if (is_admin()) {
        return;
    }
    if (rt_get_option('remove_block_style', false)) {
        wp_dequeue_style('wp-block-library'); // Wordpress core
        wp_dequeue_style('wp-block-library-theme'); // Wordpress core
        wp_dequeue_style('global-styles'); // Disable gutenberg global style
    }
    if (rt_get_option('remove_block_style', false)) {
        wp_dequeue_style('wc-blocks-style'); // WooCommerce
    }
}
add_action('wp_enqueue_scripts', 'rt_script_remove_block_css', 100);
