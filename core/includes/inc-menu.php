<?php
/*=================================================;
/* MENU
/*=================================================
 * Add css class to menu item
 * @category Menu
 * @param [type] $classes
 * @param [type] $item
 * @return css class
 */
function rt_menu_class($classes, $item)
{
    if ($item->current) {
        $classes[] = "is-active";
    }

    $classes[] = "rt-menu__item";

    return $classes;

}
add_filter('nav_menu_css_class', 'rt_menu_class', 10, 2);

/*=================================================;
/* SUBMENU
/*=================================================
 * add class sub menu
 * @category menu
 * @param [type] $menu
 * @return void
 */
function rt_submenu_class($classes)
{
    $classes[] = 'rt-menu__submenu';

    return $classes;
}

add_filter('nav_menu_submenu_css_class', 'rt_submenu_class');

/*=================================================;
/* MENU ARROW
/*=================================================
 * Add new dom each item menu if menu has submenu
 *
 * @param $item_output
 * @param $item
 * @param $depth
 * @param $args
 * @return html
 */
function rt_menu_add_arrow($item_output, $item, $depth, $args)
{
    if (in_array('menu-item-has-children', $item->classes)) {
        // append icon
        $item_output .= '<span class="rt-menu__arrow"><span class="rt-icon"><svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" fill="currentColor" class="bi bi-chevron-down" viewBox="0 0 16 16">
        <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/>
      </svg></span></span>';
    }

    return $item_output;
}
add_filter('walker_nav_menu_start_el', 'rt_menu_add_arrow', 10, 4);

/*=================================================;
/* TERM MENU
/*================================================= */
function rt_get_term_menu($args = array())
{

    $options = wp_parse_args($args, [
        'menu_type' => 'vertical',
        'taxonomy' => 'category',
        'hide_empty' => false,
        'class' => '',
        'animatein' => 'zoomIn',
        'animateout' => 'zoomOut',
        'duration' => 300,
        'hide_empty' => false,
        'hierarchical' => true,
    ]);

    $terms = get_terms($options);

    $class[] = 'rt-menu js-menu';
    $class[] = ($options['menu_type'] == 'vertical') ? 'rt-menu--vertical' : 'rt-menu--horizontal';
    $class[] = $options['class'];

    if (!empty($terms)):
        $output = '<div id=term-"' . esc_attr($options['taxonomy']) . '" class="' . esc_attr(implode(" ", $class)) . '" data-animatein="' . esc_attr($options['animatein']) . '" data-animateout="' . esc_attr($options['animateout']) . '" data-duration="' . esc_attr($options['duration']) . '">';
        $output .= '<ul class="rt-menu__main">';

        // loop term
        foreach ($terms as $term) {
            if ($term->parent == 0) {

                //remove uncategorized from loop
                if ($term->slug == 'uncategorized') {
                    continue;
                }

                // Sub category information
                // Merge all term children into a single array of their IDs.
                $children = get_term_children($term->term_id, $options['taxonomy']);

                if (isset($children) && !is_wp_error($children) && sizeof($children) > 0) {

                    $output .= '<li id="term-item-' . esc_attr($term->term_id) . '" class="rt-menu__item term-item-has-children menu-item-has-children term-item-' . esc_attr($term->term_id) . '">';
                    $output .= '<a href="' . esc_url(get_term_link($term)) . '">' . esc_attr($term->name) . '</a>';

                    // add arrow
                    $output .= '<span class="rt-menu__arrow"><span class="rt-icon"><svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" fill="currentColor" class="bi bi-chevron-down" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/>
                  </svg></span></span>';

                    // dropdown
                    $output .= '<ul class="rt-menu__submenu">';

                    foreach ($children as $child) {
                        $term = get_term_by('id', $child, $options['taxonomy']);

                        $output .= '<li id="term-item-' . esc_attr($term->term_id) . '" class="rt-menu__item term-item-' . esc_attr($term->term_id) . '"><a href="' . esc_url(get_term_link($child)) . '">' . esc_html($term->name) . '</a></li>';
                    }

                    $output .= '</ul>';
                    $output .= '</li>';
                } else {
                    $output .= '<li id="term-item-' . esc_attr($term->term_id) . '" class="rt-menu__item ' . esc_attr($term->term_id) . '">';
                    $output .= '<a href="' . esc_url(get_term_link($term)) . '">' . esc_attr($term->name) . '</a>';
                    $output .= '</li>';
                }

            }
        }

        $output .= '</ul>';
        $output .= '</div>';

        echo $output;
    endif;
}

/*=================================================;
/* SET DEFAULT MENU IF NOT SET
/*================================================= */
function rt_menu_default()
{

    $link = '<ul class="rt-menu__main">';
    if (current_user_can('manage_options')) {
        $link .= '<li class="rt-menu__item is-active"><a href="' . admin_url('nav-menus.php') . '">Edit Menu</a></li>';
    }
    $link .= '<li class="rt-menu__item"><a>Sample Menu</a></li>';
    $link .= '<li class="rt-menu__item"><a>Sample Menu</a></li>';
    $link .= '<li class="rt-menu__item"><a>Sample Menu</a></li>';
    $link .= '<ul>';

    echo $link;
}