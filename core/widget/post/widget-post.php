<?php
// Creating the widget
class Retheme_Post_Widget extends WP_Widget
{
    public function __construct()
    {
        $theme = rt_get_theme('product-name');

        parent::__construct('retheme_post', __("{$theme} - Recent Posts", RT_THEME_DOMAIN), [
            'description' => __('Show the lastest posts form the blog ', RT_THEME_DOMAIN),
            'customize_selective_refresh' => true,
        ]);
    }

    // Creating widget front-end
    // This is where the action happens
    public function widget($widget, $instance)
    {
        $title = !empty($instance['title']) ? $instance['title'] : __('Recent posts', RT_THEME_DOMAIN);
        $date = !empty($instance['meta_date']) ? $instance['meta_date'] : false;
        $post_per_page = !empty($instance['post_per_page']) ? $instance['post_per_page'] : 5;
  
        if (array_key_exists('before_widget', $widget)) {
            echo $widget['before_widget'];
            if (!empty($title)) {
                echo $widget['before_title'] . $title . $widget['after_title'];
            }

        }

    
        $the_query = new \WP_Query([
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => $post_per_page,
        ]);

        if ($the_query->have_posts()):
            while ($the_query->have_posts()): $the_query->the_post();
                include dirname(__FILE__) . '/widget-post-view.php';
            endwhile;
        else:
            echo __('Not post found', RT_THEME_DOMAIN);
        endif;


        if (array_key_exists('after_widget', $widget)) {
            echo $widget['after_widget'];
        }
    }

    // Widget Backend
    public function form($instance)
    {
        $title = !empty($instance['title']) ? $instance['title'] : '';
        $date = !empty($instance['meta_date']) ? $instance['meta_date'] : false;
        $post_per_page = !empty($instance['post_per_page']) ? $instance['post_per_page'] : 5;
        ?>

        <p>
            <label><?php _e('Title:', RT_THEME_DOMAIN);?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>

        <p>
            <label><?php _e('Post Per Page:', RT_THEME_DOMAIN);?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('post_per_page'); ?>" type="number" value="<?php echo esc_attr($post_per_page); ?>" />
        </p>

         <p>
            <label>
                <input class="checkbox" type="checkbox" <?php checked($date, 'on' ); ?> id="<?php echo $this->get_field_id( 'meta_date' ); ?>" name="<?php echo $this->get_field_name( 'meta_date' ); ?>" />  <?php _e('Show date', RT_THEME_DOMAIN);?>
            </label>
        </p>

        <?php
    }

    // Updating widget replacing old instances with new
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = !empty($new_instance['title']) ? strip_tags($new_instance['title']) : '';
        $instance['meta_date'] = !empty($new_instance['meta_date'])?strip_tags($new_instance['meta_date']):'';
        $instance['post_per_page'] = !empty($new_instance['post_per_page']) ? strip_tags($new_instance['post_per_page']) : '';

        return $instance;
    }
} // Class wpb_widget ends here
