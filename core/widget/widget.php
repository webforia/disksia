<?php
include dirname(__FILE__) . '/social-media/widget-social-media.php';
include dirname(__FILE__) . '/post/widget-post.php';

// Register and load the widget
function rt_custom_widget()
{
    register_widget('Retheme_social_widget');
    register_widget('Retheme_Post_Widget');

}
add_action('widgets_init', 'rt_custom_widget');
