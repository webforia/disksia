
<?php
/* Template Name: Compact
 *
 * The template for displaying content without sidebar and compact.
 *
 */
 ?>
<?php get_header();?>

<section class="page-wrapper ">
    <div class="page-container">
        <div class="page-columns">
            <div class="page-content">

                <?php if (have_posts()): ?>

                    <?php while (have_posts()): the_post(); ?>

                        <article class="rt-single">

                            <h1 class="rt-single-title"><?php echo get_the_title() ?></h1>

                            <div class="rt-single-content rt-entry-content">
                                <?php the_content() ?>
                            </div>

                        </article>

                        <?php  if ( (comments_open() || get_comments_number()) && rt_get_single_option('comment')) : ?>
                            <?php comments_template(); ?>
                        <?php endif ?>

                    <?php endwhile; ?>

                <?php endif; ?>

            </div>

        </div>
    </div>
</section>

<?php get_footer();?>