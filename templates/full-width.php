
<?php
/* Template Name: Full Width
 *
 * The template for displaying content without sidebar and header page.
 *
 * This is the template that displays for page builder.
 */
 ?>
<?php get_header();?>

<section class="page-wrapper ">
    <div class="page-container">
        <div class="page-columns">
            <div class="page-content">

                <?php if (have_posts()): ?>

                    <?php while (have_posts()): the_post(); ?>

                        <?php the_content() ?>

                    <?php endwhile; ?>

                <?php endif; ?>

            </div>

        </div>
    </div>
</section>

<?php get_footer();?>