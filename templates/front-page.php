
<?php
/* Template Name: Front Page
 *
 * The template for displaying content without sidebar and header page.
 *
 * This is the template that displays for page builder.
 */
 ?>
<?php get_header();?>

    <?php if (have_posts()): ?>

        <?php while (have_posts()): the_post(); ?>

            <?php rt_get_template_part('homepage/homepage'); ?>

        <?php endwhile; ?>

    <?php endif; ?>

<?php get_footer();?>