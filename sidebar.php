<aside id="page-aside" class="page-aside">
    <div class="page-aside__inner">
        <?php
        // Check Elementor Pro Location
        if (!function_exists('elementor_theme_do_location') || !elementor_theme_do_location('sidebar')) {
            dynamic_sidebar('retheme_sidebar');
        }
        ?>

    </div>
</aside>